from .base import *

DEBUG = False

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     'transcode',
        'USER':     'transcode_user',
        'PASSWORD': 'q28L036N0c7gZsQ516Qm',
        'HOST':     'mysql_db.local',
        'PORT':     '3306',
        'default-character-set': 'utf8',
        'OPTIONS': {
            'init_command': 'SET character_set_connection=utf8, collation_connection=utf8_unicode_ci, '
                            'wait_timeout=31536000, interactive_timeout=31536000'
        },
    }
}
CONN_MAX_AGE = 0


# Caches
# http://niwibe.github.io/django-redis/
# https://docs.djangoproject.com/en/1.8/topics/cache/

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': [
            'redis://redis.local:6379/10',
        ],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'CACHE_HERD_TIMEOUT': 10,
            'PICKLE_VERSION': -1,
            'SOCKET_TIMEOUT': 1,
            'COMPRESS_MIN_LEN': 0,
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 128,
            },
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'VERSION': '1',
        },
        'KEY_PREFIX': 'TRANSCODE',
    },
    'temporary': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': [
            'redis://redis.local:6379/11',
        ],
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'CACHE_HERD_TIMEOUT': 10,
            'PICKLE_VERSION': -1,
            'SOCKET_TIMEOUT': 1,
            'COMPRESS_MIN_LEN': 0,
            'CONNECTION_POOL_KWARGS': {
                'max_connections': 128,
            },
            'PARSER_CLASS': 'redis.connection.HiredisParser',
        },
        'KEY_PREFIX': 'TEMP',
        'VERSION': '1',
    },
    'thread_memory': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'transcode_configurations',
    },
}


STATIC_ROOT = '/var/www/html/transcode_static'

STATIC_URL = '/static/'


# Logging configurations

BASE_LOG_DIR = '/var/log/transcode'
if not os.path.exists(BASE_LOG_DIR):
    os.makedirs(BASE_LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "%(asctime)s %(levelname)s %(module)s:%(filename)s:%(funcName)s:%(lineno)d\t%(message)s\t"
                      "%(params)s"
        },
        'simple': {
            'format': '%(asctime)s %(levelname)s %(filename)s:%(lineno)d\t%(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'default-transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'simple',
            'backupCount': 30,
        },
        'worker_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'worker-transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'simple',
            'backupCount': 30,
        },
        'transcode_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'verbose',
            'backupCount': 30,
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['default', 'console'],
            'level': 'INFO',
            'propagate': True,
            'filters': ['require_debug_false'],
        },
        'django.server': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'Transcode': {
            'handlers': ['transcode_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False,
            'filters': ['require_debug_false'],
        },
        'worker': {
            'handlers': ['worker_handler'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}


# Broker
CELERY_BROKER_URL = 'amqp://to_rabbit_user:65q1oLC0uJg525Q8f0yY@transcode_controller.internal:5672//transcode'

RABBITMQ_ADMIN_API = 'http://localhost:15672/'
RABBITMQ_ADMIN_USERNAME = 'admin'
RABBITMQ_ADMIN_PASSWORD = '160591'


# Custom

BACKEND_URI = 'http://transcode_controller.internal/backend/'

API_FILE_INFO = BACKEND_URI + 'file/info'

API_UPDATE_LIVE_TASK = BACKEND_URI + 'live/update'

URL_INPUT_STORAGE_PREFIX = os.getenv('URL_INPUT_STORAGE_PREFIX',
                                     'ftp://transcode:16%2F05%2F91@transcode_controller.internal/transcode_temp/')

PERMANENTLY_INPUT_STORAGE_PREFIX = '/home/ftp_users/transcode/transcode_temp'

URL_OUTPUT_STORAGE_PREFIX = os.getenv('URL_OUTPUT_STORAGE_PREFIX',
                                      'ftp://transcode:16%2F05%2F91@transcode_controller.internal/transcode_temp/')

PERMANENTLY_OUTPUT_STORAGE_PREFIX = '/home/ftp_users/transcode/transcode_temp'

INGEST_URI = 'http://ingestion.crevid.com/api/'

API_REQUEST_INGEST = INGEST_URI + 'ingest/submit'

# PLAYLIST_URI = 'http://playlist.uri/plist/'

# API_ADD_RESOURCE_PLAYLIST = PLAYLIST_URI + 'add'
