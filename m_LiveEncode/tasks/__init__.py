from __future__ import absolute_import

import os
import re
import shutil
import random
import traceback
import ipaddress
import urllib.parse
from distutils.spawn import find_executable
from celery import shared_task, states

from api_core.contrib import utils
from api_core.contrib.base_task import BaseTask, request_file_info

from Controller.models import Workflow
from Monitor.models import Instance
from Monitor.tasks import add_instance_backjob

from ..contrib import scan_mode, logo, crop, dar
from ..apps import CONFIGS

__author__ = 'duybq'


def is_multicast_stream(uri):
    try:
        return ipaddress.ip_address(urllib.parse.urlparse(uri).hostname).is_multicast
    except ValueError:
        return False


def build_live_encode_cmnd(file_data, profile_mapper, video_profile_data, audio_profile_data, workflow_data, use_GPU):
    print('Build command-line encode video')
    live_configs = CONFIGS['live_info'].copy()
    live_configs.update(workflow_data['feature']['metadata'])
    has_dvr = file_data['encode_options'].get('dvr_enable', True)

    if not file_data['encode_options'].get('pass_through_mode', None):
        INPUT_CMND_TEMPATE = live_configs['live_cmnd_part_1']
        if use_GPU:
            SUB_CMND_TEMPLATE = live_configs['live_cmnd_part_2_gpu']
        else:
            SUB_CMND_TEMPLATE = live_configs['live_cmnd_part_2_cpu']
    else:
        INPUT_CMND_TEMPATE = live_configs['live_cmnd_part_1_restream']
        if file_data['encode_options']['pass_through_mode'] == 'video':
            SUB_CMND_TEMPLATE = live_configs['live_cmnd_part_2_restream_video']
        elif file_data['encode_options']['pass_through_mode'] == 'audio':
            SUB_CMND_TEMPLATE = live_configs['live_cmnd_part_2_restream_audio']
        else:
            SUB_CMND_TEMPLATE = live_configs['live_cmnd_part_2_restream']
    real_cmnd = list()

    ##########################################################################################
    # Build input and scale cmnd
    ##########################################################################################
    filter_cmnd = list()
    for idx, val in enumerate(INPUT_CMND_TEMPATE.split(' ')):
        current_cmnd = val
        if val == '<input>':
            ##########################################################################################
            # Prepare input
            ##########################################################################################
            input_resolver = urllib.parse.urlparse(file_data['uri'])
            if 'live_info' not in file_data['encode_options'] or not file_data['encode_options']['live_info'] \
                or file_data['encode_options']['live_info'].get('type', 'pull') == 'pull':
                input_cmnd = file_data['uri']
            else:
                input_cmnd = file_data['uri'].replace(input_resolver.netloc, 'localhost:9116')

            if file_data['encode_options'].get('pass_through_mode', None) in (None, False, 'audio'):
                #
                # Feature: Embed logo
                current_cmnd, filter_cmnd = logo.build_logo_parameters(input_cmnd=input_cmnd,
                                                                       filter_cmnd=filter_cmnd,
                                                                       encode_options=file_data['encode_options'],
                                                                       configs=workflow_data['feature']['metadata'])
            else:
                # Feature: copy stream
                current_cmnd = input_cmnd
        elif val == '<video_scale>':
            ##########################################################################################
            # Video scale
            ##########################################################################################
            _temp = None
            if file_data['encode_options'].get('pass_through_mode', None) in ('video', 'both'):
                # Do not thing
                #
                # Feature Set DAR
                #
                filter_cmnd = list()
                current_cmnd = dar.build_aspect_parameters(filter_cmnd=filter_cmnd,
                                                           encode_options=file_data['encode_options'])
            else:
                # For encode video
                #
                # Feature: Crop
                filter_cmnd = crop.build_crop_parameters(filter_cmnd=filter_cmnd,
                                                         encode_options=file_data['encode_options'])

                #
                # Feature change scan mode: Interlaced or Progressive
                #                           (these params always present at first)
                _temp = scan_mode.get_scan_type(encode_options=file_data['encode_options'],
                                                standard_metadata=file_data['standard_metadata'],
                                                configs=workflow_data['feature']['metadata'])
                #
                # Feature Set DAR
                #
                filter_cmnd = dar.build_dar_parameters(filter_cmnd=filter_cmnd,
                                                       encode_options=file_data['encode_options'])
                #
                # Feature: livestream adaptive
                #
                if _temp:
                    _temp += ','
                num_stream_has_been_splited = len(video_profile_data)
                if has_dvr:
                    num_stream_has_been_splited *= 2
                _temp += 'split=' + str(num_stream_has_been_splited)
                _temp += ''.join(["[out%d]" % e for e in range(1, num_stream_has_been_splited + 1)])
                filter_cmnd = filter_cmnd + [_temp, ]
                current_cmnd = ['-filter_complex', '"[0:v]' + ','.join(filter_cmnd).replace(',,', ',') + '"']

        if current_cmnd:
            if type(current_cmnd) is list:
                if len(current_cmnd) > 0:
                    real_cmnd += current_cmnd
            elif current_cmnd:
                real_cmnd.append(current_cmnd)

    # Prepare build sub cmnd
    profile_cmnd_template = list()
    profile_cmnd_keywords = list()
    for _key in SUB_CMND_TEMPLATE.split(' '):
        _key = str(_key).strip()
        profile_cmnd_template.append(_key)
        matches = re.findall(r'(<[a-zA-Z0-9_-]+>)', _key, re.IGNORECASE)
        if matches:
            for _key in matches:
                if _key not in profile_cmnd_keywords:
                    profile_cmnd_keywords.append(_key)

    short_file_id = str(str(file_data['id']).split('-')[0])
    short_mapper_keys_list = list()
    ##########################################################################################
    # Build multi sub profile cmnd
    ##########################################################################################
    sub_cmnd = list()
    set_framerate = False
    ####################################
    # In case transcode
    ####################################
    if file_data['encode_options'].get('pass_through_mode', None) in (None, False):
        n_count = 0
        for _video_profile_key in video_profile_data:
            # Find audio profile
            n_count += 1
            short_mapper_key = current_audio_profile = None
            mapping_alias = "\"[out%d]\"" % n_count
            current_video_profile = video_profile_data[_video_profile_key]
            for mapper_key, mapper_data in profile_mapper.items():
                if mapper_data['video_profile'] == _video_profile_key:
                    current_audio_profile = audio_profile_data[mapper_data['audio_profile']]
                    short_mapper_key = str(str(mapper_key).split('-')[0])
                    short_mapper_keys_list.append({
                        'postfix': short_mapper_key,
                        'bandwidth': current_video_profile['bitrate'] + '000'
                    })

                    # Check Set framerate
                    if not set_framerate and current_video_profile.get('framerate', False):
                        set_framerate = True
                        real_cmnd += ['-r', str(current_video_profile['framerate'])]

            # Build command line for live
            cmnd = ' '.join(profile_cmnd_template)
            for _key in profile_cmnd_keywords:
                if _key == '<mapping_alias>':
                    cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                    continue
                if _key == '<output>':
                    _temp = '<output_' + short_file_id + '_' + short_mapper_key + '>'
                    cmnd = cmnd.replace(_key, _temp)
                    continue
                if _key == '<live_output>':
                    # Update live output type
                    if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                    else:
                        # Default is rtmp if not supply absolute output path
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                    continue

                # Remove < and > to query attributes and replace
                search = re.match(r'.*<(video|audio)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                if search:
                    profile = current_audio_profile if search.group(1) == 'audio' else current_video_profile
                    if search.group(2) in profile:
                        _temp = float(search.group(3)) if search.group(3) else 1
                        if _temp > 1:
                            _temp = str(int(float(profile[search.group(2)]) * _temp))
                        else:
                            _temp = str(profile[search.group(2)])

                        # Feature: same as width or height
                        if (search.group(2).find('width') > -1 or search.group(2).find('height') > -1) and int(_temp) == 0:
                            _temp = str(file_data['standard_metadata']['videos'][0][search.group(2)])
                        cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
            sub_cmnd += cmnd.split(' ')

            # Build command line for dvr
            if has_dvr:
                n_count += 1
                mapping_alias = "\"[out%d]\"" % n_count
                cmnd = ' '.join(profile_cmnd_template)
                for _key in profile_cmnd_keywords:
                    if _key == '<mapping_alias>':
                        cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                        continue
                    if _key == '<output>':
                        _temp = '<dvr_output_' + short_file_id + '_' + short_mapper_key + '>'
                        cmnd = cmnd.replace(_key, _temp)
                        continue
                    if _key == '<live_output>':
                        # Update live output type
                        if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                        else:
                            # Default is rtmp if not supply absolute output path
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                        continue

                    # Remove < and > to query attributes and replace
                    search = re.match(r'.*<(video|audio)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                    if search:
                        profile = current_audio_profile if search.group(1) == 'audio' else current_video_profile
                        if search.group(2) in profile:
                            _temp = float(search.group(3)) if search.group(3) else 1
                            if _temp > 1:
                                _temp = str(int(float(profile[search.group(2)]) * _temp))
                            else:
                                _temp = str(profile[search.group(2)])

                            # Feature: same as width or height
                            if (search.group(2).find('width') > -1 or search.group(2).find('height') > -1) and int(_temp) == 0:
                                _temp = str(file_data['standard_metadata']['videos'][0][search.group(2)])
                            cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
                sub_cmnd += cmnd.split(' ')

    ####################################
    # In case video pass through
    ####################################
    elif file_data['encode_options']['pass_through_mode'] == 'video':
        n_count = 0
        live_bitrate = int(file_data['encode_options']['live_bitrate'])
        for _audio_profile_key in audio_profile_data:
            # Find audio profile
            n_count += 1
            mapping_alias = "\"[out%d]\"" % n_count
            current_audio_profile = audio_profile_data[_audio_profile_key]
            short_mapper_key = str(str(current_audio_profile['id']).split('-')[0])
            short_mapper_keys_list.append({
                'postfix': short_mapper_key,
                'bandwidth': str(live_bitrate + int(current_audio_profile['bitrate']))
            })

            # Build command line for live
            cmnd = ' '.join(profile_cmnd_template)
            for _key in profile_cmnd_keywords:
                if _key == '<mapping_alias>':
                    cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                    continue
                if _key == '<output>':
                    _temp = '<output_' + short_file_id + '_' + short_mapper_key + '>'
                    cmnd = cmnd.replace(_key, _temp)
                    continue
                if _key == '<live_output>':
                    # Update live output type
                    if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                    else:
                        # Default is rtmp if not supply absolute output path
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                    continue

                # Remove < and > to query attributes and replace
                search = re.match(r'.*<(audio)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                if search:
                    if search.group(2) in current_audio_profile:
                        _temp = float(search.group(3)) if search.group(3) else 1
                        if _temp > 1:
                            _temp = str(int(float(current_audio_profile[search.group(2)]) * _temp))
                        else:
                            _temp = str(current_audio_profile[search.group(2)])
                        cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
            sub_cmnd += cmnd.split(' ')

            # Build command line for dvr
            if has_dvr:
                n_count += 1
                mapping_alias = "\"[out%d]\"" % n_count
                cmnd = ' '.join(profile_cmnd_template)
                for _key in profile_cmnd_keywords:
                    if _key == '<mapping_alias>':
                        cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                        continue
                    if _key == '<output>':
                        _temp = '<dvr_output_' + short_file_id + '_' + short_mapper_key + '>'
                        cmnd = cmnd.replace(_key, _temp)
                        continue
                    if _key == '<live_output>':
                        # Update live output type
                        if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                        else:
                            # Default is rtmp if not supply absolute output path
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                        continue

                    # Remove < and > to query attributes and replace
                    search = re.match(r'.*<(audio)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                    if search:
                        if search.group(2) in current_audio_profile:
                            _temp = float(search.group(3)) if search.group(3) else 1
                            if _temp > 1:
                                _temp = str(int(float(current_audio_profile[search.group(2)]) * _temp))
                            else:
                                _temp = str(current_audio_profile[search.group(2)])
                            cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
                sub_cmnd += cmnd.split(' ')

    ####################################
    # In case audio pass through
    ####################################
    elif file_data['encode_options'].get('pass_through_mode', None) == 'audio':
        n_count = 0
        for _video_profile_key in video_profile_data:
            # Find audio profile
            n_count += 1
            mapping_alias = "\"[out%d]\"" % n_count
            current_video_profile = video_profile_data[_video_profile_key]
            short_mapper_key = str(str(_video_profile_key).split('-')[0])
            short_mapper_keys_list.append({
                'postfix': short_mapper_key,
                'bandwidth': current_video_profile['bitrate'] + '000'
            })

            # Check Set framerate
            if not set_framerate and current_video_profile.get('framerate', False):
                set_framerate = True
                real_cmnd += ['-r', str(current_video_profile['framerate'])]

            # Build command line for live
            cmnd = ' '.join(profile_cmnd_template)
            for _key in profile_cmnd_keywords:
                if _key == '<mapping_alias>':
                    cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                    continue
                if _key == '<output>':
                    _temp = '<output_' + short_file_id + '_' + short_mapper_key + '>'
                    cmnd = cmnd.replace(_key, _temp)
                    continue
                if _key == '<live_output>':
                    # Update live output type
                    if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                    else:
                        # Default is rtmp if not supply absolute output path
                        cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                    continue

                # Remove < and > to query attributes and replace
                search = re.match(r'.*<(video)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                if search:
                    if search.group(2) in current_video_profile:
                        _temp = float(search.group(3)) if search.group(3) else 1
                        if _temp > 1:
                            _temp = str(int(float(current_video_profile[search.group(2)]) * _temp))
                        else:
                            _temp = str(current_video_profile[search.group(2)])

                        # Feature: same as width or height
                        if (search.group(2).find('width') > -1 or search.group(2).find('height') > -1) and int(_temp) == 0:
                            _temp = str(file_data['standard_metadata']['videos'][0][search.group(2)])
                        cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
            sub_cmnd += cmnd.split(' ')

            # Build command line for dvr
            if has_dvr:
                n_count += 1
                mapping_alias = "\"[out%d]\"" % n_count
                cmnd = ' '.join(profile_cmnd_template)
                for _key in profile_cmnd_keywords:
                    if _key == '<mapping_alias>':
                        cmnd = cmnd.replace('<mapping_alias>', mapping_alias)
                        continue
                    if _key == '<output>':
                        _temp = '<dvr_output_' + short_file_id + '_' + short_mapper_key + '>'
                        cmnd = cmnd.replace(_key, _temp)
                        continue
                    if _key == '<live_output>':
                        # Update live output type
                        if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                        else:
                            # Default is rtmp if not supply absolute output path
                            cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                        continue

                    # Remove < and > to query attributes and replace
                    search = re.match(r'.*<(video)_([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                    if search:
                        if search.group(2) in current_video_profile:
                            _temp = float(search.group(3)) if search.group(3) else 1
                            if _temp > 1:
                                _temp = str(int(float(current_video_profile[search.group(2)]) * _temp))
                            else:
                                _temp = str(current_video_profile[search.group(2)])

                            # Feature: same as width or height
                            if (search.group(2).find('width') > -1 or search.group(2).find('height') > -1) and int(_temp) == 0:
                                _temp = str(file_data['standard_metadata']['videos'][0][search.group(2)])
                            cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
                sub_cmnd += cmnd.split(' ')

    ####################################
    # In case video + audio pass through
    #   WARNING: NOT SUPPORT DVR FOR THIS MODE
    ####################################
    else:
        # Build command line for live
        live_bitrate = str(file_data['encode_options']['live_bitrate'])
        cmnd = ' '.join(profile_cmnd_template)
        for _key in profile_cmnd_keywords:
            if _key == '<output>':
                _temp = '<output_' + short_file_id + "_%s>" % live_bitrate
                cmnd = cmnd.replace(_key, _temp)
            elif _key == '<live_output>':
                # Update live output type
                if file_data['encode_options'].get('live_supply_absolute_output_path', False):
                    cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type(file_data['output_uri'])))
                else:
                    # Default is rtmp if not supply absolute output path
                    cmnd = cmnd.replace(_key, ' '.join(get_live_output_stream_type('rtmp://')))
                continue
        sub_cmnd += cmnd.split(' ')

        short_mapper_keys_list.append({
            'postfix': live_bitrate,
            'bandwidth': live_bitrate
        })

    # return data
    real_cmnd += sub_cmnd
    return real_cmnd, short_mapper_keys_list


def build_live_info(information, workflow_data, use_GPU):
    profile_data = information['profile_data']
    # Build cmnd: input, encode options
    final_cmnd, short_mapper_keys_list =\
        build_live_encode_cmnd(file_data=information['file_info'],  profile_mapper=profile_data,
                               video_profile_data=information['video_profile_data'],
                               audio_profile_data=information['audio_profile_data'],
                               workflow_data=workflow_data, use_GPU=use_GPU)

    # Fix cmnd: replace output
    nginx_input_port = None
    output_root = information['file_info']['output_uri']
    print(final_cmnd)
    for idx, val in enumerate(final_cmnd):
        random_port = str(random.randint(50000, 60000) if not nginx_input_port else nginx_input_port)
        # Replace Live
        search = re.match(r'<output_([a-zA-Z0-9-]+)_([a-zA-Z0-9-]+)>', val)
        if search:
            if information['file_info']['encode_options'].get('live_supply_absolute_output_path', False):
                final_cmnd[idx] = output_root
            else:
                if is_multicast_stream(information['file_info']['uri']):
                    # Feature: multicast, random port because it use --net=host for docker
                    nginx_input_port = str(random.randint(50000, 60000) if not nginx_input_port else nginx_input_port)
                else:
                    nginx_input_port = str(1835)
                final_cmnd[idx] = 'rtmp://localhost:' + nginx_input_port + '/hls/' + search.group(1) + '_' + search.group(2)
            continue
        # Replace DVR
        search = re.match(r'<dvr_output_([a-zA-Z0-9-]+)_([a-zA-Z0-9-]+)>', val)
        if search:
            final_cmnd[idx] = 'rtmp://localhost:' + nginx_input_port + '/dvr_hls/' + search.group(1) + '_' + search.group(2)

    # Build variant playlist info for nginx
    variant_content = ''
    for val in short_mapper_keys_list:
        variant_content += "hls_variant\\ _%s\\ BANDWIDTH=%s; " % (val['postfix'], val['bandwidth'])

    final_cmnd = " ".join(final_cmnd)
    return final_cmnd, variant_content, nginx_input_port


def get_live_output_stream_type(live_output):
    live_output = str(live_output).lower()
    if live_output.startswith('rtmp://'):
        return ['-f', 'flv']
    elif live_output.startswith('rtp://'):
        return ['-f', 'rtp']
    elif live_output.startswith('rtsp://'):
        return ['-f', 'rtsp', '-rtsp_transport', 'tcp']
    elif live_output.startswith('udp://'):
        return ['-f', 'mpegts']
    elif live_output.startswith('tcp://'):
        return ['-f', 'mpegts']
    else:
        return ['-f', 'mpegts']


def build_startup_params(information, workflow_data, use_GPU=False):
    file_data = information['file_info']
    live_configs = CONFIGS['live_info'].copy()
    live_configs.update(workflow_data['feature']['metadata']['live_info'])
    has_dvr = file_data['encode_options'].get('dvr_enable', True)

    if file_data['encode_options']['live_info'].get('type', 'pull') == 'push' \
                and str(urllib.parse.urlparse(file_data['uri']).scheme).lower().find('rtmp') > -1:
        variant_info = "hls_variant\\ _%s\\ BANDWIDTH=%s;" % (str(file_data['encode_options']['live_bitrate']),
                                                               str(file_data['encode_options']['live_bitrate']))
        final_cmd, nginx_input_port = '', '9116'
    else:
        final_cmd, variant_info, nginx_input_port = build_live_info(information=information,
                                                                    workflow_data=workflow_data,
                                                                    use_GPU=use_GPU)

    # If need to be encrypted
    dvr_variant_info = str(variant_info)
    if 'ingest_mode' in information['file_info']['encode_options'] and \
            information['file_info']['encode_options']['ingest_mode']:
        encrypted_config = "\\ "
        # Encrypt HLS AES-128
        if 'aes128_drm_mode' in information['file_info']['encode_options']['ingest_mode']:
            encrypted_config += "hls_keys on;\\ hls_key_path\\ \/tmp\/data\/hls;\\ hls_fragments_per_key\\ 0;"
            if file_data['encode_options']['live_info'].get('type', 'pull') == 'push':
                encrypted_config += "\\ hls_key_url\\ \.\.\/;"
            else:
                encrypted_config += "\\ hls_key_url\\ \.\/;"
        variant_info += encrypted_config

        if has_dvr:
            encrypted_config = "\\ "
            # Encrypt HLS AES-128
            if 'aes128_drm_mode' in information['file_info']['encode_options']['ingest_mode']:
                encrypted_config += "hls_keys on;\\ hls_key_path\\ \/tmp\/data\/dvr_hls;\\ hls_fragments_per_key\\ 0;"
                if file_data['encode_options']['live_info'].get('type', 'pull') == 'push':
                    encrypted_config += "\\ hls_key_url\\ \.\.\/;"
                else:
                    encrypted_config += "\\ hls_key_url\\ \.\/;"
            dvr_variant_info += encrypted_config

    # Build docker name
    short_file_id = str(str(file_data['id']).split('-')[0])
    docker_name = "live_transcode_%s" % short_file_id
    # Build mapping path
    _temp = str(str(file_data['id']).split('-')[-1])
    outside_chunk_path = os.path.join(live_configs['chunk_hls_path'], _temp)
    inside_chunk_path = '/tmp/data/hls'
    dvr_outside_chunk_path = os.path.join(live_configs['chunk_dvr_hls_path'], _temp)
    dvr_inside_chunk_path = '/tmp/data/dvr_hls'
    network = {'__type__': 'nat'}
    env_var = dict()
    volume = dict()

    # Currently, support get pull stream and push stream (UDP / TCP)
    is_multicast_input_stream = False
    if 'live_info' not in file_data['encode_options'] or not file_data['encode_options']['live_info'] \
        or file_data['encode_options']['live_info'].get('type', 'pull') == 'pull':
        # in case no live_info supply, use pull method
        volume[str(outside_chunk_path)] = str(inside_chunk_path)
        if has_dvr:
            volume[str(dvr_outside_chunk_path)] = str(dvr_inside_chunk_path)
        # Check multicast / unicast
        try:
            is_multicast_input_stream = is_multicast_stream(file_data['uri'])
            if is_multicast_input_stream:
                # Issue of multicast: docker does not allow multicast packets pass through
                network['__type__'] = 'host'
        except ValueError:
            # in case: hostname is not IP Address
            pass
    else:
        # push method
        retrieve_port = str(urllib.parse.urlparse(file_data['uri']).port)
        volume[str(outside_chunk_path)] = str(inside_chunk_path)
        if has_dvr:
            volume[str(dvr_outside_chunk_path)] = str(dvr_inside_chunk_path)
        network.update({str(retrieve_port): '9116'})
        network.update({str(retrieve_port) + '/udp': '9116'})
    if not is_multicast_input_stream:
        env_var['NGINX_INPUT_PORT'] = str(nginx_input_port)

    seconds_per_chunk = str(file_data['encode_options'].get('live_seconds_per_chunk', '10'))

    # DVR Feature
    playlist_length = '60'
    if file_data['encode_options'].get('live_info', None):
        playlist_length = file_data['encode_options']['live_info'].get('live_playlist_length', '60')

    env_var.update({
        'FFMPEG_CMD': str(final_cmd), 'PLAYLIST_LENGTH': str(playlist_length),
        'SECONDS_PER_SEGMENT': str(seconds_per_chunk), 'VARIANT_INFO': str(variant_info),
    })
    data = {
        # Instance object info
        'name': docker_name,
        'hostname': docker_name.replace('_', '-'),
        'run_mode': Instance.M_DOCKER,
        'gpu_mode': Instance.G_AUTO_SETUP,
        'memory_mb': 1024*4,
        'cpu_mode': Instance.C_LIMITLESS,
        'volume': volume,
        'env_var': env_var,
        'network': network,
        'execute_info': {
            'docker_image': live_configs.get('docker_image_cpu', None),
            'docker_image_gpu': live_configs.get('docker_image_gpu', None),
            'docker_cmnd': None
        },
    }
    print(data)
    return data


@shared_task(bind=True, base=BaseTask)
def mux_live_stream_stage(self, input_params):
    error = 0
    workflow_data = retrieve_port = None
    junk_paths = list()

    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])
        file_data = information['file_info']
        has_dvr = file_data['encode_options'].get('dvr_enable', True)

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        _temp = str(str(file_data['id']).split('-')[-1])
        short_file_id = str(str(file_data['id']).split('-')[0])
        server_name = workflow_data['feature']['metadata']['live_info']['transmux_server']
        hls_stream = file_data['output_uri'].strip('/') + "/hls/%s/%s.m3u8" % (_temp, short_file_id)
        request_params = {
            'gpu': build_startup_params(information, workflow_data, use_GPU=True),
            'cpu': build_startup_params(information, workflow_data, use_GPU=False),
            # Extra info
            'instance_id': workflow_data['id'],
            'hls_stream': hls_stream,
            'server_name': server_name,
        }
        request_params.update({
            'name': request_params['cpu']['name'],
            'run_mode': request_params['cpu']['run_mode'],
            'gpu_mode': request_params['cpu']['gpu_mode'],
        })
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=60, file_id=input_params['file_id'],
                              workflow=workflow_data, message='Run encode video cmnd', extras=request_params)
        # END OF: Update progress

        add_instance_backjob.apply_async(args=[request_params, ])

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=70, message='Complete to run docker cmnd',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress

        current_percentage = 70
        delta_percentage = round((95 - current_percentage) / (len(information['live_tasks']) * 2), 2)
        for live_task in information['live_tasks']:
            live_task.update({
                'status': 'processing',
                'server_name': request_params['server_name'],
                'hls_stream': request_params['hls_stream'],
                'dash_stream': None,
                'smooth_stream': None,
            })
            current_percentage += delta_percentage
            # START OF: Update progress
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Update live encode task information',
                                  file_id=input_params['file_id'], workflow=workflow_data, live_task=live_task)
            # END OF: Update progress

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=current_percentage, message='Done',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        workflow_data['status'] = Workflow.S_SUCCESS
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if _temp:
                    if os.path.isfile(_temp):
                        os.remove(_temp)
                    elif os.path.isdir(_temp):
                        shutil.rmtree(_temp)