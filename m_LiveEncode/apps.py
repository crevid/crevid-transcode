import os
from django.conf import settings

__author__ = 'duybq'


CONFIGS = {
    "logo": {
        "top_right": {
            "cmnd": "overlay=main_w-overlay_w-<X>:<Y>"
        },
        "bottom_left": {
            "cmnd": "overlay=<X>:main_h-overlay_h-<Y>"
        },
        "bottom_right": {
            "cmnd": "overlay=(main_w-overlay_w-<X>):(main_h-overlay_h-<Y>)"
        },
        "top_left": {
            "cmnd": "overlay=<X>:<Y>"
        }
    },
    "progressive": {
        "scale": {
            "default": "",
            "optional": ""
        }
    },
    "interlaced": {
        "scale": {
            "default": "yadif=0:-1:0",
            "optional": "yadif=0:-1:0"
        }
    },
    "live_info": {
        "ssh_info": {
            "key_filename": os.path.join(settings.BASE_DIR, "Transcode", "keys", "id_rsa"),
            "host_string": "",
            "user": "duybq",
            "clean_revert": True,
            "disable_known_hosts": True,
        },
        "transmux_server": "",
        "chunk_hls_path": "/data/nginx/hls",
        "chunk_dvr_hls_path": "/data/nginx/dvr_hls",
        "docker_image_cpu": "transcode/livetranscode",
        "docker_image_gpu": "transcode/livetranscode-gpu",
        "docker_cmnd_template": "nvidia-docker run -d --name %s --restart unless-stopped -m 2g -e "
                                "FFMPEG_CMD=\"%s\" -e PLAYLIST_LENGTH=\"%s\" -e SECONDS_PER_SEGMENT=\"%s\" "
                                "-e VARIANT_INFO=\"%s\" transcode/livetranscode-gpu",

        #
        # For Live has Audio only
        #
        "live_cmnd_audio_only_part_1": "ffmpeg -re -loglevel error -nostats -hide_banner -i <input>",
        "live_cmnd_audio_only_part_2": "-map_metadata -1 -acodec libfdk_aac -ac 2 -ab <audio_bitrate>K "
                                       "-af volume=<audio_volume> <live_output> <output>",

        #
        # For Live has both Video & Audio
        #
        "live_cmnd_part_1": "ffmpeg -re -loglevel error -nostats -hide_banner -i <input> <video_scale>",
        "live_cmnd_part_2_gpu": "-map 0:a:0 -map <mapping_alias> -vcodec nvenc_h264 -vprofile <video_vprofile> "
                                "-s <video_width>:<video_height> -level <video_level> -preset <video_preset> "
                                "-b:v <video_bitrate>K -refs 4 -bf 3 -trellis 2 -b_strategy 2 -sc_threshold 0 "
                                "-g <video_keyint> -maxrate <video_bitrate_2>K -bufsize <video_bitrate_2>K "
                                "-pix_fmt yuv420p -map_metadata -1 -acodec libfdk_aac -ac 2 -ab <audio_bitrate>K "
                                "-af volume=<audio_volume> -strict -2 <live_output> -gpu 1 <output>",
        "live_cmnd_part_2_cpu": "-map 0:a:0 -map <mapping_alias> -vcodec libx264 -vprofile <video_vprofile> "
                                "-s <video_width>:<video_height> -level <video_level> -preset <video_preset> "
                                "-b:v <video_bitrate>K -refs 4 -bf 3 -trellis 2 -b_strategy 2 -sc_threshold 0 "
                                "-g <video_keyint> -maxrate <video_bitrate_2>K -bufsize <video_bitrate_2>K "
                                "-pix_fmt yuv420p -map_metadata -1 -acodec libfdk_aac -ac 2 -ab <audio_bitrate>K "
                                "-af volume=<audio_volume> -strict -2 <live_output> <output>",

        "live_cmnd_part_1_restream": "ffmpeg -re -loglevel error -nostats -hide_banner -i <input> <video_scale> ",
        "live_cmnd_part_2_restream_video": "-map 0:a:0 -map 0:v -vcodec copy -acodec libfdk_aac -ac 2 "
                                           "-ab <audio_bitrate>K -af volume=<audio_volume> -strict -2 <live_output> "
                                           "-map_metadata -1 <output>",
        "live_cmnd_part_2_restream_audio": "-map 0:a:0 -map <mapping_alias> -vcodec libx264 -vprofile <video_vprofile> "
                                           "-s <video_width>:<video_height> -level <video_level> -preset <video_preset> "
                                           "-b:v <video_bitrate>K -refs 4 -bf 3 -trellis 2 -b_strategy 2 -sc_threshold 0 "
                                           "-g <video_keyint> -maxrate <video_bitrate_2>K -bufsize <video_bitrate_2>K "
                                           "-pix_fmt yuv420p -acodec copy -map_metadata -1 "
                                           "<live_output> <output>",
        "live_cmnd_part_2_restream": "-c copy -map 0:v -map 0:a:0 -map_metadata -1 <live_output> <output>",

    }
}
