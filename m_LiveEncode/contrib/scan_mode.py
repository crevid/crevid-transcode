from ..apps import CONFIGS

__author__ = 'duybq'


def get_scan_type(encode_options, standard_metadata, configs=None):
    # Firstly, load configurations and auto-detect
    configs = configs if configs else CONFIGS
    scan_type = 'interlaced'
    method = 'default'
    if standard_metadata and standard_metadata.get('videos', None) and \
                    'interlaced' in standard_metadata['videos'][0] and standard_metadata['videos'][0]['interlaced']:
        scan_type = 'interlaced'
    #
    # Secondly, check encode options
    if 'force_progressive' in encode_options and encode_options['force_progressive']:
        scan_type = 'progressive'
    if 'force_interlaced' in encode_options and encode_options['force_interlaced']:
        scan_type = 'interlaced'
    if 'manual_scale' in encode_options and encode_options['manual_scale']:
        method = 'optional'
    #################################################################################
    return configs[scan_type]['scale'][method]