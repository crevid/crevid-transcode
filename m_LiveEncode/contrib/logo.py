import os
import tempfile

from api_core.contrib.storage import StorageAdapter
from api_core.contrib import utils

from ..apps import CONFIGS

__author__ = 'duybq'


def build_logo_parameters(input_cmnd, filter_cmnd, encode_options, configs=CONFIGS):
    configs = configs if configs else CONFIGS
    if 'logo' not in encode_options or 'X' not in encode_options['logo'] or 'Y' not in encode_options['logo'] \
            or 'position' not in encode_options['logo'] or encode_options['logo']['position'] not in configs['logo']:
        # Bad params
        return input_cmnd, filter_cmnd

    logo_cmnd = configs['logo'][encode_options['logo']['position']]['cmnd']
    if logo_cmnd and 'logo_uri' in encode_options['logo'] and encode_options['logo']['logo_uri']:
        # Prepare input
        input_cmnd = input_cmnd if type(input_cmnd) is list else [input_cmnd, ]
        logo_cmnd = str(logo_cmnd).replace('<X>', str(encode_options['logo']['X'])) \
            .replace('<Y>', str(encode_options['logo']['Y']))
        # TODO: Currently only support HTTP / HTTPS for logo
        if str(encode_options['logo']['logo_uri']).lower().startswith('http'):
            input_cmnd += ['-i', str(encode_options['logo']['logo_uri'])]
            filter_cmnd.append(logo_cmnd)

    return input_cmnd, filter_cmnd
