from __future__ import absolute_import

import os
import re
import shutil
import tempfile
import traceback
from celery import states, shared_task
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.base_task import BaseTask, request_file_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def encode_audio_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()

    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################

    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']
        template_data = information['template_data']
        audio_profile_data = information['audio_profile_data']

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Prepare input encode
        audio_inputs = list()
        temporary_src_paths = workflow_data['metadata']['input']['phys_audio_src_paths']
        current_percentage = 5
        delta_percentage = round((30 - current_percentage) / (len(temporary_src_paths) * len(audio_profile_data) * 2), 2)
        for _t_data in temporary_src_paths:
            if not os.path.isfile(_t_data['phys_path']):
                # Download file if not in local
                _t_data['phys_path'] = os.path.join(
                    tempfile.gettempdir(), 'transcode',
                    utils.generate_random_string(16) + os.path.split(_t_data['phys_path'])[-1])
                _temp = os.path.dirname(_t_data['phys_path'])
                if not os.path.isdir(_temp):
                    os.makedirs(_temp)
                current_percentage += delta_percentage
                # START OF: Update progress
                utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                      message='Copy file to temporary storage: ' + str(_t_data['uri_path']),
                                      file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
                # END OF: Update progress
                #
                StorageAdapter.copy_file(_t_data['uri_path'], 'file://' + _t_data['phys_path'])
                #
                # START OF: Update progress
                current_percentage += delta_percentage
                utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                      message='Copy finish: ' + str(_t_data['uri_path']),
                                      file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
                # END OF: Update progress
                junk_paths.append(_t_data['phys_path'])
            audio_inputs.append(_t_data)

        # Prepare output encode
        rel_output_dst_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
                                           utils.generate_random_string(8, str(workflow_data['id'])))
        phys_output_dst_path = os.path.join(settings.PERMANENTLY_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        uri_output_dst_path = os.path.join(settings.URL_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        temporary_dst_path = os.path.join(tempfile.gettempdir(), 'transcode', rel_output_dst_path)
        if not os.path.isdir(temporary_dst_path):
            os.makedirs(temporary_dst_path)

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=5, message='Update temporary metadata',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        output_paths = list()
        print('Build command-line and encode')
        current_percentage = 5
        delta_percentage = round((80 - current_percentage) / (len(audio_inputs) * len(audio_profile_data) * 2), 2)
        for _audio_input in audio_inputs:
            # Per file will be encoded to n profile(s)
            for _key in audio_profile_data:
                _info = {
                    'info': _audio_input['info']
                }
                profile = audio_profile_data[_key]
                # Build command line
                template_id = profile['template']
                cmnd = template_data[template_id]['pattern']
                for _key in template_data[template_id]['keyword']:
                    # Skip input and output tag
                    if _key in ('<input>', '<output>'):
                        continue
                    # Remove < and > to query attributes and replace
                    search = re.match(r'.*<([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                    if search:
                        if search.group(1) in profile:
                            _temp = float(search.group(2)) if search.group(2) else 1
                            if _temp > 1:
                                _temp = str(int(float(profile[search.group(1)]) * _temp))
                            else:
                                _temp = str(profile[search.group(1)])
                            cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
                cmnd = cmnd.split(' ')

                # Replace <input> / <output> tag and <scale>
                _filename = utils.generate_random_string(8) + '.' + profile['audio_type']
                _info.update({
                    'data_type': 'audio',
                    'profile_id': profile['id'],
                    'phys_output_dst_path': os.path.join(phys_output_dst_path, _filename),
                    'uri_output_dst_path': os.path.join(uri_output_dst_path, _filename),
                    'temporary_dst_path': os.path.join(temporary_dst_path, _filename),
                })
                junk_paths.append(_info['temporary_dst_path'])
                for idx, val in enumerate(cmnd):
                    if val == '<input>':
                        cmnd[idx] = _audio_input['phys_path']
                    elif val == '<output>':
                        cmnd[idx] = _info['temporary_dst_path']

                # Run encoding
                current_percentage += delta_percentage
                # START OF: Update progress
                utils.update_progress(self_task=self, error=error, percentage=current_percentage, file_id=file_data['id'],
                                      workflow=workflow_data, file_info=information['file_info'],
                                      message='Run audio cmnd', extras={'cmnd': ' '.join(cmnd)})
                # END OF: Update progress
                #
                utils.run_command_with_progress(cmnd=cmnd, timeout=60*60, percentage_init=current_percentage,
                                                percentage_factor=delta_percentage/100,
                                                callback_func=utils.update_progress,
                                                callback_params={'self_task': self, 'error': 0, 'percentage': 40,
                                                                 'message': '', 'file_id': file_data['id']})
                current_percentage += delta_percentage
                print(' '.join(cmnd))
                # If everything success, add it and notify back nearly real-time
                output_paths.append(_info)

                if not os.path.isfile(_info['temporary_dst_path']) \
                        or os.path.getsize(_info['temporary_dst_path']) < 1024*32:
                    raise Exception("Bad file output: %s - %s"
                                    % (_info['temporary_dst_path'], os.path.getsize(_info['temporary_dst_path'])))

        if not isinstance(workflow_data['metadata']['output'], list):
            workflow_data['metadata']['output'] = list()
        workflow_data['metadata']['output'].extend(e for e in output_paths if e not in workflow_data['metadata']['output'])

        current_percentage += 1
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                              message='Update temporary metadata',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress
        delta_percentage = round((95 - current_percentage) / (len(output_paths) * 2), 2)

        # Check sub process and Update file to output storage
        for _t_data in output_paths:
            try:
                # START OF: Update progress
                current_percentage += delta_percentage
                utils.update_progress(self_task=self, error=error, percentage=current_percentage,workflow=workflow_data,
                                      message='Upload output to: ' + str(_t_data['uri_output_dst_path']),
                                      file_id=input_params['file_id'],  file_info=information['file_info'])
                # END OF: Update progress
                #
                StorageAdapter.copy_file('file://' + _t_data['temporary_dst_path'], _t_data['uri_output_dst_path'])
                #
                # START OF: Update progress
                current_percentage += delta_percentage
                utils.update_progress(self_task=self, error=error, percentage=current_percentage,workflow=workflow_data,
                                      message='Finish upload output to: ' + str(_t_data['uri_output_dst_path']),
                                      file_id=input_params['file_id'],  file_info=information['file_info'])
                # END OF: Update progress
            except Exception as e:
                print(str(e.args))
                traceback.print_exc()
                raise

        workflow_data['status'] = Workflow.S_SUCCESS
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if os.path.isfile(_temp):
                    os.remove(_temp)
                elif os.path.isdir(_temp):
                    shutil.rmtree(_temp)