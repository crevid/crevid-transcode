try:
    import ujson as json
except ImportError:
    import json
import os
import time
from urllib.parse import urlparse
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response

from api_core.contrib import utils, exceptions
from api_core.define import RESPONSE_SUCCESS_MSG
from api_core.contrib.views import APIViewBase, APIVersion

from Controller.models import *
from Controller.tasks.driven import switch_task

__author__ = 'duybq'


class SubmitRequestView(APIViewBase):
    """
    Retrieve request to transcode file

    This API will receive information from client to transcode file.

    <h1>Request info</h1>
    <ul><p><u>Body JSON attributes</u>:
        <li>input_uris: (required) list of uri
                        (example&#58; ftp://duybq:16%2F05%2F91@localhost/duybq-input/big_buck_bunny_720p_10mb.mp4)</li>
        <li>features: (required) feature code list</li>
        <li>profile_mappers: (required) list of profiles to be transcode</li>
        <li>output_uri: (required) the full output uri folder</li>
        <li>owner: (optional) use it to filter the owner of file</li>
        <li>encode_options: (optional) use it to filter the owner of file
            <ul>
                <li>live_mode: (required) False for default. If True, the input_uris must be HTTP Stream or RTMP Stream</li>
                <li>load_input_from_external_service: (required) true if you want to download source from Youtube.
                                                      false for normal URI location (default)
                </li>
                <li>external_input_quality: (optional) the quality of video from external service you want to encode from.
                                            Default: the best quality</li>
                <li>force_progressive: (optional) for encode video with scan type progressive. Default: false.</li>
                <li>force_interlaced: (optional) for encode video with scan type interlaced. Default: false.</li>
                <li>manual_scale: (optional) false if auto scale by height by width (to keep ratio).
                                  true for forcing scale by height and video pixels</li>
                <li>logo: (optional) enable this option to embed logo to video
                    <ul>
                        <li>position: (required), one of these value: top_right, bottom_left, bottom_right, top_left</li>
                        <li>X: (required), number of pixel on X axis from position in integer</li>
                        <li>Y: (required), number of pixel on Y axis from position in integer</li>
                        <li>logo_uri: (required), the full uri to logo</li>
                    </ul>
                </li>
                <li>ingest_mode: required if ingestion module is enabled, array of values:
                    <ul>
                        <li>non_drm_mode: Non-DRM</li>
                        <li>drmtoday_mode: DRM with DTMToday</li>
                        <li>buydrm_mode: DRM with BuyDRM</li>
                    </ul>
                </li>
                <li>ingest_type: required if ingestion module is enabled, array of values:
                    <ul>
                        <li>hls: to enable HLS</li>
                        <li>dash: to enable DASH</li>
                        <li>smooth_streaming: to enable Smooth Streaming</li>
                    </ul>
                </li>
                <li>ingest_output_uri: required if ingestion module is enabled, </li>
                <li>callback_id: optionally, it will be sent when notify enabled </li>
                <li>callback_url: optionally, it will be sent when notify enabled </li>
            </ul>
        </li>
    </p></ul>
    ---
    POST:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: false
          type: dictionary, array or null
          description: null value
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: body
          paramType: body
          description: JSON string
          required: true
          type: Example&#58; '{
                                "input_uris"&#58; [
                                    "https&#58;//www.youtube.com/watch?v=rvLmCH4HLgE"
                                ],
                                "features"&#58; ["pre_progress_stage"],
                                "profile_mappers"&#58; [
                                    "b644833d-e0d8-43c5-880b-9559d4cf620c"
                                ],
                                "encode_options"&#58; {
                                    "load_input_from_external_service"&#58; true,
                                    "external_input_quality"&#58; "best"
                                },
                                "output_uri"&#58; "ftp&#58;//demo&#58;demopassword@demo-ip/output/"
                            }'
      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 429
          message: Too many requests in ip address for pair entity_id and visitor_id in 1 minute.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    # Setting REST configurations
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.submit_request_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'input_uris': list(),
            'output_uri': None,
            'features': list(),
            'encode_options': {
                'live_mode': False,
                'load_input_from_external_service': False,
                'external_input_quality': 'best',
                'force_progressive': False,
                'force_interlaced': False,
                'manual_scale': True,
                'callback_id': None,
                # 'logo': {
                #     'position': 'top_right',
                #     'X': '10',
                #     'Y': '10',
                #     'logo_uri': None,
                # },
                # 'ingest_mode': list(),
                # 'ingest_type': list(),
            },
            'profile_mappers': list(),      # List of profile mapper IDs
            'owner': None,
        }
        params.update(request.data)
        params.update({
            'ip': utils.get_client_ip(request),
            'ua': request.META['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in request.META else '',
        })
        if not params['features'] or not params['profile_mappers'] \
                or not params['input_uris'] or not params['output_uri']:
            raise exceptions.ParamDoesNotExist
        return params

    def submit_request_1_0(self, request, dispatch_params):
        """
        :param request: This is the request context of django rest framework
        :type request: rest_framework.request.Request
        """
        # Query profiles
        params = self.get_input_params(request, dispatch_params)
        feature_condition = ~Q(disabled=True) & (Q(code__in=params['features']) | Q(mode=Feature.M_FORCE))
        feature_queryset = Feature.objects.filter(feature_condition).order_by('priority')

        # Parse profile request information
        audio_profiles = list()
        audio_ids = list()
        video_profiles = list()
        video_ids = list()
        profile_mappers = list()
        profile_queryset = ProfileMapper.objects.filter(~Q(disabled=True) & Q(id__in=params['profile_mappers']))
        for profile_obj in profile_queryset:
            audio_profiles.append({'all': str(profile_obj.audio_profile.pk)})
            audio_ids.append(str(profile_obj.audio_profile.pk))
            video_profiles.append({'all': str(profile_obj.video_profile.pk)})
            video_ids.append(str(profile_obj.video_profile.pk))
            profile_mappers.append(str(profile_obj.pk))

        # Save information
        result = list()
        audio_profiles = [str(e.id) for e in AudioProfile.objects.filter(Q(id__in=audio_ids)).only('id')]
        video_profiles = [str(e.id) for e in VideoProfile.objects.filter(Q(id__in=video_ids)).only('id')]
        for uri in params['input_uris']:
            filename = ''.join((e if e.isalnum() else '_') for e in os.path.basename(uri))
            file_obj = FileInfo.objects.create(uri=uri, name=filename, output_uri=params['output_uri'])
            file_obj.owner = params['owner']
            file_obj.encode_options = params['encode_options']
            file_obj.save()
            for feature_obj in feature_queryset:
                workflow = Workflow.objects.create(file=file_obj, feature=feature_obj, priority=feature_obj.priority)
                if str(feature_obj.code).startswith(Feature.PREFIX_FT_ENCODE):
                    workflow.metadata['profile_mappers'] = profile_mappers
                    if str(feature_obj.code).lower().find('audio') > -1:
                        workflow.metadata['audio_profiles'] = audio_profiles
                    elif str(feature_obj.code).lower().find('video') > -1:
                        workflow.metadata['video_profiles'] = video_profiles
                elif str(feature_obj.code).startswith(Feature.PREFIX_FT_MUX):
                    if 'profile_mappers' not in workflow.metadata or not workflow.metadata['profile_mappers']:
                        workflow.metadata['profile_mappers'] = list()
                    workflow.metadata['profile_mappers'] = profile_mappers
                workflow.save()

            # for live mode
            _temp = {'file_id': str(file_obj.pk), 'uri': uri}
            if 'live_mode' in params['encode_options'] and params['encode_options']['live_mode']:
                for profile_id in profile_mappers:
                    live_task = LiveEncodeTask.objects.create(file=file_obj, profile_id=profile_id,
                                                              status=LiveEncodeTask.S_INITIAL)

                short_file_id = str(str(file_obj.pk).split('-')[0])
                short_parent_folder = str(str(file_obj.pk).split('-')[-1])
                hls_stream = params['output_uri'].strip('/') + "/hls/%s/%s.m3u8" \
                             % (short_parent_folder, short_file_id)
                _temp.update({'hls_stream': hls_stream})
            result.append(_temp)
            switch_task(excluded_priority=None, file_id=str(file_obj.pk))
        return result


class SubmitCustomRequestView(APIViewBase):
    """
    Retrieve request to transcode file

    This API will receive information from client to transcode file.

    <h1>Request info</h1>
    <ul><p><u>Body JSON attributes</u>:
        <li>input_uris: (required) list of uri or list of dict {'uri': ..., 'filename': ..., 'live_info': null, 'task_priority': ...}.
                        task_priority is an integer from 0 (lowest) to 10 (highest), default is 3.
                        live_info is null or ex: {"type": "push", "port": null}. This field is only valuable when
                        livetranscoding with input has UDP scheme. Type may be: pull or push with port is the gateway receives signals
                        (example&#58; ftp://duybq:16%2F05%2F91@localhost/duybq-input/big_buck_bunny_720p_10mb.mp4)</li>
        <li>features: (required) feature code list</li>
        <li>profile_data: (required) list of mapper profiles to be transcode</li>
        <li>profile_data: (optional) the mapping between audio and video profiles</li>
        <li>audio_profile_data: (optional) list of audio profiles to be transcode</li>
        <li>video_profile_data: (optional) list of video profiles to be transcode</li>
        <li>output_uri: (required) the full output uri folder</li>
        <li>owner: (optional) use it to filter the owner of file</li>
        <li>encode_options: (optional) use it to filter the owner of file
            <ul>
                <li>live_mode: (required) False for default. If True, the input_uris must be HTTP Stream or RTMP Stream</li>
                <li>live_playlist_length: (optional) 30 for default, the duration of playlist in seconds </li>
                <li>live_bitrate: (optional) 2000000 for default. Set livestream bitrate for copy mode</li>
                <li>live_device_mode: (optional) auto for default. Device that live transcode uses: gpu, cpu, auto</li>
                <li>live_supply_absolute_output_path: (optional) false for default. If it is set true, the output_uri field will
                                                        present for the definitely destination output live steam</li>
                <li>dar: (optional) auto for default. the value is: auto, 16/9, 21/9, 4/3</li>
                <li>pass_through_mode: (optional) null for default. There are 4 options:
                    <ul>
                        <li>null: transcode. It uses video_profile_data, audio_profile_data and profile_mappers</li>
                        <li>audio: passthrough audio and encode video. It uses video_profile_data fields only</li>
                        <li>video: passthrough video and encode audio. It uses audio_profile_data fields only</li>
                        <li>both: passthrough audio and video. It does not use any data profiles</li>
                    </ul>
                </li>
                <li>load_input_from_external_service: (required) true if you want to download source from Youtube.
                                                      false for normal URI location (default)
                </li>
                <li>external_input_quality: (optional) the quality of video from external service you want to encode from.
                                            Default: the best quality</li>
                <li>force_progressive: (optional) for encode video with scan type progressive. Default: false.</li>
                <li>force_interlaced: (optional) for encode video with scan type interlaced. Default: false.</li>
                <li>manual_scale: (optional) false if auto scale by height by width (to keep ratio).
                                  true for forcing scale by height and video pixels</li>
                <li>logo: (optional) enable this option to embed logo to video
                    <ul>
                        <li>position: (required), one of these value: top_right, bottom_left, bottom_right, top_left</li>
                        <li>X: (required), number of pixel on X axis from position in integer</li>
                        <li>Y: (required), number of pixel on Y axis from position in integer</li>
                        <li>logo_uri: (required), the full uri to logo</li>
                    </ul>
                </li>
                <li>subtitle: (optional) enable this option to hardsub
                    <ul>
                        <li>input_uri: (required), the full uri to subtitle (allow SRT and ASS file ext)</li>
                    </ul>
                </li>
                <li>crop: (optional) enable this option to crop video
                    <ul>
                        <li>top: (optional), default is 0, number of pixel from top</li>
                        <li>bottom: (optional), default is 0, number of pixel from bottom</li>
                        <li>right: (optional), default is 0, number of pixel from right</li>
                        <li>left: (optional), default is 0, number of pixel from left</li>
                    </ul>
                </li>
                <li>ingest_mode: required if ingestion module is enabled, array of values:
                    <ul>
                        <li>non_drm_mode: Non-DRM</li>
                        <li>drmtoday_mode: DRM with DTMToday</li>
                        <li>buydrm_mode: DRM with BuyDRM</li>
                    </ul>
                </li>
                <li>ingest_type: required if ingestion module is enabled, array of values:
                    <ul>
                        <li>hls: to enable HLS</li>
                        <li>dash: to enable DASH</li>
                        <li>smooth_streaming: to enable Smooth Streaming</li>
                    </ul>
                </li>
                <li>ingest_output_uri: required if ingestion module is enabled, </li>
                <li>callback_id: optionally, it will be sent when notify enabled </li>
                <li>ingest_callback_url: optionally, it will be sent when notify enabled </li>
            </ul>
        </li>
    </p></ul>
    ---
    POST:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: false
          type: dictionary, array or null
          description: null value
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: body
          paramType: body
          description: JSON string
          required: true
          type: Example&#58; '{
                                "input_uris"&#58; [
                                    "https&#58;//www.youtube.com/watch?v=rvLmCH4HLgE"
                                ],
                                "features"&#58; ["pre_progress_stage"],
                                "profile_mappers"&#58; [
                                    "b644833d-e0d8-43c5-880b-9559d4cf620c"
                                ],
                                "encode_options"&#58; {
                                    "load_input_from_external_service"&#58; true,
                                    "external_input_quality"&#58; "best"
                                },
                                "output_uri"&#58; "ftp&#58;//demo&#58;demopassword@demo-ip/output/"
                            }'
      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 429
          message: Too many requests in ip address for pair entity_id and visitor_id in 1 minute.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    # Setting REST configurations
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.submit_request_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'input_uris': list(),
            'output_uri': None,
            'features': list(),
            'encode_options': {
                'live_mode': False,
                'live_bitrate': 2000000,
                'live_device_mode': 'auto',
                'live_supply_absolute_output_path': False,
                'pass_through_mode': None,
                'load_input_from_external_service': False,
                'external_input_quality': 'best',
                'force_progressive': False,
                'force_interlaced': False,
                'manual_scale': True,
                'callback_id': None,
                # 'logo': {
                #     'position': 'top_right',
                #     'X': '10',
                #     'Y': '10',
                #     'logo_uri': None,
                # },
                # "subtitle": {
                #     "input_uri": "http://localhost/static/dragon_10min.srt"
                # },
                # "crop": {
                #     "top": 0,
                #     "right": 0,
                #     "bottom": 0,
                #     "left": 0
                # }
                # 'ingest_mode': list(),
                # 'ingest_type': list(),
            },
            'profile_data': dict(),
            'audio_profile_data': dict(),
            'video_profile_data': dict(),
            'owner': None,
        }
        _temp = request.data['encode_options'].copy()
        del request.data['encode_options']
        params.update(request.data)
        params['encode_options'].update(_temp)
        params.update({
            'ip': utils.get_client_ip(request),
            'ua': request.META['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in request.META else '',
            # hard code live_playlist_length
            'live_playlist_length': 24*60*60
        })
        if not params['features'] or not params['profile_data'] or not params['audio_profile_data'] \
                or not params['video_profile_data'] or not params['input_uris'] or not params['output_uri']:

            # Feature: copy stream, not send profile
            if not((not params['profile_data']) and (not params['audio_profile_data'])
                   and (not params['video_profile_data'])) and (not params['encode_options']['pass_through_mode']):
                raise exceptions.ParamDoesNotExist
        return params

    def submit_request_1_0(self, request, dispatch_params):
        """
        :param request: This is the request context of django rest framework
        :type request: rest_framework.request.Request
        """
        # Query profiles
        params = self.get_input_params(request, dispatch_params)
        feature_condition = ~Q(disabled=True) & (Q(code__in=params['features']) | Q(mode=Feature.M_FORCE))
        feature_queryset = Feature.objects.filter(feature_condition).order_by('priority')

        # Parse profile request information
        if 'pass_through_mode' in params['encode_options'] and params['encode_options']['pass_through_mode']:
            profile_mappers = []
            if str(params['encode_options']['pass_through_mode']).lower() == 'video':
                video_profiles = []
                audio_profiles = [_key for _key in params['audio_profile_data']]
            elif str(params['encode_options']['pass_through_mode']).lower() == 'audio':
                audio_profiles = []
                video_profiles = [_key for _key in params['video_profile_data']]
            else:
                video_profiles = []
                audio_profiles = []
        else:
            profile_mappers = [_key for _key in params['profile_data']]
            video_profiles = [_key for _key in params['video_profile_data']]
            audio_profiles = [_key for _key in params['audio_profile_data']]

        # Save information
        result = list()
        redis_cache = utils.AdvancedRedisCache()
        template_progress_data = json.dumps({'percentage': 0, 'extras': {}})
        for uri_info in params['input_uris']:
            filename = None
            live_info = None
            task_priority = min(max(int(uri_info.get('task_priority', 3)), 0), 10)
            if type(uri_info) is dict:
                uri = uri_info['uri']
                live_info = uri_info.get('live_info', None)
                if 'filename' in uri_info:
                    filename, filext = os.path.splitext(uri_info['filename'])
                    filename = ''.join((e if e.isalnum() else '_') for e in filename) + filext
            else:
                uri = uri_info
            file_obj = FileInfo.objects.create(uri=uri, name=filename, output_uri=params['output_uri'])
            file_obj.owner = params['owner']
            file_obj.encode_options = params['encode_options']
            file_obj.encode_options.update({'live_info': live_info})
            file_obj.save()

            # Save custom profile
            extra_option = CustomEncodingOption.objects.create(
                file=file_obj, profile_data=params['profile_data'],
                audio_profile_data=params['audio_profile_data'], video_profile_data=params['video_profile_data'])

            # Build progress cache
            cache_progress_key = FileInfo.get_progress_key(str(file_obj.pk))
            cache_progress_data = dict()
            # Build workflow
            for feature_obj in feature_queryset:
                workflow = Workflow.objects.create(file=file_obj, feature=feature_obj, priority=feature_obj.priority)
                if str(feature_obj.code).startswith(Feature.PREFIX_FT_ENCODE):
                    workflow.metadata['profile_mappers'] = profile_mappers
                    if str(feature_obj.code).lower().find('audio') > -1:
                        workflow.metadata['audio_profiles'] = audio_profiles
                    elif str(feature_obj.code).lower().find('video') > -1:
                        workflow.metadata['video_profiles'] = video_profiles
                elif str(feature_obj.code).startswith(Feature.PREFIX_FT_MUX):
                    if 'profile_mappers' not in workflow.metadata or not workflow.metadata['profile_mappers']:
                        workflow.metadata['profile_mappers'] = list()
                    workflow.metadata['profile_mappers'] = profile_mappers
                workflow.metadata['task_priority'] = task_priority
                workflow.save()
                # Set progress cache
                cache_progress_data.update({str(workflow.pk): template_progress_data})

            # for live mode
            _temp = {'file_id': str(file_obj.pk), 'uri': uri}
            if 'live_mode' in params['encode_options'] and params['encode_options']['live_mode']:
                live_task = LiveEncodeTask.objects.create(file=file_obj, status=LiveEncodeTask.S_INITIAL)

                if file_obj.encode_options.get('live_supply_absolute_output_path', False):
                    hls_stream = file_obj.output_uri
                else:
                    if file_obj.encode_options['live_info'].get('type', 'pull') == 'push' \
                            and str(urlparse(file_obj.uri).scheme).lower().find('rtmp') > -1:
                        short_file_id = file_obj.name + '_' + str(file_obj.encode_options['live_bitrate']) + '/index'
                    else:
                        short_file_id = str(str(file_obj.pk).split('-')[0])
                    short_parent_folder = str(str(file_obj.pk).split('-')[-1])
                    hls_stream = params['output_uri'].strip('/') \
                                 + "/hls/%s/%s.m3u8" % (short_parent_folder, short_file_id)
                _temp.update({'hls_stream': hls_stream})
            result.append(_temp)
            redis_cache.hmset(cache_progress_key, cache_progress_data, 60*60*24*2)
            switch_task(excluded_priority=None, file_id=str(file_obj.pk))
        return result
