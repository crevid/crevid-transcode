try:
    import ujson as json
except ImportError:
    import json
import time
import urllib.parse
from django.db.models import Q
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.response import Response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from api_core.contrib import utils, exceptions
from api_core.define import RESPONSE_SUCCESS_MSG
from api_core.contrib.views import APIViewBase, APIVersion

from ..serializers import *

__author__ = 'duybq'


class FileDetailView(APIViewBase):
    """
    Get detail of file job by id

    This API support to get detail of file job by id, include: general file info, workflow, profile,...

    <h1>Request info</h1>
    <ul><p><u>GET param</u>:
        <li>file_id: (required) the file ID</li>
    </p></ul>
    ---
    GET:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: false
          type: dictionary, array or null
          description: null value
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string
        - name: file_id
          paramType: query
          description: File ID
          required: true
          type: string

      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    authentication_classes = ()
    permission_classes = ()

    def get(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.generate_file_detail_info_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'file_id': request.GET.get('file_id', None),
        }
        params.update({
            'ip': utils.get_client_ip(request),
            'ua': request.META['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in request.META else '',
        })
        if not params['file_id']:
            raise exceptions.ParamDoesNotExist
        return params

    def generate_file_detail_info_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        file_info = FileInfoSerializer(FileInfo.objects.get(pk=params['file_id'])).data
        workflow_queryset = Workflow.objects.filter(Q(file_id=params['file_id']) & ~Q(disabled=True))\
                                            .order_by('priority', 'created_date')
        profile_ids = list()
        video_profile_ids = list()
        audio_profile_ids = list()
        template_ids = list()
        workflow = list()
        playlist_resource_id = None
        for _w in workflow_queryset:
            workflow.append(WorkflowShortSerializer(_w).data)
            _temp = WorkflowSerializer(_w).data
            if 'playlist_resource_id' in _temp['metadata']['output']:
                playlist_resource_id = _temp['metadata']['output']['playlist_resource_id']
            if str(_temp['feature']['code']).startswith(Feature.PREFIX_FT_ENCODE):
                profile_ids += _temp['metadata']['profile_mappers']
                if 'video_profiles' in _temp['metadata'] and _temp['metadata']['video_profiles']:
                    video_profile_ids += _temp['metadata']['video_profiles']
                if 'audio_profiles' in _temp['metadata'] and _temp['metadata']['audio_profiles']:
                    audio_profile_ids += _temp['metadata']['audio_profiles']

        result = {
            'file_info': file_info,
            'workflow': workflow,
            'profile_data': None,
            'video_profile_data': None,
            'audio_profile_data': None,
            'playlist_resource_id': playlist_resource_id
        }

        if profile_ids:
            valid_uuid = [_temp for _temp in profile_ids if _temp and len(_temp) >= 32]
            profiles = ProfileMapper.objects.filter(Q(id__in=valid_uuid) & ~Q(disabled=True))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = ProfileMapperShortSerializer(profile).data
            result.update({'profile_data': profile_data})

        if video_profile_ids:
            valid_uuid = [_temp for _temp in video_profile_ids if _temp and len(_temp) >= 32]
            profiles = VideoProfile.objects.filter(Q(id__in=valid_uuid))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = VideoProfileShortSerializer(profile).data
                template_ids.append(profile_data[str(profile.pk)]['template'])
            result.update({'video_profile_data': profile_data})

        if audio_profile_ids:
            valid_uuid = [_temp for _temp in audio_profile_ids if _temp and len(_temp) >= 32]
            profiles = AudioProfile.objects.filter(Q(id__in=valid_uuid))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = AudioProfileShortSerializer(profile).data
                template_ids.append(profile_data[str(profile.pk)]['template'])
            result.update({'audio_profile_data': profile_data})

        if 'live_mode' in file_info['encode_options'] and file_info['encode_options']['live_mode']:
            live_tasks = list()
            for e in LiveEncodeTask.objects.filter(file_id=file_info['id']):
                live_tasks.append(LiveEncodeTaskSerializer(e).data)
            result.update({'live_tasks': live_tasks})

        return result


class FileSummaryListView(APIViewBase):
    """
    Get list of files API

    This API returns a list of list of file with the filters (status, search_key, owner), order and pagination.

    <h1>Request info</h1>
    <ul><p><u>GET params</u>:
        <li>search_key: (optional) filter by a string</li>
        <li>filter_owner: (optional) filter by a an owner id</li>
        <li>filter_status: (optional), a list of status id separated by commas. The status are:
            <ul>
                <ul><li>0: initial</li>
                <li>1: success</li>
                <li>2: processing</li>
                <li>-1: failure</li>
            </ul>
        </li>
        <li><u>order_by</u>: (optional), choose one of these:
            <ul>
                <li>-created_date: sort descending by created_date (default)</li>
                <li>created_date: sort ascending by created_date</li>
                <li>-size: sort descending by sort</li>
                <li>size: sort descending by</li>
                <li>-name: sort descending by name</li>
                <li>name: sort descending by name</li>
            </ul>
        </li>
        <li>items_per_page: (optional) the visitor ID, default is 30</li>
        <li>page: (optional) the visitor ID, default is 1</li>
    </p></ul>
    ---
    GET:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: true
          type: dictionary
          description: dictionary
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: search_key
          paramType: query
          description: Search key
          required: false
          type: string
        - name: filter_owner
          paramType: query
          description: Filter by an owner string
          required: false
          type: string
        - name: filter_status
          paramType: query
          description: Leave blank if query all. Can be sent multiple status separated by commas.
          required: false
          type: Example&#58; 1,0
        - name: order_by
          paramType: query
          description: Leave blank if order by created_date. See description abow.
          required: false
          type: Example&#58; -created_date
        - name: items_per_page
          paramType: query
          description: Maximum items per page, default is 30.
          required: false
          type: Example&#58; 30
        - name: page
          paramType: query
          description: Page number, default is 1.
          required: false
          type: Example&#58; 1

      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    authentication_classes = ()
    permission_classes = ()

    def get(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.generate_file_list_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'filter_owner': request.GET.get('filter_owner', None),
            'filter_status': request.GET.get('filter_status', None),
            'order_by': request.GET.get('order_by', '-created_date'),
            'items_per_page': request.GET.get('items_per_page', 30),
            'search_key': request.GET.get('search_key', None),
            'page': request.GET.get('page', 1)
        }
        if 'filter_status' in params and params['filter_status']:
            params['filter_status'] = str(params['filter_status']).split(',')
        try:
            params['items_per_page'] = int(params['items_per_page'])
            params['page'] = int(params['page'])
        except ValueError:
            raise exceptions.InvalidParam
        params['items_per_page'] = min(max(int(params['items_per_page']), 1), 100)
        return params

    def generate_file_list_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        condition = ~Q(disabled=True)
        if 'filter_owner' in params and params['filter_owner']:
            condition &= Q(owner__icontains=params['filter_owner'])
        if 'filter_status' in params and params['filter_status']:
            condition &= Q(condition__in=str(params['filter_status']).split(','))
        if 'search_key' in params and params['search_key']:
            condition &= Q(uri__icontains=params['search_key'])
        if 'order_by' in params and params['order_by']:
            fileinfo_queryset = FileInfo.objects.filter(condition).order_by(params['order_by'])
        else:
            fileinfo_queryset = FileInfo.objects.filter(condition)

        paginator = Paginator(fileinfo_queryset, params['items_per_page'])
        try:
            page_data = paginator.page(params['page'])
        except PageNotAnInteger:
            params['page'] = 1
            page_data = paginator.page(params['page'])
        except EmptyPage:
            params['page'] = paginator.num_pages
            page_data = paginator.page(paginator.num_pages)

        files_data = list()
        for _temp in page_data.object_list:
            _temp = FileInfoShortSerializer(_temp).data
            _temp.update({
                'detail_url': request.build_absolute_uri(reverse('file_detail_view'))
                                + '?' + urllib.parse.urlencode({'file_id': _temp['id']})
            })
            files_data.append(_temp)

        result = {
            'items_per_page': params['items_per_page'],
            'page': params['page'],
            'max_page': paginator.num_pages,
            'num_items': len(files_data),
            'result': files_data,
        }
        for _temp in params.copy():
            if params[_temp] is None:
                del params[_temp]
        try:
            params['page'] = page_data.next_page_number()
            result.update({
                'next_url': request.build_absolute_uri(reverse('file_summary_list_view'))
                                + '?' + urllib.parse.urlencode(params)
            })
        except EmptyPage:
            result.update({
                'next_url': request.build_absolute_uri(reverse('file_summary_list_view'))
                                + '?' + urllib.parse.urlencode(params)
            })
        try:
            params['page'] = page_data.previous_page_number()
            result.update({
                'previous_url': request.build_absolute_uri(reverse('file_summary_list_view'))
                                    + '?' + urllib.parse.urlencode(params)})
        except EmptyPage:
            result.update({
                'previous_url': request.build_absolute_uri(reverse('file_summary_list_view'))
                                    + '?' + urllib.parse.urlencode(params)})
        return result


class FileProgressView(APIViewBase):
    """
    Get progress of file(s)

    This API will receive information from client to transcode file.

    <h1>Request info</h1>
    <ul><p><u>Body JSON attributes</u>:
        <li>file_ids: (required) A list of file ids, maximum is 50.</li>
        <li>with_extras: (optional) Default is False.</li>
    </p></ul>
    ---
    POST:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: false
          type: dictionary, array or null
          description: null value
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: body
          paramType: body
          description: JSON string
          required: true
          type: Example&#58; '{
                                "file_ids"&#58; [
                                    "7f0e609d-9043-448d-b35d-f86b9aa4cd3e",
                                    "8f92bc77-586f-4c2f-81bf-a528a5176555"
                                ]
                            }'
      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.get_file_progress_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_200_OK)

    def get_input_params(self, request, dispatch_params):
        params = {
            'file_ids': [],
            'with_extras': False
        }
        params.update(request.data)
        if ('file_ids' not in params) or (not params['file_ids']) or (not isinstance(params['file_ids'], list)):
            raise exceptions.ParamDoesNotExist
        if len(params['file_ids']) > 50:
            raise exceptions.InvalidParam
        return params

    def get_file_progress_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        redis_cache = utils.AdvancedRedisCache()
        results = dict()
        for file_id in params['file_ids']:
            file_id = str(file_id)
            if len(file_id) < 1 or len(file_id) > 36:
                continue
            # process data
            results[file_id] = {'detail': {}, 'total_percentage': 0}
            results[file_id]['detail'] = redis_cache.hgetall(FileInfo.get_progress_key(file_id))
            if results[file_id]['detail']:
                _temp = 0
                for task_id in results[file_id]['detail'].keys():
                    results[file_id]['detail'][task_id] = json.loads(results[file_id]['detail'][task_id])
                    if not params['with_extras'] and 'extras' in results[file_id]['detail'][task_id]:
                        del results[file_id]['detail'][task_id]['extras']
                    _temp += results[file_id]['detail'][task_id]['percentage']
                results[file_id]['total_percentage'] = round(_temp/len(results[file_id]['detail']), 4)
        return results


class MarkLiveInactiveView(APIViewBase):
    """
    Mark this live stream inactive API

    This API returns success or not.

    <h1>Request info</h1>
    <ul><p><u>Query param</u>:
        <li>file_id: (required) file id</li>
    </p></ul>
    ---
    DELETE:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: true
          type: dictionary
          description: dictionary
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: file_id
          paramType: query
          description: The file id
          required: false
          type: string

      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    authentication_classes = ()
    permission_classes = ()

    def delete(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.mark_live_inactive_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'file_id': request.GET.get('file_id', None),
        }
        if not params['file_id']:
            raise exceptions.InvalidParam
        return params

    def mark_live_inactive_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        LiveEncodeTask.objects.filter(file_id=params['file_id']).update(status=LiveEncodeTask.S_INACTIVE)
        return None