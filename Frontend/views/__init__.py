from .submiter import SubmitRequestView, SubmitCustomRequestView
from .file_info import FileProgressView, FileDetailView, FileSummaryListView, MarkLiveInactiveView
from .profiles import ProfilesListView

__author__ = 'duybq'