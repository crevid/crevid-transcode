try:
    import ujson as json
except ImportError:
    import json
import time
import urllib.parse
from django.db.models import Q
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.response import Response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from api_core.contrib import utils, exceptions
from api_core.define import RESPONSE_SUCCESS_MSG
from api_core.contrib.views import APIViewBase, APIVersion

from ..serializers import *

__author__ = 'duybq'


class ProfilesListView(APIViewBase):
    """
    Get list of profiles API

    This API returns a list of profiles with the filters (search_key), order (-created_date,...) and pagination.

    <h1>Request info</h1>
    <ul><p><u>Body JSON attributes</u>:
        <li>search_key: (optional) filter by a string</li>
        <li>filter_owner: (optional) filter by a an owner id</li>
        <li>filter_status: (optional), a list of status id separated by commas. The status are:
            <ul>
                <ul><li>0: initial</li>
                <li>1: success</li>
                <li>2: processing</li>
                <li>-1: failure</li>
            </ul>
        </li>
        <li><u>order_by</u>: (optional), choose one of these:
            <ul>
                <li>-created_date: sort descending by created_date (default)</li>
                <li>created_date: sort ascending by created_date</li>
                <li>-size: sort descending by sort</li>
                <li>size: sort descending by</li>
            </ul>
        </li>
        <li>items_per_page: (optional) the visitor ID, default is 30</li>
        <li>page: (optional) the visitor ID, default is 1</li>
    </p></ul>
    ---
    GET:
      type:
        e:
          type: integer
          required: true
          description: The value specifies error (different than 0) or not (0)
        data:
          required: true
          type: dictionary
          description: dictionary contains&#58; entity_id(string), updated_at(int), watch_later(boolean),
                        add_watch_at(int), progress (dictionary&#58; empty or {"android"&#58; ..., "ios"&#58; ...})
        detail:
          required: true
          type: string
          description: The description about this response
        time:
          required: true
          type: float
          description: Unix timestamp of this response
      parameters:
        - name: Accept
          paramType: header
          description: application/json; version=1.0
          required: true
          type: string
        - name: Accept-Language
          paramType: header
          description: vi or en
          required: true
          type: string
        - name: Content-Type
          paramType: header
          description: application/json
          required: true
          type: string

        - name: search_key
          paramType: query
          description: Search key
          required: false
          type: string
        - name: filter_owner
          paramType: query
          description: Filter by an owner string
          required: false
          type: string
        - name: filter_status
          paramType: query
          description: Leave blank if query all. Can be sent multiple status separated by commas.
          required: false
          type: Example&#58; 1,0
        - name: order_by
          paramType: query
          description: Leave blank if order by created_date. See description abow.
          required: false
          type: Example&#58; -created_date
        - name: items_per_page
          paramType: query
          description: Maximum items per page, default is 30.
          required: false
          type: Example&#58; 30
        - name: page
          paramType: query
          description: Page number, default is 1.
          required: false
          type: Example&#58; 1

      responseMessages:
        - code: 200
          message: Success response.
        - code: 400
          message: Missing required parameters
        - code: 406
          message: The supplied value of parameter is invalid.
        - code: 503
          message: Service does not support this API version.
        - code: 500
          message: Internal Server Error
    """
    authentication_classes = ()
    permission_classes = ()

    def get(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.generate_file_list_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'items_per_page': request.GET.get('items_per_page', 25),
            'search_key': request.GET.get('search_key', None),
            'page': request.GET.get('page', 1),
            'order_by': request.GET.get('order_by', '-created_date'),
        }
        params['items_per_page'] = min(max(int(params['items_per_page']), 1), 100)
        return params

    def generate_file_list_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        condition = ~Q(disabled=True)
        if 'search_key' in params and params['search_key']:
            _cond = Q(name__icontains=params['search_key'])
            for _temp in params['search_key'].split(' '):
                _cond |= Q(name__icontains=_temp)
            condition &= _cond
        if 'order_by' in params and params['order_by']:
            fileinfo_queryset = ProfileMapper.objects.filter(condition).order_by(params['order_by'])
        else:
            fileinfo_queryset = ProfileMapper.objects.filter(condition)

        paginator = Paginator(fileinfo_queryset, params['items_per_page'])
        try:
            page_data = paginator.page(params['page'])
        except PageNotAnInteger:
            params['page'] = 1
            page_data = paginator.page(params['page'])
        except EmptyPage:
            params['page'] = paginator.num_pages
            page_data = paginator.page(paginator.num_pages)

        profiles_data = list()
        for _temp in page_data.object_list:
            profiles_data.append(ProfileMapperSerializer(_temp).data)

        result = {
            'items_per_page': params['items_per_page'],
            'page': params['page'],
            'max_page': paginator.num_pages,
            'num_items': len(profiles_data),
            'result': profiles_data,
        }
        for _temp in params.copy():
            if params[_temp] is None:
                del params[_temp]
        try:
            params['page'] = page_data.next_page_number()
            result.update({
                'next_url': request.build_absolute_uri(reverse('profile_list_view'))
                                + '?' + urllib.parse.urlencode(params)
            })
        except EmptyPage:
            result.update({
                'next_url': request.build_absolute_uri(reverse('profile_list_view'))
                                + '?' + urllib.parse.urlencode(params)
            })
        try:
            params['page'] = page_data.previous_page_number()
            result.update({
                'previous_url': request.build_absolute_uri(reverse('profile_list_view'))
                                    + '?' + urllib.parse.urlencode(params)})
        except EmptyPage:
            result.update({
                'previous_url': request.build_absolute_uri(reverse('profile_list_view'))
                                    + '?' + urllib.parse.urlencode(params)})
        return result
