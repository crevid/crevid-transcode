try:
    import ujson as json
except ImportError:
    import json
from rest_framework import serializers

from Controller.models import *

__author__ = 'duybq'


class FeatureSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    metadata = serializers.SerializerMethodField('decode_metadata')

    class Meta:
        model = Feature
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def decode_metadata(self, obj):
        return obj.metadata


class FileInfoSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    original_metadata = serializers.SerializerMethodField('decode_original_metadata')
    standard_metadata = serializers.SerializerMethodField('decode_standard_metadata')
    temporary_metadata = serializers.SerializerMethodField('decode_temporary_metadata')
    encode_options = serializers.SerializerMethodField('decode_encode_options')

    class Meta:
        model = FileInfo
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def decode_original_metadata(self, obj):
        return obj.original_metadata

    def decode_standard_metadata(self, obj):
        return obj.standard_metadata

    def decode_temporary_metadata(self, obj):
        return obj.temporary_metadata

    def decode_encode_options(self, obj):
        return obj.encode_options


class FileInfoShortSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    standard_metadata = serializers.SerializerMethodField('decode_standard_metadata')

    class Meta:
        model = FileInfo
        exclude = ('original_metadata', 'temporary_metadata', 'created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def decode_standard_metadata(self, obj):
        return obj.standard_metadata


class WorkflowSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    file = serializers.SerializerMethodField('get_string_file_id')
    metadata = serializers.SerializerMethodField('decode_metadata')
    feature = serializers.SerializerMethodField('get_full_feature')

    class Meta:
        model = Workflow
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_string_file_id(self, obj):
        return str(obj.file)

    def decode_metadata(self, obj):
        return obj.metadata

    def get_full_feature(self, obj):
        if obj.feature:
            return FeatureSerializer(obj.feature).data
        return None


class WorkflowShortSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    feature = serializers.SerializerMethodField('get_full_feature')

    class Meta:
        model = Workflow
        exclude = ('file', 'metadata', 'created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_full_feature(self, obj):
        if obj.feature:
            return FeatureSerializer(obj.feature).data
        return None


class CommandTemplateSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    keyword = serializers.SerializerMethodField('decode_keyword')

    class Meta:
        model = CommandTemplate
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def decode_keyword(self, obj):
        return obj.keyword


class VideoProfileSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    template = serializers.SerializerMethodField('get_full_template')

    class Meta:
        model = VideoProfile
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_full_template(self, obj):
        if obj.template:
            return CommandTemplateSerializer(obj.template).data
        return None


class VideoProfileShortSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    template = serializers.SerializerMethodField('get_template_id')

    class Meta:
        model = VideoProfile
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_template_id(self, obj):
        if obj.template:
            return str(obj.template_id)
        return None


class AudioProfileSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    template = serializers.SerializerMethodField('get_full_template')

    class Meta:
        model = AudioProfile
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_full_template(self, obj):
        if obj.template:
            return CommandTemplateSerializer(obj.template).data
        return None


class AudioProfileShortSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    template = serializers.SerializerMethodField('get_template_id')

    class Meta:
        model = AudioProfile
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_template_id(self, obj):
        if obj.template:
            return str(obj.template_id)
        return None


class ProfileMapperSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    video_profile = serializers.SerializerMethodField('get_full_video_profile')
    audio_profile = serializers.SerializerMethodField('get_full_audio_profile')
    live_template = serializers.SerializerMethodField('get_live_template_detail')

    class Meta:
        model = ProfileMapper
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_full_video_profile(self, obj):
        if obj.video_profile:
            return VideoProfileSerializer(obj.video_profile).data
        return None

    def get_full_audio_profile(self, obj):
        if obj.audio_profile:
            return AudioProfileSerializer(obj.audio_profile).data
        return None

    def get_live_template_detail(self, obj):
        if obj.live_template:
            return CommandTemplateSerializer(obj.live_template).data


class ProfileMapperShortSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    video_profile = serializers.SerializerMethodField('get_video_profile_id')
    audio_profile = serializers.SerializerMethodField('get_audio_profile_id')

    class Meta:
        model = ProfileMapper
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_video_profile_id(self, obj):
        if obj.video_profile:
            return str(obj.video_profile_id)
        return None

    def get_audio_profile_id(self, obj):
        if obj.audio_profile:
            return str(obj.audio_profile_id)
        return None

    def get_live_template_id(self, obj):
        if obj.live_template:
            return str(obj.live_template_id)


class LiveEncodeTaskSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_string_id')
    file = serializers.SerializerMethodField('get_file_id')
    profile = serializers.SerializerMethodField('get_mapper_profile_id')

    class Meta:
        model = LiveEncodeTask
        exclude = ('created_date', 'modified_date')

    def get_string_id(self, obj):
        return str(obj.pk)

    def get_file_id(self, obj):
        return str(obj.file_id)

    def get_mapper_profile_id(self, obj):
        if obj.profile:
            return str(obj.profile_id)
        return None
