from django.conf.urls import url
from django.views.decorators.cache import cache_page

from .views import SubmitRequestView, FileDetailView, FileSummaryListView, ProfilesListView, FileProgressView
from .views import MarkLiveInactiveView, SubmitCustomRequestView

__author__ = 'duybq'


urlpatterns = [
    # Front-end API
    url(r'^submit$', SubmitRequestView.as_view(), name='submit_request_view'),
    url(r'^custom_submit$', SubmitCustomRequestView.as_view(), name='submit_custom_request_view'),
    url(r'^file/list$', FileSummaryListView.as_view(), name='file_summary_list_view'),
    url(r'^file/progress$', FileProgressView.as_view(), name='file_progress_view'),
    url(r'^file/detail$', FileDetailView.as_view(), name='file_detail_view'),
    url(r'^profile/list$', ProfilesListView.as_view(), name='profile_list_view'),
    url(r'^live/mark_inactive$', MarkLiveInactiveView.as_view(), name='mark_live_inactive'),
]
