from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException

__author__ = 'duybq'


class BadClientRequest(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('Bad client')


class ProfileNotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('Not found any profile(s)')