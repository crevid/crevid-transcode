#! /bin/bash

#export C_FORCE_ROOT=True

case "$1" in
  start)
    echo "Starting worker for mux_queue"
    # Start the daemon
    /home/transcode/env/bin/celery multi start mux_worker -A Transcode -Q mux_queue --concurrency=1 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stop)
    echo "Stopping worker for mux_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stopwait mux_worker -A Transcode -Q mux_queue --concurrency=1 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stopnow)
    echo "Stopping worker for mux_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stop mux_worker -A Transcode -Q mux_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  restart)
    echo "Restart worker for mux_queue"
    /home/transcode/env/bin/celery multi restart mux_worker -A Transcode -Q mux_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  *)
    # Refuse to do other stuff
    echo "$0 {start|stop|stopnow|restart}"
    exit 1
    ;;
esac

exit 0