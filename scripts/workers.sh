#! /bin/bash

#export C_FORCE_ROOT=True

case "$1" in
  start)
    echo "Starting worker for default_queue, pre_process_queue, encode_audio_queue, mux_queue, encode_video_queue"
    # Start the daemon
    /home/transcode/env/bin/celery multi start 7 -A Transcode -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Q:1 pre_process_queue -Q:2 default_queue -Q:3 encode_audio_queue -Q:4 mux_queue -Q:5 post_process_queue -Q encode_video_queue

    ;;
  stop)
    echo "Stopping worker for default_queue, pre_process_queue, encode_audio_queue, mux_queue, encode_video_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stopwait 7 -A Transcode -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Q:1 pre_process_queue -Q:2 default_queue -Q:3 encode_audio_queue -Q:4 mux_queue -Q:5 post_process_queue -Q encode_video_queue

    ;;
  stopnow)
    echo "Stopping worker for default_queue, pre_process_queue, encode_audio_queue, mux_queue, encode_video_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stop 7 -A Transcode -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Q:1 pre_process_queue -Q:2 default_queue -Q:3 encode_audio_queue -Q:4 mux_queue -Q:5 post_process_queue -Q encode_video_queue

    ;;
  restart)
    echo "Restart worker for default_queue, pre_process_queue, encode_audio_queue, mux_queue, encode_video_queue"
    /home/transcode/env/bin/celery multi restart 7 -A Transcode -l restart --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Q:1 pre_process_queue -Q:2 default_queue -Q:3 encode_audio_queue -Q:4 mux_queue -Q:5 post_process_queue -Q encode_video_queue

    ;;
  *)
    # Refuse to do other stuff
    echo "$0 {start|stop|stopnow|restart}"
    exit 1
    ;;
esac

exit 0