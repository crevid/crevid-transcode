FROM ubuntu:14.04

MAINTAINER cvald.it@gmail.com


USER root

WORKDIR /tmp

###########################################################
# BUILD FFMPEG & MEDIAINFO & FFMPEG
###########################################################
# Set Locale

USER root

RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ADD build_ffmpeg.sh /tmp/build_ffmpeg.sh

RUN echo deb http://archive.ubuntu.com/ubuntu precise universe multiverse >> /etc/apt/sources.list; apt-get update; apt-get install -y git autoconf automake build-essential mercurial cmake libass-dev libgpac-dev libtheora-dev libtool libvdpau-dev libvorbis-dev pkg-config texi2html zlib1g-dev libmp3lame-dev wget yasm openssl libssl-dev mediainfo gpac
# Run build script

RUN chmod +x /tmp/build_ffmpeg.sh;
RUN /bin/bash /tmp/build_ffmpeg.sh

###########################################################
# END OF BUILD FFMPEG
###########################################################

###########################################################
# BUILD NGINX
###########################################################

WORKDIR /usr/local/share

RUN wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.39.tar.gz
RUN tar -zxf pcre-8.39.tar.gz
WORKDIR /usr/local/share/pcre-8.39
RUN ./configure
RUN make && make install

WORKDIR /tmp
RUN git clone https://github.com/kaltura/nginx-vod-module.git
RUN git clone https://github.com/sergey-dryabzhinsky/nginx-rtmp-module.git

RUN wget https://nginx.org/download/nginx-1.8.1.tar.gz
RUN tar zxf nginx-1.8.1.tar.gz
WORKDIR /tmp/nginx-1.8.1

RUN ./configure --prefix=/opt/nginx_vod --sbin-path=/usr/bin/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/var/run/nginx/nginx.pid --with-pcre=/usr/local/share/pcre-8.39 --with-debug --with-http_ssl_module --without-http_empty_gif_module --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module --with-http_stub_status_module --add-module=/tmp/nginx-vod-module --add-module=/tmp/nginx-rtmp-module --with-file-aio --with-threads
RUN make -j8 && make install -j8; ln -s /usr/bin/nginx /usr/local/bin/nginx
ADD livestream-nginx.conf /etc/nginx/nginx.conf

###########################################################
# END OF BUILD NGINX
###########################################################

RUN echo "#!/bin/bash" > /tmp/run.sh && \
 echo "cd /tmp/nginx" >> /tmp/run.sh && \
 echo "sed \"s/_hls_variant_keyword_/\$VARIANT_INFO/g\" \"/etc/nginx/nginx.conf\" > \"/tmp/nginx.conf.bk\"" >> /tmp/run.sh && \
 echo "sed \"s/_hls_seconds_per_segment_/\$SECONDS_PER_SEGMENT/g\" \"/tmp/nginx.conf.bk\" > \"/tmp/nginx.conf.bk2\"" >> /tmp/run.sh && \
 echo "sed \"s/_hls_playlist_length_/\$PLAYLIST_LENGTH/g\" \"/tmp/nginx.conf.bk2\" > \"/tmp/nginx.conf.bk3\"" >> /tmp/run.sh && \
 echo "sed \"s/_nginx_input_port_/\$NGINX_INPUT_PORT/g\" \"/tmp/nginx.conf.bk3\" > \"/tmp/nginx.conf.bk4\"" >> /tmp/run.sh && \
 echo "rm /tmp/nginx.conf.bk" >> /tmp/run.sh && \
 echo "rm /tmp/nginx.conf.bk2" >> /tmp/run.sh && \
 echo "rm /tmp/nginx.conf.bk3" >> /tmp/run.sh && \
 echo "mv /tmp/nginx.conf.bk4 /etc/nginx/nginx.conf" >> /tmp/run.sh && \
 echo "if [ -n \"\$FFMPEG_CMD\" ]; then " >> /tmp/run.sh && \
 echo "/usr/bin/nginx -g \"daemon on;\"" >> /tmp/run.sh && \
 echo "\$FFMPEG_CMD" >> /tmp/run.sh && \
 echo "else" >> /tmp/run.sh && \
 echo "/bin/mkdir -p /tmp/nginx" >> /tmp/run.sh && \
 echo "/bin/ln -s /dev/stdout /tmp/nginx/nginx.log" >> /tmp/run.sh && \
 echo "/usr/bin/nginx -g \"daemon off;\"" >> /tmp/run.sh && \
 echo "fi"  >> /tmp/run.sh && \
 chmod +x /tmp/run.sh

RUN mkdir /tmp/nginx; mkdir /tmp/data

CMD ["/bin/bash", "/tmp/run.sh"]

EXPOSE 1835