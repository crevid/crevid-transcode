#!/bin/bash

chmod -R 777 /home/ftp_users
su transcode -c "cd /home/transcode/source/crevid-transcode && git reset --hard && git pull && . /home/transcode/env/bin/activate && pip install -r /home/transcode/source/crevid-transcode/requirements.txt && cp -f /home/transcode/source/crevid-transcode/scripts/worker_config.py /home/transcode/source/crevid-transcode/Transcode/settings/local.py"

case "$1" in
  default_worker)
    echo "Starting default_worker listen at default_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q default_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  pre_process_worker)
    echo "Starting pre_process_worker listen at pre_process_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q pre_process_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  encode_video_worker)
    echo "Starting encode_video_worker listen at encode_video_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q encode_video_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  encode_audio_worker)
    echo "Starting encode_audio_worker listen at encode_audio_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q encode_audio_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  mux_worker)
  echo "Starting mux_worker listen at mux_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q mux_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  post_process_worker)
    echo "Starting post_process_worker listen at post_process_queue"
    su transcode -c "cd /home/transcode/source/crevid-transcode && . /home/transcode/env/bin/activate && /home/transcode/env/bin/celery worker -A Transcode -Q post_process_queue --concurrency=1 -l info --pidfile='/var/log/transcode/%n.pid' -Ofair"
    ;;
  *)
    # Refuse to do other stuff
    echo "$0 {start|stop|stopnow|restart}"
    /bin/bash
    ;;
esac
