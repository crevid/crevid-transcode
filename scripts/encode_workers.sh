#! /bin/bash

#export C_FORCE_ROOT=True

case "$1" in
  start)
    echo "Starting worker for encode_audio_queue, encode_video_queue"
    # Start the daemon
    /home/transcode/env/bin/celery multi start encode_video_worker -A Transcode -Q encode_video_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair
    /home/transcode/env/bin/celery multi start encode_audio_worker -A Transcode -Q encode_audio_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stop)
    echo "Stopping worker for encode_audio_queue, encode_video_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stopwait encode_video_worker -A Transcode -Q encode_video_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair
    /home/transcode/env/bin/celery multi stopwait encode_audio_worker -A Transcode -Q encode_audio_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stopnow)
    echo "Stopping worker for encode_audio_queue, encode_video_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stop encode_video_worker -A Transcode -Q encode_video_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair
    /home/transcode/env/bin/celery multi stop encode_audio_worker -A Transcode -Q encode_audio_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  restart)
    echo "Restart worker for encode_audio_queue, encode_video_queue"
    /home/transcode/env/bin/celery multi restart encode_video_worker -A Transcode -Q encode_video_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair
    /home/transcode/env/bin/celery multi restart encode_audio_worker -A Transcode -Q encode_audio_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  *)
    # Refuse to do other stuff
    echo "$0 {start|stop|stopnow|restart}"
    exit 1
    ;;
esac

exit 0