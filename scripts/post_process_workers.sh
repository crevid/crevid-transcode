#! /bin/bash

#export C_FORCE_ROOT=True

case "$1" in
  start)
    echo "Starting worker for post_process_queue"
    # Start the daemon
    /home/transcode/env/bin/celery multi start post_process_worker -A Transcode -Q post_process_queue --concurrency=1 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stop)
    echo "Stopping worker for post_process_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stopwait post_process_worker -A Transcode -Q post_process_queue --concurrency=1 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  stopnow)
    echo "Stopping worker for post_process_queue"
    # Stop the daemon
    /home/transcode/env/bin/celery multi stop post_process_worker -A Transcode -Q post_process_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  restart)
    echo "Restart worker for post_process_queue"
    /home/transcode/env/bin/celery multi restart post_process_worker -A Transcode -Q post_process_queue --concurrency=2 -l warning --pidfile="/var/log/transcode/%n.pid" --logfile="/var/log/transcode/%n.log" -Ofair

    ;;
  *)
    # Refuse to do other stuff
    echo "$0 {start|stop|stopnow|restart}"
    exit 1
    ;;
esac

exit 0