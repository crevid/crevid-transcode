#!/bin/bash

su transcode -c "cd /home/transcode/source/crevid-transcode && git reset --hard && git pull && . /home/transcode/env/bin/activate && pip install -r /home/transcode/source/crevid-transcode/requirements.txt;"

/usr/bin/supervisord