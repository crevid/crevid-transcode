from .base import *

DEBUG = False

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {}

# Caches
# http://niwibe.github.io/django-redis/
# https://docs.djangoproject.com/en/1.8/topics/cache/

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'worker',
    },
}


# Logging configurations

BASE_LOG_DIR = '/var/log/transcode'
if not os.path.exists(BASE_LOG_DIR):
    os.makedirs(BASE_LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "%(levelname)s\t%(asctime)s\t%(module)s:%(filename)s:%(funcName)s:%(lineno)d\t%(message)s\t"
                      "%(params)s"
        },
        'simple': {
            'format': '%(levelname)s\t%(asctime)s\t%(filename)s\t%(lineno)d\t%(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'default-transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'simple',
            'backupCount': 30,
        },
        'worker_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'worker-transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'simple',
            'backupCount': 30,
        },
        'transcode_handler': {
            'level': 'DEBUG',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, 'transcode.log'),
            'when': 'D',
            'encoding': 'utf-8',
            'formatter': 'verbose',
            'backupCount': 30,
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['default', 'console'],
            'level': 'DEBUG',
            'propagate': True,
            'filters': ['require_debug_false'],
        },
        'django.server': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'Transcode': {
            'handlers': ['transcode_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False,
            'filters': ['require_debug_false'],
        },
        'worker': {
            'handlers': ['worker_handler', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    }
}


# Broker
CELERY_BROKER_URL = 'amqp://to_rabbit_user:65q1oLC0uJg525Q8f0yY@transcode_controller.internal:5672//transcode'

# CELERY_RESULT_BACKEND = 'redis://redis.local:6379/9'


# Custom

BACKEND_URI = 'http://transcode_controller.internal/backend/'

API_FILE_INFO = BACKEND_URI + 'file/info'

API_UPDATE_LIVE_TASK = BACKEND_URI + 'live/update'


URL_INPUT_STORAGE_PREFIX = os.getenv('URL_INPUT_STORAGE_PREFIX',
    's3://AKIAJJ6YKAAFWQ4UZBQA:A574I3rnHfjmkOtXj6ktaI83DAt%2BaP5sz%2FDix0Uh@s3-ap-southeast-1.amazonaws.com/crevid-temp/transcode_temp/')

PERMANENTLY_INPUT_STORAGE_PREFIX = '/home/ftp_users/transcode/transcode_temp'

URL_OUTPUT_STORAGE_PREFIX = os.getenv('URL_OUTPUT_STORAGE_PREFIX',
    's3://AKIAJJ6YKAAFWQ4UZBQA:A574I3rnHfjmkOtXj6ktaI83DAt%2BaP5sz%2FDix0Uh@s3-ap-southeast-1.amazonaws.com/crevid-temp/transcode_temp/')

PERMANENTLY_OUTPUT_STORAGE_PREFIX = '/home/ftp_users/transcode/transcode_temp'

INGEST_URI = 'http://ingestion.crevid.com/api/'

API_REQUEST_INGEST = INGEST_URI + 'ingest/submit'

# PLAYLIST_URI = 'http://plist.uri/plist/'

# API_ADD_RESOURCE_PLAYLIST = PLAYLIST_URI + 'add'
