# User Account Management

## Yêu cầu hệ thống
- Python 2.7.x & dependent libraries (in requirements.txt)
apt-get install python-docutils
apt-get install libxml2-dev libxslt1-dev python-dev
apt-get install python-lxml
- Mysqlite
- Redis Server >= 2.8
- Rabbitmq 3.5
- Nginx >= 1.4.6
- UWSGI >= 2.0

## Yêu cầu cài đặt phần mềm
- [Pip (https://pip.pypa.io/en/latest/installing.html)](https://pip.pypa.io/en/latest/installing.html)
- [Setup tools (https://pypi.python.org/pypi/setuptools)](https://pypi.python.org/pypi/setuptools)
- [Virtual Environments (http://docs.python-guide.org/en/latest/dev/virtualenvs/)](http://docs.python-guide.org/en/latest/dev/virtualenvs/).

## Cài đặt
**Clone code từ git**
> git clone ...
> python manage.py makemessages -a
> python manage.py compilemessages

If get bug about Crypto, run: pip install paramiko --upgrade

## Cấu hình sau cài đăt
- apt-get install ncftp
- Setup rabbitmq
rabbitmqctl add_vhost /transcode
rabbitmqctl add_user admin 160591
rabbitmqctl add_user to_rabbit_user 65q1oLC0uJg525Q8f0yY
rabbitmqctl change_password guest guestRABBIT427
rabbitmqctl set_permissions -p /transcode to_rabbit_user ".*" ".*" ".*"
rabbitmqctl set_permissions -p /transcode guest ".*" ".*" ".*"
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
rabbitmqctl set_permissions -p /transcode admin ".*" ".*" ".*"
rabbitmqctl set_user_tags admin administrator
- Setup mysql
CREATE DATABASE transcode CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON transcode.* To 'transcode_user'@'%' IDENTIFIED BY 'q28L036N0c7gZsQ516Qm';
FLUSH PRIVILEGES;
- Setup PureFTP
mkdir /home/ftpusers/transcode
pure-pw useradd transcode -u ftpuser -d /home/ftpusers/transcode
pure-pw usermod transcode -y ''
pure-pw mkdb


docker build -t transcode/redis:latest -f Dockerfile_Redis .
docker build -t transcode/controller:latest -f Dockerfile_TranscodeController .
docker build -t transcode/worker:latest -f Dockerfile_TranscodeWorker .
nvidia-docker build -t transcode/worker-gpu:latest -f Dockerfile_TranscodeWorkerGPU .
docker build -t transcode/liveingest:latest -f Dockerfile_LiveIngest .
nvidia-docker build -t transcode/livetranscode-gpu:latest -f Dockerfile_LiveTranscodeGPU .
docker build -t transcode/livetranscode:latest -f Dockerfile_LiveTranscode .
docker build -t transcode/tempstorage:latest -f Dockerfile_PureFTP .

- Run FTP
- Run docker Pure-FTP
docker run -d --name pureftp_service -v /data/pureftp/config:/etc/pure-ftpd -v /data/pureftp/ftpusers:/home/ftpusers -m 1g --memory-swap -1 -p 21:21 -p 30000-50165:30000-50165 -e "PUBLICHOST=103.205.105.44" transcode/tempstorage
- Run redis
docker run -d --name redis_cache -v /data/redis/data:/data --net host -m 2g transcode/redis:latest
- Run MySQL
docker run --name mysql_db -v /tmp_ssd/mysql/data:/var/lib/mysql -m 2g --memory-swap -1 -p 127.0.0.1:3306:3306 -e MYSQL_ROOT_PASSWORD=123456789 -d mysql:5.7
- Run docker rabbitmq:
docker run -d --name rabbit_broker -v /tmp_ssd/rabbitmq:/var/lib/rabbitmq -p 5672:5672 -p 127.0.0.1:15672:15672 -m 2g --memory-swap -1 rabbitmq:3-management
- Run docker transcode controller
docker run -d --name transcode_controller -p 0.0.0.0:1518:1518 --link redis_cache:redis.local --link mysql_db:mysql_db.local -m 1g -v /var/run/docker.sock:/tmp/docker.sock --add-host transcode_controller.internal:52.220.65.153 --add-host ingestion.crevid.com:52.76.44.67 --memory-swap -1 transcode/controller:latest

- Run docker generate feed stream:
docker run -d --name liveingest_container -v /mnt/nginx_live_feed:/tmp/nginx -p 0.0.0.0:1835:1835 -p 0.0.0.0:1680:80 transcode/liveingest:latest

docker run -d --name livestream_container -v /data/nginx_live_feed:/tmp/nginx -p 0.0.0.0:80:80 -e "FFMPEG_CMD=ffmpeg -re -i rtmp://stream.taksimbilisim.com:1935/btv/bant1 -vcodec libx264 -preset fast -vprofile main -level 3.1 -filter_complex scale=800:450 -map_metadata -1 -acodec aac -ac 2 -af volume=1 -strict -2 -f flv rtmp://0.0.0.0:1835/hls/btv" transcode/livestream

- Run docker ingest:
docker run -d --name liveingest_container -v /mnt/nginx_live_feed:/tmp/nginx -p 0.0.0.0:1835:1835 -p 0.0.0.0:80:80 transcode/livesingest:latest

####
celery -A Transcode worker -Q rpc_results_queue --concurrency=1 -l debug
celery -A Transcode worker -Q pre_process_queue --concurrency=1 -l debug
celery -A Transcode worker -Q post_process_queue --concurrency=1 -l debug
celery -A Transcode worker -Q default_queue --concurrency=1 -l debug
celery -A Transcode worker -Q encode_audio_queue --concurrency=1 -l debug
celery -A Transcode worker -Q encode_video_queue --concurrency=1 -l debug
celery -A Transcode worker -Q mux_queue --concurrency=1 -l debug

docker run --name transcode_worker_pre_process -d -v /mnt/ftp_users:/home/ftp_users -m 1g --memory-swap -1 --add-host transcode_controller.internal:52.220.65.153 transcode/worker pre_process_worker
docker run --name transcode_worker_post_process -d -v /mnt/ftp_users:/home/ftp_users --add-host ingestion.crevid.com:52.76.44.67 -m 1g --memory-swap -1 --add-host transcode_controller.internal:52.220.65.153 transcode/worker post_process_worker
docker run --name transcode_worker_encode_audio -d -v /mnt/ftp_users:/home/ftp_users -m 1g --memory-swap -1 --add-host transcode_controller.internal:52.220.65.153 transcode/worker encode_audio_worker
docker run --name transcode_worker_encode_video_1 -d -v /mnt/ftp_users:/home/ftp_users -m 1g --memory-swap -1 --add-host transcode_controller.internal:52.220.65.153 transcode/worker encode_video_worker
docker run --name transcode_worker_mux -d -v /mnt/ftp_users:/home/ftp_users --add-host transcode_controller.internal:52.220.65.153 transcode/worker mux_worker