import traceback

from ..apps import CONFIGS

__author__ = 'duybq'


def get_scan_type(encode_options, standard_metadata, configs=None):
    # Firstly, load configurations and auto-detect
    configs = configs if configs else CONFIGS
    scan_type = 'progressive'
    method = 'default'
    try:
        _temp = 'interlaced' in standard_metadata['videos'][0]
        _temp = _temp and standard_metadata['videos'][0]['interlaced']
        if _temp:
            scan_type = 'interlaced'
    except TypeError:
        print('# DEBUG')
        traceback.print_exc()
        print(standard_metadata)
    #
    # Secondly, check encode options
    if 'force_progressive' in encode_options and encode_options['force_progressive']:
        scan_type = 'progressive'
    if 'force_interlaced' in encode_options and encode_options['force_interlaced']:
        scan_type = 'interlaced'
    if 'manual_scale' in encode_options and encode_options['manual_scale']:
        method = 'optional'
    #################################################################################
    return configs[scan_type]['scale'][method]