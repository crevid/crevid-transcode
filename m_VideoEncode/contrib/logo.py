from email.encoders import encode_7or8bit

import os
import tempfile

from api_core.contrib.storage import StorageAdapter
from api_core.contrib import utils

from ..apps import CONFIGS

__author__ = 'duybq'


def build_logo_parameters(input_cmnd, filter_cmnd, encode_options, configs=CONFIGS):
    temp_logo_path = None
    if 'logo' not in encode_options or 'X' not in encode_options['logo'] or 'Y' not in encode_options['logo'] \
            or 'position' not in encode_options['logo'] or encode_options['logo']['position'] not in configs['logo']:
        # Bad params
        return input_cmnd, filter_cmnd, temp_logo_path

    logo_cmnd = configs['logo'][encode_options['logo']['position']]['cmnd']
    if logo_cmnd and 'logo_uri' in encode_options['logo'] and encode_options['logo']['logo_uri']:
        # Prepare input
        input_cmnd = input_cmnd if type(input_cmnd) is list else [input_cmnd, ]
        logo_cmnd = str(logo_cmnd).replace('<X>', str(encode_options['logo']['X']))\
                                  .replace('<Y>', str(encode_options['logo']['Y']))
        temp_folder = os.path.join(tempfile.gettempdir(), 'transcode')
        if not os.path.isdir(temp_folder):
            os.makedirs(temp_folder)
        temp_logo_path = os.path.join(temp_folder, utils.generate_random_string() + '-'
                                                   + os.path.basename(encode_options['logo']['logo_uri']))
        StorageAdapter.copy_file(encode_options['logo']['logo_uri'], 'file://' + temp_logo_path)
        # Replace param
        input_cmnd += ['-i', temp_logo_path]
        filter_cmnd.append(logo_cmnd)

    return input_cmnd, filter_cmnd, temp_logo_path