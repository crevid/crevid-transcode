from email.encoders import encode_7or8bit

import os
import tempfile

from api_core.contrib.storage import StorageAdapter
from api_core.contrib import utils

from ..apps import CONFIGS

__author__ = 'duybq'


def build_subtitle_parameters(filter_cmnd, encode_options):

    temp_sub_path = None
    if 'subtitle' not in encode_options or 'input_uri' not in encode_options['subtitle'] \
            or not encode_options['subtitle']['input_uri']:
        # Bad params
        return filter_cmnd, temp_sub_path

    temp_folder = os.path.join(tempfile.gettempdir(), 'transcode')
    if not os.path.isdir(temp_folder):
        os.makedirs(temp_folder)
    temp_sub_path = os.path.join(temp_folder, utils.generate_random_string() + '-'
                                               + os.path.basename(encode_options['subtitle']['input_uri']))
    StorageAdapter.copy_file(encode_options['subtitle']['input_uri'], 'file://' + temp_sub_path)
    # Replace param
    filter_cmnd.append('subtitles=' + temp_sub_path)
    return filter_cmnd, temp_sub_path