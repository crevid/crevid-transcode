__author__ = 'duybq'


def build_dar_parameters(filter_cmnd, encode_options):
    if 'dar' not in encode_options or not encode_options['dar']:
        filter_cmnd.append('setsar=1/1')
    else:
        if encode_options['dar'] == '16/9':
            filter_cmnd.append('setdar=16/9')
        elif encode_options['dar'] == '4/3':
            filter_cmnd.append('setdar=4/3')
        elif encode_options['dar'] == '21/9':
            filter_cmnd.append('setdar=21/9')
        else:
            filter_cmnd.append('setsar=1/1')
    return filter_cmnd
