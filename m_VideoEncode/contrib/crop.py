__author__ = 'duybq'


def build_crop_parameters(filter_cmnd, encode_options):
    if 'crop' not in encode_options or not encode_options['crop']:
        # Bad params
        return filter_cmnd

    cmnd = command_crop_media(c_top=int(encode_options['crop'].get('top', 0)),
                              c_right=int(encode_options['crop'].get('right', 0)),
                              c_bottom=int(encode_options['crop'].get('bottom', 0)),
                              c_left=int(encode_options['crop'].get('left', 0)))
    # Only insert at first position for crop
    filter_cmnd.insert(0, cmnd)
    return filter_cmnd


def command_crop_media(c_top=0, c_right=0, c_bottom=0, c_left=0):
    parameter_crop = ''
    if c_top != 0 or c_right != 0 or c_bottom != 0 or c_left != 0:
        if (c_top != 0 and c_bottom != 0) or (c_left != 0 and c_right != 0):
            width_left_out = c_left
            width_right_out = c_right
            height_top_out = c_top
            height_bottom_out = c_bottom
            if c_bottom != 0:
                c_bottom = -c_bottom
            if c_right != 0:
                c_right = -c_right
            parameter_crop_left_top = 'crop=iw-%s:ih-%s:%s:%s' % (str(width_left_out), str(height_top_out),
                                                                  str(c_left), str(c_top))
            parameter_crop_right_bottom = 'crop=iw-%s:ih-%s:%s:%s' % (str(width_right_out), str(height_bottom_out),
                                                                      str(c_right), str(c_bottom))
            parameter_crop = '%s,%s,' % (parameter_crop_left_top, parameter_crop_right_bottom)
        else:
            crop_vertical = 0
            crop_horizotal = 0
            width_out = 0
            height_out = 0
            if c_top != 0:
                crop_vertical = c_top
                height_out = c_top
            if c_bottom != 0:
                crop_vertical = -c_bottom
                height_out = c_bottom
            if c_left != 0:
                crop_horizotal = c_left
                width_out = c_left
            if c_right != 0:
                crop_horizotal = -c_right
                width_out = c_right
            parameter_crop = 'crop=iw-%s:ih-%s:%s:%s,' % (str(width_out), str(height_out),
                                                          str(crop_horizotal), str(crop_vertical))
    return parameter_crop
