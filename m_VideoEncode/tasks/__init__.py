from __future__ import absolute_import

import os
import re
import time
import math
import queue
import shutil
import psutil
import tempfile
import traceback
import threading
from celery import shared_task, states
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.base_task import BaseTask, request_file_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

from ..contrib import scan_mode, logo, subtitle, crop, dar

__author__ = 'duybq'


def pop_highest_weight(items):
    if len(items) < 1:
        return None, []
    elif len(items) == 1:
        if items[0]['is_fetched']:
            return None, items
        else:
            items[0].update({'is_fetched': True})
            return 0, items
    highest_item_index = None
    for idx, e in enumerate(items):
        if e['is_fetched']:
            continue
        if highest_item_index is None or int(e['weight']) > int(items[highest_item_index]['weight']):
            highest_item_index = idx
    if highest_item_index is None:
        return None, items
    items[highest_item_index].update({'is_fetched': True})
    return highest_item_index, items


def pop_lowest_weight(items):
    if len(items) < 1:
        return None, []
    elif len(items) == 1:
        if items[0]['is_fetched']:
            return None, items
        else:
            items[0].update({'is_fetched': True})
            return 0, items
    lowest_item_index = None
    for idx, e in enumerate(items):
        if e['is_fetched']:
            continue
        if lowest_item_index is None or int(e['weight']) < int(items[lowest_item_index]['weight']):
            lowest_item_index = idx
    if lowest_item_index is None:
        return None, items
    items[lowest_item_index].update({'is_fetched': True})
    return lowest_item_index, items


def calc_number_of_threads():
    return int(math.ceil(float(psutil.cpu_count()) / 12))


class ThreadSignal(object):
    keep_on = True


class UploadWorker(threading.Thread):

    queue = None
    signal = None
    func = None

    def __init__(self, signaling, queue, func):
        threading.Thread.__init__(self)
        self.queue = queue
        self.signal = signaling
        self.func = func

    def run(self):
        while True:
            if not self.signal.keep_on and self.queue.empty():
                break
            try:
                params = self.queue.get(timeout=60)
                print(params)
                self.func(**params)
            except queue.Empty:
                pass
            except Exception as e:
                traceback.print_exc()
                print(str(e.args))
            finally:
                try:
                    self.queue.task_done()
                except ValueError as e:
                    pass


@shared_task(bind=True, base=BaseTask)
def encode_video_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()

    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################

    encode_items = list()
    sample_item = {
        'cmnd': None,
        'process': None,
        'source_path': None,
        'dest_path': None,
        'sent': False,
        'weight': 0,
        'is_fetched': False,
    }
    signal = ThreadSignal()
    upload_queue = queue.LifoQueue(maxsize=32)
    upload_thread = UploadWorker(signal, upload_queue, StorageAdapter.copy_file)
    upload_thread.daemon = True
    upload_thread.start()
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']
        template_data = information['template_data']
        video_profile_data = information['video_profile_data']

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Prepare input encode
        temporary_src_path = workflow_data['metadata']['input']['phys_video_src_path']
        uri_input_src_path = workflow_data['metadata']['input']['uri_video_src_path']
        if not os.path.isfile(temporary_src_path):
            # Download file if not in local
            temporary_src_path = os.path.join(tempfile.gettempdir(), 'transcode',
                                              utils.generate_random_string(16) + os.path.split(temporary_src_path)[-1])
            _temp = os.path.dirname(temporary_src_path)
            if not os.path.isdir(_temp):
                os.makedirs(_temp)
            # START OF: Update progress
            utils.update_progress(self_task=self, error=error, percentage=5, message='Begin to download file to encode',
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress
            #
            StorageAdapter.copy_file(uri_input_src_path, 'file://' + temporary_src_path)
            #
            # START OF: Update progress
            utils.update_progress(self_task=self, error=error, percentage=15, message='Complete to download file',
                                  file_id=input_params['file_id'], workflow=workflow_data, file_data=None)
            # END OF: Update progress
            junk_paths.append(temporary_src_path)
        # Prepare output encode
        rel_output_dst_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
                                           utils.generate_random_string(8, str(workflow_data['id'])))
        phys_output_dst_path = os.path.join(settings.PERMANENTLY_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        uri_output_dst_path = os.path.join(settings.URL_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        temporary_dst_path = os.path.join(tempfile.gettempdir(), 'transcode', rel_output_dst_path)
        if not os.path.isdir(temporary_dst_path):
            os.makedirs(temporary_dst_path)

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=16, message='Update temporary metadata',
                              file_id=input_params['file_id'], workflow=workflow_data, file_data=None)
        # END OF: Update progress

        output_paths = list()
        current_percentage = 16
        print('Build command-line and encode')
        print('##################', len(video_profile_data))
        for _key in video_profile_data:
            profile = video_profile_data[_key]
            # Build command line
            template_id = profile['template']
            cmnd = template_data[template_id]['pattern']
            for _key in template_data[template_id]['keyword']:
                # Skip input and output tag
                if _key in ('<input>', '<output>', '<scale>'):
                    continue
                # Remove < and > to query attributes and replace
                search = re.match(r'.*<([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                if search:
                    if search.group(1) in profile:
                        _temp = float(search.group(2)) if search.group(2) else 1
                        if _temp > 1:
                            _temp = str(int(float(profile[search.group(1)]) * _temp))
                        else:
                            # Case profile still return but empty params
                            if (profile[search.group(1)] is not None) and (str(profile[search.group(1)]) != '0'):
                                _temp = str(profile[search.group(1)])

                            # Feature: keyint = framerate*2
                            elif search.group(1).find('keyint') > -1:
                                _temp = str(math.ceil(float(file_data['standard_metadata']['videos'][0]['framerate'])*2))

                            # Feature: other missing params
                            else:
                                _temp = str(file_data['standard_metadata']['videos'][0][search.group(1)])
                    else:
                        # Case Profile does not return params
                        # Feature: keyint = framerate*2
                        if search.group(1).find('keyint') > -1:
                            _temp = str(math.ceil(float(file_data['standard_metadata']['videos'][0]['framerate'])*2))
                        else:
                            # Feature: other missing params
                            _temp = str(file_data['standard_metadata']['videos'][0][search.group(1)])
                    cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
            cmnd = cmnd.split(' ')
            # Replace <input> / <output> tag and <scale>
            _filename = utils.generate_random_string(8) + '.mp4'
            _paths = {
                'data_type': 'video',
                'profile_id': profile['id'],
                'phys_output_dst_path': os.path.join(phys_output_dst_path, _filename),
                'uri_output_dst_path': os.path.join(uri_output_dst_path, _filename),
                'temporary_dst_path': os.path.join(temporary_dst_path, _filename),
            }
            junk_paths.append(_paths['temporary_dst_path'])
            filter_cmnd = list()
            real_cmnd = list()
            for idx, val in enumerate(cmnd):
                current_cmnd = cmnd[idx]
                if val == '<input>':
                    #
                    # Replace <input> tag with input path
                    #
                    current_cmnd = temporary_src_path

                    #
                    # Feature: Embed logo
                    current_cmnd, filter_cmnd, _temp = \
                        logo.build_logo_parameters(input_cmnd=current_cmnd, filter_cmnd=filter_cmnd,
                                                   encode_options=file_data['encode_options'],
                                                   configs=workflow_data['feature']['metadata'])
                    junk_paths.append(_temp)

                elif val == '<output>':
                    #
                    # Replace <output> tag with input path
                    #
                    current_cmnd = _paths['temporary_dst_path']
                elif val == '<scale>':
                    #
                    # Feature: Hardsub
                    #
                    filter_cmnd, _temp = \
                        subtitle.build_subtitle_parameters(filter_cmnd=filter_cmnd,
                                                           encode_options=file_data['encode_options'],)
                    junk_paths.append(_temp)

                    #
                    # Feature change scan mode: Interlaced or Progressive
                    #                           (these params always present at first)
                    _temp = scan_mode.get_scan_type(encode_options=file_data['encode_options'],
                                                    standard_metadata=file_data['standard_metadata'],
                                                    configs=workflow_data['feature']['metadata'])
                    _temp = _temp.split(',')
                    if len(_temp) > 1:
                        filter_cmnd = [_temp[0], ] + filter_cmnd + [_temp[1], ]
                    else:
                        filter_cmnd = filter_cmnd + _temp

                    #
                    # Feature: Crop
                    #
                    filter_cmnd = crop.build_crop_parameters(filter_cmnd=filter_cmnd,
                                                             encode_options=file_data['encode_options'])

                    #
                    # Feature Set DAR
                    #
                    filter_cmnd = dar.build_dar_parameters(filter_cmnd=filter_cmnd,
                                                           encode_options=file_data['encode_options'])


                    ###############################################################################
                    filter_cmnd = ','.join(filter_cmnd).strip(',').replace(',,', ',')
                    #
                    # Feature: Force encode by width or height (if these attributes available)
                    for _key in ('<width>', '<height>'):
                        search = re.match(r'.*<([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
                        if search:
                            _temp = float(search.group(2)) if search.group(2) else 1
                            if _temp > 1:
                                _temp = str(int(float(profile[search.group(1)]) * _temp))
                            else:
                                _temp = str(profile[search.group(1)])
                            filter_cmnd = str(filter_cmnd).replace(_key, _temp)
                    ###############################################################################
                    current_cmnd = filter_cmnd
                if type(current_cmnd) is list:
                    real_cmnd += current_cmnd
                else:
                    real_cmnd.append(current_cmnd)

            # Build params for parallel encoding
            _temp = sample_item.copy()
            print(profile)
            _temp.update({
                'cmnd': real_cmnd,
                'source_path_no_prefix': _paths['temporary_dst_path'],
                'source_path': 'file://' + _paths['temporary_dst_path'],
                'dest_path': _paths['uri_output_dst_path'],
                'weight': profile['weight'],
            })
            encode_items.append(_temp)
            # If everything success, add it and notify back nearly real-time
            output_paths.append(_paths)
            workflow_data['metadata']['output'] = output_paths

            current_percentage += 1
            # START OF: Update progress
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Build metadata for profile ' + str(profile['id']),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress

            print(_paths['uri_output_dst_path'])

        processes = list()
        # Run encoding with n processes
        continue_flag = True
        delta_percentage = round((85 - current_percentage) / (len(encode_items) * len(encode_items) * 2), 2)
        while continue_flag:
            # Run lowest profile
            lowest_item_idx, encode_items = pop_lowest_weight(encode_items)
            if lowest_item_idx is None:
                continue_flag = None
                continue
            print(' '.join(encode_items[lowest_item_idx]['cmnd']))

            current_percentage += delta_percentage
            # START OF: Update progress
            utils.update_progress(self_task=self, error=error, percentage=current_percentage, file_id=file_data['id'],
                                  workflow=workflow_data, file_info=information['file_info'],
                                  message='Run encode video cmnd',
                                  extras={'cmnd': ' '.join(encode_items[lowest_item_idx]['cmnd'])})
            # END OF: Update progress
            utils.run_command_with_progress(cmnd=encode_items[lowest_item_idx]['cmnd'], timeout=60*60,
                                            percentage_init=current_percentage, percentage_factor=delta_percentage/100,
                                            callback_func=utils.update_progress,
                                            callback_params={'self_task': self, 'error': 0, 'percentage': 40,
                                                             'message': '', 'file_id': file_data['id']})
            current_percentage += delta_percentage
            upload_queue.put({
                'src_path': encode_items[lowest_item_idx]['source_path'],
                'dst_path': encode_items[lowest_item_idx]['dest_path']
            })
            continue_flag = lowest_item_idx is not None

        _temp = None
        delta_percentage = round((95 - current_percentage) / (upload_queue.unfinished_tasks * 2), 2)
        stop = time.time() + 60*60
        while upload_queue.unfinished_tasks and time.time() < stop:
            if _temp != upload_queue.unfinished_tasks:
                current_percentage += delta_percentage
                _temp = upload_queue.unfinished_tasks
                # START OF: Update progress
                utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                      message='Wait for uploading encoded video files',
                                      file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
                # END OF: Update progress
            print('Wait for exit update queue - ' + str(upload_queue.unfinished_tasks))
            time.sleep(5)

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95,
                              message='Complete to upload encoded video files',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress

        workflow_data['status'] = Workflow.S_SUCCESS
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if _temp:
                    if os.path.isfile(_temp):
                        os.remove(_temp)
                    elif os.path.isdir(_temp):
                        shutil.rmtree(_temp)


# @shared_task(bind=True, base=BaseTask)
# def encode_video_stage(self, input_params):
#     error = 0
#     workflow_data = file_data = None
#     junk_paths = list()
#     encode_items = list()
#     sample_item = {
#         'cmnd': None,
#         'process': None,
#         'source_path': None,
#         'dest_path': None,
#         'sent': False,
#         'weight': 0,
#         'is_fetched': False,
#     }
#
#     try:
#         # Request File information
#         print('Query file / workflow information from server')
#         information = request_file_info(input_params['file_id'])
#
#         # Scan for workflow information
#         for _t_data in information['workflow']:
#             if str(_t_data['id']) == str(input_params['workflow']):
#                 workflow_data = _t_data
#                 break
#         file_data = information['file_info']
#         template_data = information['template_data']
#         video_profile_data = information['video_profile_data']
#
#         # Prepare input encode
#         temporary_src_path = workflow_data['metadata']['input']['phys_video_src_path']
#         uri_input_src_path = workflow_data['metadata']['input']['uri_video_src_path']
#         if not os.path.isfile(temporary_src_path):
#             # Download file if not in local
#             temporary_src_path = os.path.join(tempfile.gettempdir(), 'transcode',
#                                               utils.generate_random_string(16) + os.path.split(temporary_src_path)[-1])
#             _temp = os.path.dirname(temporary_src_path)
#             if not os.path.isdir(_temp):
#                 os.makedirs(_temp)
#             StorageAdapter.copy_file(uri_input_src_path, 'file://' + temporary_src_path)
#             junk_paths.append(temporary_src_path)
#         # Prepare output encode
#         rel_output_dst_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
#                                            utils.generate_random_string(8, str(workflow_data['id'])))
#         phys_output_dst_path = os.path.join(settings.PERMANENTLY_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
#         uri_output_dst_path = os.path.join(settings.URL_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
#         temporary_dst_path = os.path.join(tempfile.gettempdir(), 'transcode', rel_output_dst_path)
#         if not os.path.isdir(temporary_dst_path):
#             os.makedirs(temporary_dst_path)
#
#         # Notify this flow is in processing
#         workflow_data['status'] = Workflow.S_PROGRESSING
#         update_data_info(settings.API_UPDATE_WORKFLOW, {'workflow': workflow_data})
#
#         output_paths = list()
#         print('Build command-line and encode')
#         print('##################', len(video_profile_data))
#         for _key in video_profile_data:
#             profile = video_profile_data[_key]
#             # Build command line
#             template_id = profile['template']
#             cmnd = template_data[template_id]['pattern']
#             for _key in template_data[template_id]['keyword']:
#                 # Skip input and output tag
#                 if _key in ('<input>', '<output>', '<scale>'):
#                     continue
#                 # Remove < and > to query attributes and replace
#                 search = re.match(r'.*<([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
#                 if search:
#                     if search.group(1) in profile:
#                         _temp = float(search.group(2)) if search.group(2) else 1
#                         if _temp > 1:
#                             _temp = str(int(float(profile[search.group(1)]) * _temp))
#                         else:
#                             _temp = str(profile[search.group(1)])
#
#                         # Feature: same as width or height
#                         if (search.group(1).find('width') > -1 or search.group(1).find('height') > -1) \
#                                 and int(_temp) == 0:
#                             _temp = str(file_data['standard_metadata']['videos'][0][search.group(1)])
#
#                         cmnd = re.sub(_key, _temp, cmnd, re.IGNORECASE)
#             cmnd = cmnd.split(' ')
#             # Replace <input> / <output> tag and <scale>
#             _filename = utils.generate_random_string(8) + '.mp4'
#             _paths = {
#                 'data_type': 'video',
#                 'profile_id': profile['id'],
#                 'phys_output_dst_path': os.path.join(phys_output_dst_path, _filename),
#                 'uri_output_dst_path': os.path.join(uri_output_dst_path, _filename),
#                 'temporary_dst_path': os.path.join(temporary_dst_path, _filename),
#             }
#             junk_paths.append(_paths['temporary_dst_path'])
#             filter_cmnd = list()
#             real_cmnd = list()
#             for idx, val in enumerate(cmnd):
#                 current_cmnd = cmnd[idx]
#                 if val == '<input>':
#                     #
#                     # Replace <input> tag with input path
#                     #
#                     current_cmnd = temporary_src_path
#
#                     #
#                     # Feature: Embed logo
#                     current_cmnd, filter_cmnd, _temp = \
#                         logo.build_logo_parameters(input_cmnd=current_cmnd, filter_cmnd=filter_cmnd,
#                                                    encode_options=file_data['encode_options'],
#                                                    configs=workflow_data['feature']['metadata'])
#                     junk_paths.append(_temp)
#
#                 elif val == '<output>':
#                     #
#                     # Replace <output> tag with input path
#                     #
#                     current_cmnd = _paths['temporary_dst_path']
#                 elif val == '<scale>':
#                     #
#                     # Feature: Hardsub
#                     #
#                     filter_cmnd, _temp = \
#                         subtitle.build_subtitle_parameters(filter_cmnd=filter_cmnd,
#                                                            encode_options=file_data['encode_options'],)
#                     junk_paths.append(_temp)
#
#                     #
#                     # Feature change scan mode: Interlaced or Progressive
#                     #                           (these params always present at first)
#                     _temp = scan_mode.get_scan_type(encode_options=file_data['encode_options'],
#                                                     standard_metadata=file_data['standard_metadata'],
#                                                     configs=workflow_data['feature']['metadata'])
#                     _temp = _temp.split(',')
#                     if len(_temp) > 1:
#                         filter_cmnd = [_temp[0], ] + filter_cmnd + [_temp[1], ]
#                     else:
#                         filter_cmnd = filter_cmnd + _temp
#
#                     #
#                     # Feature: Crop
#                     #
#                     filter_cmnd = crop.build_crop_parameters(filter_cmnd=filter_cmnd,
#                                                              encode_options=file_data['encode_options'])
#
#                     #
#                     # Feature Set DAR
#                     #
#                     filter_cmnd = dar.build_dar_parameters(filter_cmnd=filter_cmnd,
#                                                            encode_options=file_data['encode_options'])
#
#
#                     ###############################################################################
#                     filter_cmnd = ','.join(filter_cmnd).strip(',').replace(',,', ',')
#                     #
#                     # Feature: Force encode by width or height (if these attributes available)
#                     for _key in ('<width>', '<height>'):
#                         search = re.match(r'.*<([a-zA-Z0-9-]+)[_]?(\d*[.]?\d*)?>.*', _key)
#                         if search:
#                             _temp = float(search.group(2)) if search.group(2) else 1
#                             if _temp > 1:
#                                 _temp = str(int(float(profile[search.group(1)]) * _temp))
#                             else:
#                                 _temp = str(profile[search.group(1)])
#                             filter_cmnd = str(filter_cmnd).replace(_key, _temp)
#                     ###############################################################################
#                     current_cmnd = filter_cmnd
#                 if type(current_cmnd) is list:
#                     real_cmnd += current_cmnd
#                 else:
#                     real_cmnd.append(current_cmnd)
#
#             # Build params for parallel encoding
#             _temp = sample_item.copy()
#             print(profile)
#             _temp.update({
#                 'cmnd': real_cmnd,
#                 'source_path': 'file://' + _paths['temporary_dst_path'],
#                 'dest_path': _paths['uri_output_dst_path'],
#                 'weight': profile['weight'],
#             })
#             encode_items.append(_temp)
#             # If everything success, add it and notify back nearly real-time
#             output_paths.append(_paths)
#             workflow_data['metadata']['output'] = output_paths
#             update_data_info(settings.API_UPDATE_WORKFLOW, {'workflow': workflow_data})
#
#             # Update file to output storage
#             print(_paths['uri_output_dst_path'])
#
#         processes = list()
#         # Run encoding with n processes
#         continue_flag = True
#         release_item_type = None
#         max_threads = calc_number_of_threads()
#         print('MAX THREADS: ' + str(max_threads))
#         while continue_flag:
#             # Run lowest profile
#             lowest_item_idx = highest_item_idx = None
#             if len(processes) < max_threads and (release_item_type is None or release_item_type == 'low'):
#                 lowest_item_idx, encode_items = pop_lowest_weight(encode_items)
#                 if lowest_item_idx is not None:
#                     print('# RUN LOWEST PROFILE')
#                     print(' '.join(encode_items[lowest_item_idx]['cmnd']))
#                     _temp = utils.run_command(encode_items[lowest_item_idx]['cmnd'], async=True)
#                     encode_items[lowest_item_idx]['process'] = _temp
#                     processes.append({
#                         'process': _temp,
#                         'item_idx': lowest_item_idx,
#                         'item_type': 'low',
#                     })
#
#             # Run highest profile
#             if len(processes) < max_threads and (release_item_type is None or release_item_type == 'high'):
#                 highest_item_idx, encode_items = pop_highest_weight(encode_items)
#                 if highest_item_idx is not None:
#                     print('# RUN HIGHEST PROFILE')
#                     print(' '.join(encode_items[highest_item_idx]['cmnd']))
#                     _temp = utils.run_command(encode_items[highest_item_idx]['cmnd'], async=True)
#                     encode_items[highest_item_idx]['process'] = _temp
#                     processes.append({
#                         'process': _temp,
#                         'item_idx': highest_item_idx,
#                         'item_type': 'high',
#                     })
#
#             # Check loop
#             continue_flag = (lowest_item_idx is not None) or (highest_item_idx is not None)
#             while len(processes) >= max_threads:
#                 for p_idx, p in enumerate(processes):
#                     if p['process'].poll() is not None:
#                         StorageAdapter.copy_file(encode_items[p['item_idx']]['source_path'],
#                                                  encode_items[p['item_idx']]['dest_path'])
#                         encode_items[p['item_idx']]['sent'] = True
#                         release_item_type = p['item_type']
#                         del processes[p_idx]
#                         break
#                 time.sleep(0.1)
#
#         # Securely check again
#         for e_idx, e in enumerate(encode_items):
#             if not e['sent']:
#                 e['process'].wait()
#                 StorageAdapter.copy_file(e['source_path'], e['dest_path'])
#                 encode_items[e_idx]['sent'] = True
#         workflow_data['status'] = Workflow.S_SUCCESS
#         return {
#             'error': error,
#             'file_id': input_params['file_id'],
#             'file_info': None,
#             'workflow': workflow_data,
#         }
#     except Exception as e:
#         print("Error: " + str(e.args))
#         raise
#     finally:
#         if junk_paths:
#             for _temp in junk_paths:
#                 if _temp:
#                     if os.path.isfile(_temp):
#                         os.remove(_temp)
#                     elif os.path.isdir(_temp):
#                         shutil.rmtree(_temp)