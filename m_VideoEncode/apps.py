__author__ = 'duybq'


CONFIGS = {
    "logo": {
        "top_right": {
            "cmnd": "overlay=main_w-overlay_w-<X>:<Y>"
        },
        "bottom_left": {
            "cmnd": "overlay=<X>:main_h-overlay_h-<Y>"
        },
        "bottom_right": {
            "cmnd": "overlay=(main_w-overlay_w-<X>):(main_h-overlay_h-<Y>)"
        },
        "top_left": {
            "cmnd": "overlay=<X>:<Y>"
        }
    },
    "progressive": {
        "scale": {
            "default": "scale=<width>:'trunc(ow/a/2)*2'",
            "optional": "scale=<width>:<height>"
        }
    },
    "interlaced": {
        "scale": {
            "default": "yadif=0:-1:0,scale=<width>:'trunc(ow/a/2)*2'",
            "optional": "yadif=0:-1:0,scale=<width>:<height>"
        }
    }
}
