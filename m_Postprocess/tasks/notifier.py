from __future__ import absolute_import

try:
    import ujson as json
except ImportError:
    import json
import os
import shutil
import requests
import traceback
from urllib.parse import urlparse
from celery import shared_task, states
from django.conf import settings
from simplejson.scanner import JSONDecodeError

from api_core.contrib import utils
from api_core.contrib.storage import StorageAdapter
from api_core.contrib.base_task import BaseTask, request_file_info

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def notify_add_playlist_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']
        resource_type = 1
        if str(file_data['owner']).lower().find('epg_') > -1:
            resource_type = 3
        elif str(file_data['name']).lower().find('trailer') > -1:
            resource_type = 2

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=5, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json; version=1.0'
        }
        # TODO: Apply for HLS mode only
        _temp = workflow_data['metadata']['input'][0]['uri_output_dst_path']
        connector = StorageAdapter.get_connector_by_uri(_temp)
        parser = urlparse(_temp)
        callback_id = None
        if 'callback_id' in file_data['encode_options'] and file_data['encode_options']['callback_id']:
            callback_id = file_data['encode_options']['callback_id']
        params = {
            'callback_id': callback_id,
            'transcode_id': file_data['id'],
            'transcode_title': file_data['name'],
            'master_playlist_url': connector.get_link(parser.path + '/playlist.m3u8'),
            'encrypted_protocol': 1,
            'resource_type': resource_type,
            'root_location': _temp,
        }

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=20, message='Begin to notify playlist',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        #
        print('Request URL', settings.API_ADD_RESOURCE_PLAYLIST)
        print('Request params', str(params))
        resp = requests.post(settings.API_ADD_RESOURCE_PLAYLIST, data=json.dumps(params), headers=headers, timeout=300)
        print('Results', resp.text)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=80, message='Complete to notify playlist',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        data = resp.json()
        if str(data['e']) != '0':
            workflow_data['status'] = Workflow.S_FAILURE
        else:
            workflow_data['metadata']['output'] = {
                'playlist_resource_id': data['data']['id'],
            }
            workflow_data['status'] = Workflow.S_SUCCESS

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95, message='Complete notifying task',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        resp.close()
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except JSONDecodeError:
        print("Decode error!")
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Decode errors!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _path in junk_paths:
                if os.path.isfile(_path):
                    os.remove(_path)
                elif os.path.isdir(_path):
                    shutil.rmtree(_path)


@shared_task(bind=True, base=BaseTask)
def notify_update_epg_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=5, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json; version=1.0'
        }
        callback_url = callback_id = None
        if 'callback_id' in file_data['encode_options'] and file_data['encode_options']['callback_id']:
            callback_id = file_data['encode_options']['callback_id']
        if 'callback_url' in file_data['encode_options'] and file_data['encode_options']['callback_url']:
            callback_url = file_data['encode_options']['callback_url']
        else:
            callback_url = settings.get('API_UPDATE_PROGRAM_EPG', None)
        params = {
            'callback_id': callback_id,
            'transcode_id': file_data['id'],
            'playlist_resource_id': workflow_data['metadata']['input']['playlist_resource_id'],
            'epg_id': str(file_data['owner']).split('_')[-1],
        }

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=20, message='Begin to notify EPG',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        #
        resp = requests.post(callback_url, data=json.dumps(params), headers=headers, timeout=300)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=80, message='Complete to notify EPG',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress

        data = resp.json()
        if str(data['e']) != '0':
            workflow_data['status'] = Workflow.S_FAILURE
        else:
            workflow_data['metadata']['output'] = params
            workflow_data['status'] = Workflow.S_SUCCESS

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95, message='Complete notifying task',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        resp.close()
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except JSONDecodeError:
        print("Decode error!")
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Decode errors!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _path in junk_paths:
                if os.path.isfile(_path):
                    os.remove(_path)
                elif os.path.isdir(_path):
                    shutil.rmtree(_path)


@shared_task(bind=True, base=BaseTask)
def notify_ingestion_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=5, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Get ingest options
        if 'ingest_mode' not in file_data['encode_options'] or not file_data['encode_options']['ingest_mode']:
            ingest_mode = ['non_drm_mode', ]
        else:
            ingest_mode = file_data['encode_options']['ingest_mode']
        if 'ingest_type' not in file_data['encode_options'] or not file_data['encode_options']['ingest_type']:
            ingest_type = ['hls', ]
        else:
            ingest_type = file_data['encode_options']['ingest_type']
        callback_id = None
        if 'callback_id' in file_data['encode_options'] and file_data['encode_options']['callback_id']:
            callback_id = file_data['encode_options']['callback_id']
        ingest_callback_url = None
        if 'ingest_callback_url' in file_data['encode_options'] and file_data['encode_options']['ingest_callback_url']:
            ingest_callback_url = file_data['encode_options']['ingest_callback_url']
        # Get ingest files
        output_files = list()
        if type(workflow_data['metadata']['input']) is not list:
            _temp_input = [workflow_data['metadata']['input']]
        else:
            _temp_input = workflow_data['metadata']['input']
        for _t_data in _temp_input:
            if 'uri_output_dst_path' in _t_data:
                connector = StorageAdapter.get_connector_by_uri(_t_data['uri_output_dst_path'])
                root_path = urlparse(_t_data['uri_output_dst_path']).path
                paths = connector.list_files(root_path, extensions=['.mp4'])
                for _path_info in paths:
                    _temp = _path_info['url'].replace(root_path, _t_data['uri_output_dst_path'])
                    output_files.append(_temp)

        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json; version=1.0'
        }
        params = {
            'callback_info': {
                'callback_params': {
                    'callback_id': callback_id,
                    'transcode_id': file_data['id'],
                },
                'callback_url': ingest_callback_url,
            },
            'files': output_files,
            'output_uri': file_data['encode_options']['ingest_output_uri'],
            'ingest_type': ingest_type,
            'ingest_mode': ingest_mode,
        }
        print('REQUEST TO INGESTION', settings.API_REQUEST_INGEST, str(params))

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=20, message='Begin to notify ingestion',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        #
        resp = requests.post(settings.API_REQUEST_INGEST, data=json.dumps(params), headers=headers, timeout=300)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=80, message='Complete to notify ingestion',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress

        print('RESPONSE', resp.text)
        data = resp.json()
        if str(data['error']) != '0':
            workflow_data['status'] = Workflow.S_FAILURE
        else:
            workflow_data['metadata']['output'] = params
            workflow_data['status'] = Workflow.S_SUCCESS

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95, message='Complete notifying ingestion',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        resp.close()
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except JSONDecodeError:
        print("Decode error!")
        traceback.print_exc()
        notify_ingestion_stage.apply_async(
                    args=[{'file_id': input_params['file_id'], 'workflow': str(input_params['workflow'])}],
                    countdown=5*30, task_id=str(input_params['workflow']), priority=6)
        raise Exception('MANUAL_RETRY: Notifying failed because of bad response format by remote API')
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _path in junk_paths:
                if os.path.isfile(_path):
                    os.remove(_path)
                elif os.path.isdir(_path):
                    shutil.rmtree(_path)