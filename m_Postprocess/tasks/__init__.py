from __future__ import absolute_import

import os
import shutil
import tempfile
import traceback
from datetime import datetime
from urllib.parse import urlparse
from celery import shared_task, states
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.base_task import BaseTask, request_file_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

from .notifier import notify_add_playlist_stage, notify_update_epg_stage, notify_ingestion_stage

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def post_progress_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break
        file_data = information['file_info']
        relative_output_path = os.path.join(str(datetime.now().date()), file_data['id'])

        if type(workflow_data['metadata']['input']) is not list:
            workflow_data['metadata']['input'] = [workflow_data['metadata']['input']]

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # For each input, process upload
        results = list()
        root_relative_path = os.path.splitext(file_data['name'])[0]
        current_percentage = 5
        delta_percentage = round((90 - current_percentage) / (len(workflow_data['metadata']['input']) * 4), 2)
        for input_item in workflow_data['metadata']['input']:
            temporary_src_path = input_item['phys_output_dst_path']
            uri_input_src_path = input_item['uri_output_dst_path']

            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Begin to download output file to local disk: ' + str(temporary_src_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress
            #
            if not os.path.isfile(temporary_src_path):
                # Download file if not in local
                temporary_src_path = os.path.join(tempfile.gettempdir(), 'transcode',
                                                  os.path.split(temporary_src_path)[-1])
                junk_paths.append(temporary_src_path)
                _temp = os.path.dirname(temporary_src_path)
                if not os.path.isdir(_temp):
                    os.makedirs(_temp)

                print("Copy file %s -> %s" % (str(uri_input_src_path), str(temporary_src_path)))
                StorageAdapter.copy_file(uri_input_src_path, 'file://' + temporary_src_path)

            # Unzip file
            if str(os.path.splitext(temporary_src_path)[-1]).lower().endswith('zip'):
                # generate relative path
                if temporary_src_path.lower().find('_hls'):
                    relative_output_path = os.path.join(root_relative_path, 'hls')
                elif temporary_src_path.lower().find('_dash'):
                    relative_output_path = os.path.join(root_relative_path, 'dash')
                elif temporary_src_path.lower().find('_smooth'):
                    relative_output_path = os.path.join(root_relative_path, 'smooth')
                else:
                    relative_output_path = os.path.join(root_relative_path, 'other')

                uri_output_dst_path = os.path.join(file_data['output_uri'], relative_output_path)
                temporary_src = os.path.join(tempfile.gettempdir(), 'transcode', 'unzip',
                                             os.path.splitext(os.path.basename(uri_input_src_path))[0])
                if not os.path.isdir(temporary_src):
                    os.makedirs(temporary_src)
                junk_paths.append(temporary_src)
                utils.unzip_file(temporary_src_path, temporary_src)

            else:
                # get output extention
                _temp = os.path.splitext(os.path.basename(temporary_src_path.lower()))[-1].strip('.')
                # generate relative path
                relative_output_path = os.path.join(root_relative_path, _temp)
                temporary_src = temporary_src_path
                uri_output_dst_path = os.path.join(file_data['output_uri'], relative_output_path,
                                                   os.path.basename(temporary_src))
            #
            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Complete to download output file to local disk: ' + str(temporary_src_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress

            connector = StorageAdapter.get_connector_by_uri(uri_output_dst_path)
            print("Upload from %s to %s" % (str(temporary_src), str(uri_output_dst_path)))
            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Begin to upload output file to user storage: ' + str(uri_output_dst_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress
            #
            if os.path.isdir(temporary_src):
                connector.upload_folder(temporary_src, urlparse(uri_output_dst_path).path)
            elif os.path.isfile(temporary_src):
                connector.upload_file(temporary_src, urlparse(uri_output_dst_path).path)
            #
            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Complete to upload output file to user storage: ' + str(temporary_src_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress
            results.append({'uri_output_dst_path': uri_output_dst_path})

        # START OF: Update progress
        current_percentage += delta_percentage
        utils.update_progress(self_task=self, error=error, percentage=95,
                              message='Complete to upload all output files to user storage',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        workflow_data['metadata']['output'] = results
        workflow_data['status'] = Workflow.S_SUCCESS
        file_data['output_uri'] = os.path.join(file_data['output_uri'], root_relative_path)
        if 'ingest_output_uri' in file_data['encode_options']:
            file_data['encode_options']['ingest_output_uri'] = \
                os.path.join(file_data['encode_options']['ingest_output_uri'], root_relative_path)
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': file_data,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _path in junk_paths:
                if os.path.isfile(_path):
                    os.remove(_path)
                elif os.path.isdir(_path):
                    shutil.rmtree(_path)
