import traceback
from celery import shared_task, states
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.storage import StorageAdapter
from api_core.contrib.base_task import BaseTask, request_file_info, update_data_info

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def clean_junk_files_stage(self, input_params):
    workflow_data = None
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################

    try:
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # START OF: Update progress
        utils.update_progress(self_task=self, error=0, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=None, file_info=None)
        # END OF: Update progress

        # Scan for workflow information
        current_percentage = 2
        delta_percentage = round((95 - current_percentage) / len(information['workflow']), 2)
        for _workflow_data in information['workflow']:
            current_percentage += delta_percentage
            # START OF: Update progress
            utils.update_progress(self_task=self, error=0, percentage=current_percentage,
                                  message='Clean junk from task ' + str(_workflow_data['id']),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress

            if str(_workflow_data['id']) == str(input_params['workflow']):
                workflow_data = _workflow_data
                continue
            # Force to list
            if type(_workflow_data['metadata']['input']) is dict:
                _temp = [_workflow_data['metadata']['input'], ]
            else:
                _temp = _workflow_data['metadata']['input']
            # Clean it
            for _t_data in _temp:
                # Clean raw input
                if 'phys_input_src_path' in _t_data:
                    print('Remove phys_input_src_path=' + str(_t_data['phys_input_src_path']))
                    if not StorageAdapter.remove_it('file://' + _t_data['phys_input_src_path']):
                        print('Remove uri_input_src_path=' + str(_t_data['uri_input_src_path']))
                        StorageAdapter.remove_it(_t_data['uri_input_src_path'])
                        # Clean raw input
                if 'phys_output_dst_path' in _t_data:
                    print('Remove phys_output_dst_path=' + str(_t_data['phys_output_dst_path']))
                    if not StorageAdapter.remove_it('file://' + _t_data['phys_output_dst_path']):
                        print('Remove uri_output_dst_path=' + str(_t_data['uri_output_dst_path']))
                        StorageAdapter.remove_it(_t_data['uri_output_dst_path'])
                # Clean encoded video output
                if 'phys_video_src_path' in _t_data:
                    print('Remove phys_video_src_path=' + str(_t_data['phys_video_src_path']))
                    if not StorageAdapter.remove_it('file://' + _t_data['phys_video_src_path']):
                        print('Remove uri_video_src_path=' + str(_t_data['uri_video_src_path']))
                        StorageAdapter.remove_it(_t_data['uri_video_src_path'])
                # Clean encoded audio output
                if 'phys_audio_src_paths' in _t_data:
                    for _path in _t_data['phys_audio_src_paths']:
                        print('Remove phys_path=' + str(_path['phys_path']))
                        if not StorageAdapter.remove_it('file://' + _path['phys_path']):
                            print('Remove uri_path=' + str(_path['uri_path']))
                            StorageAdapter.remove_it(_path['uri_path'])

        # START OF: Update progress
        utils.update_progress(self_task=self, error=0, percentage=95, message='Finish to clean garbage',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress

        workflow_data['status'] = Workflow.S_SUCCESS
        return {
            'error': 0,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
