try:
    import ujson as json
except ImportError:
    import json
import requests
from celery import states
from celery import shared_task
from django.db.models import Q

import Controller.tasks
from Monitor.tasks import recv_request_action
from api_core.contrib.utils import AdvancedRedisCache

from ..models import *

__author__ = 'duybq'


def switch_task(excluded_priority, file_id, input_metadata=None, output_metadata=None,
                publish_task=True, ended_task=False):
    workflow_condition = Q(file_id=file_id) & ~Q(disabled=True)
    workflow_queryset = Workflow.objects.filter(workflow_condition).only('id', 'status', 'priority', 'metadata')
    min_priority = None
    considered_workflow = list()
    count_not_finish_yet = 0
    for workflow_obj in workflow_queryset:
        if workflow_obj.status == Workflow.S_FAILURE:
            workflow_condition &= Q(feature__code='clean_junk_files_stage') & Q(status=Workflow.S_INITIAL)
            workflow_obj = Workflow.objects.filter(workflow_condition).only('id', 'status', 'priority', 'metadata').last()
            if workflow_obj:
                considered_workflow = [workflow_obj, ]
            break
        elif excluded_priority != workflow_obj.priority and workflow_obj.status != Workflow.S_INITIAL:
            continue
        elif excluded_priority == workflow_obj.priority:
            # Skip for the same priority
            if workflow_obj.status in (Workflow.S_INITIAL, Workflow.S_PROGRESSING):
                # Tracking if there are some flow is not done for this process
                count_not_finish_yet += 1
            continue

        # Lookup workflow at lowest priority
        if min_priority is None or min_priority > workflow_obj.priority:
            min_priority = workflow_obj.priority
            considered_workflow = [workflow_obj, ]
        elif min_priority == workflow_obj.priority:
            considered_workflow.append(workflow_obj)

    if not considered_workflow:
        # TODO: must be process
        return True
    workflow_ids = list()
    for workflow_obj in considered_workflow:
        if input_metadata:
            if not workflow_obj.metadata.get('input', None):
                workflow_obj.metadata['input'] = input_metadata
            elif type(workflow_obj.metadata['input']) is dict:
                workflow_obj.metadata['input'].update(input_metadata)
            elif type(workflow_obj.metadata['input']) is list:
                workflow_obj.metadata['input'].extend(e for e in input_metadata if e not in workflow_obj.metadata['input'])
            else:
                workflow_obj.metadata['input'] = input_metadata
        if output_metadata:
            if not workflow_obj.metadata.get('output', None):
                workflow_obj.metadata['output'] = output_metadata
            elif type(workflow_obj.metadata['output']) is dict:
                workflow_obj.metadata['output'].update(output_metadata)
            elif type(workflow_obj.metadata['output']) is list:
                workflow_obj.metadata['output'].extend(e for e in output_metadata if e not in workflow_obj.metadata['output'])
            else:
                workflow_obj.metadata['output'] = output_metadata
        workflow_obj.save()
        if count_not_finish_yet < 1 and publish_task or ended_task:
            # Skip not public task because there are some task has not been processed
            request_data = {
                'action': 'manual_schedule_task',
                'file_id': file_id,
                'task_id': str(workflow_obj.pk),
                'feature_module': workflow_obj.feature.module,
                'published_id': str(workflow_obj.pk),
                'priority': workflow_obj.metadata.get('task_priority', 3),
            }
            recv_request_action.apply_async(args=[request_data['action'], request_data])
            # getattr(Controller.tasks, workflow_obj.feature.module).apply_async(
            #     args=[{'file_id': file_id, 'workflow': str(workflow_obj.pk)}],
            #     countdown=30, task_id=str(workflow_obj.id), priority=workflow_obj.metadata.get('task_priority', 3))
        workflow_ids.append(str(workflow_obj.pk))
    return workflow_ids


def update_fileinfo(fileinfo):
    if isinstance(fileinfo, dict):
        fileinfo_obj = FileInfo.objects.get(pk=fileinfo['id'])
        for _t_key, _t_data in fileinfo.items():
            if _t_key in ('id', 'pk', 'feature', 'file', 'template', 'priority',
                          'video_profile', 'audio_profile', 'disabled'):
                continue
            # Only update the dict, not overwrite
            if type(_t_data) is dict:
                _temp = getattr(fileinfo_obj, _t_key)
                if _temp and type(_temp) is dict:
                    _temp.update(_t_data)
                    _t_data = _temp
            setattr(fileinfo_obj, _t_key, _t_data)
        fileinfo_obj.save()
    return True


def update_livetask(livetask):
    if isinstance(livetask, dict):
        live_obj = LiveEncodeTask.objects.get(pk=livetask['id'])
        for _t_key, _t_data in livetask.items():
            if _t_key in ('id', 'pk', 'feature', 'file', 'file_id', 'profile', 'profile_id', 'disabled'):
                continue
            # Only update the dict, not overwrite
            if type(_t_data) is dict:
                _temp = getattr(live_obj, _t_key)
                if _temp and type(_temp) is dict:
                    _temp.update(_t_data)
                    _t_data = _temp
            elif type(_t_data) is list:
                # Only extend the list, not overwrite
                _temp = getattr(live_obj, _t_key)
                if _temp and type(_temp) is list:
                    _t_data.extend(e for e in _temp if e not in _t_data)
            setattr(live_obj, _t_key, _t_data)
        live_obj.save()
    return True


def update_workflow(workflow, publish_task=False):
    if isinstance(workflow, dict):
        workflow_obj = Workflow.objects.get(pk=workflow['id'])
        for _t_key, _t_data in workflow.items():
            if _t_key in ('id', 'pk', 'feature', 'file', 'template', 'priority',
                          'video_profile', 'audio_profile', 'disabled'):
                continue
            # Only update the dict, not overwrite
            if type(_t_data) is dict:
                _temp = getattr(workflow_obj, _t_key)
                if _temp and type(_temp) is dict:
                    _temp.update(_t_data)
                    _t_data = _temp
            elif type(_t_data) is list:
                # Only extend the list, not overwrite
                _temp = getattr(workflow_obj, _t_key)
                if _temp and type(_temp) is list:
                    _t_data.extend(e for e in _temp if e not in _t_data)
            setattr(workflow_obj, _t_key, _t_data)
        workflow_obj.save()

        # Publish next task
        if publish_task:
            print('Publish task ' + str(workflow['id']))
            switch_task(excluded_priority=workflow['priority'], file_id=workflow['file'],
                        input_metadata=workflow['metadata']['output'],
                        output_metadata=None, publish_task=publish_task)
    return True


def notify_failure(file_id):
    fileinfo_obj = FileInfo.objects.get(pk=file_id)
    try:
        if 'ingest_callback_url' in fileinfo_obj.encode_options \
                and fileinfo_obj.encode_options['ingest_callback_url']:
            params = {
                'status': 3,
                'transcode_id': file_id,
                'callback_id': fileinfo_obj.encode_options['callback_id']
                                if 'callback_id' in fileinfo_obj.encode_options else None,
                'ingest_id': None,
                'description': file_id
            }
            headers = {
                'Content-Type': 'application/json',
                'Accept': 'application/json; version=1.0'
            }
            print('Notify failure to API: ' + str(fileinfo_obj.encode_options['ingest_callback_url']))
            print('Notify failure headers: ' + str(headers))
            print('Notify failure data: ' + str(params))
            resp = requests.post(fileinfo_obj.encode_options['ingest_callback_url'], data=json.dumps(params),
                                 headers=headers, timeout=5)
            if not resp.ok:
                raise Exception(resp.text)
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()


def set_intial_retry(file_id, workflow_id):
    fileinfo_obj = FileInfo.objects.get(pk=file_id)
    workflow_obj = Workflow.objects.get(pk=workflow_id)
    Workflow.objects.filter(file=fileinfo_obj, workflow=workflow_obj).update(status=Workflow.S_INITIAL)


@shared_task(bind=True)
def drive_events(self, body):
    publish_task = False
    cache = AdvancedRedisCache()
    task_id = body['task_id']
    file_id = body['result'].get('file_id', None)
    if 'file_info' in body['result'] and body['result']['file_info']:
        file_id = body['result']['file_info']['id']

    if isinstance(body['status'], list):
        body['status'] = body['status'].pop()
    must_publish_next_task = False
    if body['status'] == 'PROGRESSING':
        #
        # Status: PROGRESSING
        #
        print("PROGRESSING\tfile_id=%s\ttask_id=%s\tpercentage=%s\tmessage=%s"
              % (str(file_id), str(task_id), str(body['result'].get('percentage', None)),
                 str(body['result'].get('message', None))))
        cache_progress_key = FileInfo.get_progress_key(file_id)
        cache_progress_field = str(task_id)
        data = cache.hget(cache_progress_key, cache_progress_field)
        if data is None:
            data = {'percentage': 0, 'extras': {}}
        else:
            data = json.loads(data)
        if body['result'].get('extras', None):
            data['extras'].update(body['result'].get('extras'))
            if body['result']['extras'].get('cmnd', None):
                print("CMND of file_id=%s, task_id=%s: %s"
                      % (str(file_id), str(task_id), str(body['result']['extras']['cmnd'])))
        if body['result'].get('percentage', None):
            data['percentage'] = body['result'].get('percentage', None)
        cache.hset(cache_progress_key, cache_progress_field, json.dumps(data), 60*60*24*30)

    elif body['status'] == states.STARTED:
        #
        # Status: STARTED
        #
        print("STARTED\tfile_id=%s\ttask_id=%s\tpercentage=%s\tmessage=%s"
              % (str(file_id), str(task_id), str(body['result'].get('percentage', None)),
                 str(body['result'].get('message', None))))
        workflow = Workflow.objects.get(pk=body['task_id'])
        workflow.status = Workflow.S_PROGRESSING
        workflow.metadata.update(body.get('result', {}))
        workflow.save()

    elif body['status'] == states.SUCCESS:
        #
        # Status: SUCCESS
        #
        print("SUCCESS\tfile_id=%s\ttask_id=%s\tpercentage=%s\tmessage=%s"
              % (str(file_id), str(task_id), str(body['result'].get('percentage', None)),
                 str(body['result'].get('message', None))))
        cache_progress_key = FileInfo.get_progress_key(file_id)
        cache_progress_field = str(task_id)
        data = cache.hget(cache_progress_key, cache_progress_field)
        if data is None:
            data = {'percentage': 0, 'extras': {}}
        else:
            data = json.loads(data)
        if body['result'].get('extras', None):
            data['extras'].update(body['result'].get('extras', None))
        data['percentage'] = 100
        cache.hset(cache_progress_key, cache_progress_field, json.dumps(data), 60*60*24*30)
        must_publish_next_task = True

    elif body['status'] == states.FAILURE and 'exc_message' not in body['result']:
        #
        # Status: FAILURE
        #
        if str(body['result'].get('message', None)).find('MANUAL_RETRY') > -1:
            print("RETRY\tfile_id=%s\ttask_id=%s\tpercentage=%s\tmessage=%s"
              % (str(file_id), str(task_id), str(body['result'].get('percentage', None)),
                 str(body['result'].get('message', None))))
            set_intial_retry(file_id, task_id)
        else:
            print("FAILURE\tfile_id=%s\ttask_id=%s\tpercentage=%s\tmessage=%s"
                  % (str(file_id), str(task_id), str(body['result'].get('percentage', None)),
                     str(body['result'].get('message', None))))
            if 'workflow' in body['result'] and body['result']['workflow']:
                body['result']['workflow'].update({'status': Workflow.S_FAILURE})
                trace_info = body.get('traceback', None)
                trace_info = body['result'].get('traceback', None) if not trace_info else trace_info
                if trace_info:
                    print("FAILURE\tfile_id=%s\ttrace_info=%s" % (file_id, str(trace_info)))
                    body['result']['workflow']['metadata'].update({'reason': trace_info})
                else:
                    print("FAILURE\tfile_id=%s\ttrace_info=DO_NOT_HAVE_TRACEBACK" % file_id)
            notify_failure(file_id)
            must_publish_next_task = True

    # Update workflow
    if 'workflow' in body['result'] and body['result']['workflow']:
        update_workflow(body['result']['workflow'], publish_task=must_publish_next_task)
    # Update fileinfo
    if 'file_info' in body['result'] and body['result']['file_info']:
        update_fileinfo(body['result']['file_info'])
    if 'live_task' in body['result'] and body['result']['live_task']:
        update_livetask(body['result']['live_task'])
    return True
