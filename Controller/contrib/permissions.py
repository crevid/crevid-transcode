from rest_framework.permissions import BasePermission

__author__ = 'duybq'


class IsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated()
