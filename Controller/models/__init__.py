from .materials import *

import re
import traceback
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import pre_save

__author__ = 'duybq'


# define signal functions here

@receiver(pre_save, sender=CommandTemplate)
def command_handle_pre_save(sender, instance, **kwargs):
    if instance.pattern:
        params = str(instance.pattern).split(' ')
        keyword = list()
        for param in params:
            matches = re.findall(r'(<[a-zA-Z0-9_-]+>)', param, re.IGNORECASE)
            if matches:
                for _key in matches:
                    if _key not in keyword:
                        keyword.append(_key)
        instance.keyword = keyword