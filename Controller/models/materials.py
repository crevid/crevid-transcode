from jsonfield import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api_core.models import CommonInfo

__author__ = 'duybq'


class FileInfo(CommonInfo):
    name = models.CharField(max_length=512, default=None, null=True)
    uri = models.CharField(max_length=2048, null=False)
    owner = models.CharField(max_length=60, null=True)
    size = models.BigIntegerField(null=True)
    encode_options = JSONField(null=True, default=dict())
    original_metadata = JSONField(null=True, default=None)
    standard_metadata = JSONField(null=True, default=None)
    temporary_metadata = JSONField(null=True, default=None)
    output_uri = models.CharField(max_length=2048, null=True, default=None)
    disabled = models.NullBooleanField(default=None, db_index=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.name)

    @staticmethod
    def get_progress_key(file_id):
        return 'progress:' + str(file_id)


class CustomEncodingOption(CommonInfo):
    file = models.ForeignKey(FileInfo, null=False, db_index=True)
    profile_data = JSONField(null=True, default=None, blank=True)
    audio_profile_data = JSONField(null=True, default=None, blank=True)
    video_profile_data = JSONField(null=True, default=None, blank=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.file.name)


class Feature(CommonInfo):
    M_FORCE = 1
    M_OPTIONAL = 2
    MODES = (
        (M_FORCE, _('Force to apply to any workflow')),
        (M_OPTIONAL, _('Optionally apply')),
    )

    PREFIX_FT_PRE = 'pre_'
    PREFIX_FT_ENCODE = 'encode_'
    PREFIX_FT_MUX = 'mux_'
    PREFIX_FT_POST = 'post_'
    PREFIX_FT_CLEAN = 'clean_'

    name = models.CharField(max_length=512, default=None)
    code = models.CharField(max_length=32, null=False)
    module = models.CharField(max_length=64, null=True)
    priority = models.IntegerField(null=False)
    metadata = JSONField(null=True, default=None, blank=True)
    version = models.CharField(max_length=8, null=False, default=None)
    mode = models.IntegerField(default=M_OPTIONAL, choices=MODES, null=False, db_index=True)
    disabled = models.NullBooleanField(default=False, db_index=True)

    class Meta:
        unique_together = (('code', 'version', 'priority'),)

    def __str__(self):
        return str(self.id) + ' - ' + str(self.code) + ': ' + str(self.name)


class Workflow(CommonInfo):
    S_INITIAL = 0
    S_SUCCESS = 1
    S_PROGRESSING = 2
    S_FAILURE = -1
    STATUSES = (
        (S_INITIAL, _('Initial')),
        (S_SUCCESS, _('Job is successful')),
        (S_PROGRESSING, _('Job is in progress')),
        (S_FAILURE, _('Job is failed')),
    )

    file = models.ForeignKey(FileInfo, null=False, db_index=True)
    feature = models.ForeignKey(Feature, null=False, db_index=True)
    extra_key = models.CharField(max_length=40, null=True)
    priority = models.IntegerField(null=False)
    metadata = JSONField(null=False, default={
        'input': dict(),
        'output': dict(),
        'reason': dict(),
        'previous': None,
        'next': None
    })
    status = models.IntegerField(null=False, default=S_INITIAL, choices=STATUSES)
    disabled = models.NullBooleanField(default=False, db_index=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.file.name) + ' - ' + str(self.feature.name)


class CommandTemplate(CommonInfo):
    name = models.CharField(max_length=256, null=True, default=None)
    keyword = JSONField(null=True)
    pattern = models.TextField(null=True, default=None)

    def __str__(self):
        return str(self.id) + ': ' + str(self.name)


class VideoProfile(CommonInfo):
    name = models.CharField(max_length=256)
    weight = models.IntegerField(default=0)
    level = models.CharField(max_length=8, null=True, default=None)
    preset = models.CharField(max_length=8, null=True, default=None)
    vprofile = models.CharField(max_length=8, null=True, default=None)
    framerate = models.CharField(max_length=16, null=True, default=None)
    keyint = models.CharField(max_length=8, null=True, default=None)
    width = models.CharField(max_length=8, null=True, default=None)
    height = models.CharField(max_length=5, null=True, default=None)
    bitrate = models.CharField(max_length=32, null=True, default=None)
    maxrate = models.CharField(max_length=32, null=True, default=None)
    bufsize = models.CharField(max_length=32, null=True, default=None)
    template = models.ForeignKey(CommandTemplate, null=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.name)


class AudioProfile(CommonInfo):
    T_AAC = 'aac'
    T_AC3 = 'ac3'
    T_MP3 = 'mp3'
    AUDIO_TYPES = (
        (T_AAC, 'Audio AAC'),
        (T_AC3, 'Audio AC3'),
        (T_MP3, 'Audio MP3'),
    )

    name = models.CharField(max_length=256)
    audio_type = models.CharField(max_length=16, default=T_AAC, choices=AUDIO_TYPES)
    bitrate = models.CharField(max_length=50, null=True)
    volume = models.CharField(max_length=16, null=True)
    template = models.ForeignKey(CommandTemplate, null=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.name)


class ProfileMapper(CommonInfo):
    name = models.CharField(max_length=512, default=None)
    video_profile = models.ForeignKey(VideoProfile)
    audio_profile = models.ForeignKey(AudioProfile)
    live_template = models.ForeignKey(CommandTemplate, null=True, default=None)
    disabled = models.NullBooleanField(default=False, db_index=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.name)


class LiveEncodeTask(CommonInfo):
    S_INITIAL = 'initial'
    S_PROCESSING = 'processing'
    S_INACTIVE = 'inactive'
    S_ERROR = 'error'
    STATUSES = (
        (S_INITIAL, _('Initially')),
        (S_PROCESSING, _('Processing')),
        (S_INACTIVE, _('In-active by user')),
        (S_ERROR, _('Error')),
    )

    file = models.ForeignKey(FileInfo, null=False, db_index=True)
    profile = models.ForeignKey(ProfileMapper, null=True)
    status = models.CharField(max_length=32, null=False, default=S_INITIAL, choices=STATUSES)
    retries = models.IntegerField(default=0, editable=False)
    server_name = models.CharField(max_length=1024, null=True, default=None, editable=False)
    hls_stream = models.CharField(max_length=1024, null=True, default=None, editable=False)
    dash_stream = models.CharField(max_length=1024, null=True, default=None, editable=False)
    smooth_stream = models.CharField(max_length=1024, null=True, default=None, editable=False)
    disabled = models.NullBooleanField(default=False, db_index=True)

    def __str__(self):
        return str(self.id) + ': ' + str(self.file.name)