try:
    import ujson as json
except ImportError:
    import json
import os
from docker import Client
from django.db.models import Q
from django.core.management.base import BaseCommand

from ...models import LiveEncodeTask


class Command(BaseCommand):
    help = 'Command perform checking live task'
    temp_folder = None

    def create_parser(self, prog_name, subcommand):
        return super(Command, self).create_parser(prog_name, subcommand)

    def handle(self, *args, **options):
        self.process(options)

    def process(self, options):
        cli = Client(base_url='unix://tmp/docker.sock')
        queryset = LiveEncodeTask.objects.filter(Q(status=LiveEncodeTask.S_INACTIVE) & Q(disabled=False))

        for live_task in queryset:
            self.stdout.write('#### Process task: ' + str(live_task.pk))
            condition = ~Q(status=LiveEncodeTask.S_INACTIVE) & Q(disabled=False) & Q(file=live_task.file)
            check_remain_task = LiveEncodeTask.objects.filter(condition).exists()
            if check_remain_task:
                live_task.disabled = True
                live_task.save()
                continue

            docker_name = 'live_transcode_' + str(str(live_task.file_id).split('-')[0])
            docker_instance = cli.containers(all=True, filters={'name': docker_name})
            # Case: docker instance is removed
            if len(docker_instance) == 0:
                live_task.disabled = True
                live_task.status = LiveEncodeTask.S_INACTIVE
                live_task.save()
                continue

            docker_instance = docker_instance[0]
            cli.stop(docker_instance['Id'])
            cli.remove_container(docker_instance['Id'])
            live_task.disabled = True
            live_task.save()
        return True