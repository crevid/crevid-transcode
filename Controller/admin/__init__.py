from django.contrib import admin
from ..models import FileInfo, Feature, AudioProfile, VideoProfile, CommandTemplate, ProfileMapper, Workflow
from ..models import LiveEncodeTask, CustomEncodingOption

__author__ = 'duybq'


class FileInfoAdmin(admin.ModelAdmin):
    """
    Admin interface of FileInfo
    """
    list_display = ('id', 'name', 'owner', 'size', 'created_date', 'disabled')
    list_display_links = ('id', 'name', )
    search_fields = ['id', 'name', 'uri', 'owner', 'size', 'original_metadata',
                     'standard_metadata', 'temporary_metadata']
    ordering = ('-created_date', )
    list_filter = ('disabled', )
    readonly_fields = ('id', 'uri', 'size', 'temporary_metadata', 'created_date', 'modified_date')

admin.site.register(FileInfo, FileInfoAdmin)


class FeatureAdmin(admin.ModelAdmin):
    """
    Admin interface of Feature
    """
    list_display = ('name', 'code', 'module', 'priority', 'version', 'mode', 'disabled')
    list_display_links = ('name', )
    search_fields = ['id', 'name', 'code', 'module', 'priority', 'version', 'mode', 'metadata']
    ordering = ('priority', '-created_date')
    list_filter = ('mode', 'disabled', )
    readonly_fields = ('id', 'created_date', 'modified_date')

admin.site.register(Feature, FeatureAdmin)


class WorkflowAdmin(admin.ModelAdmin):
    """
    Admin interface of Workflow
    """
    list_display = ('id', 'file', 'feature', 'priority', 'status', 'disabled')
    list_display_links = ('id', )
    search_fields = ['id', 'file__id', 'file__name', 'feature__id', 'feature__code', 'extra_key', 'priority', 'metadata']
    ordering = ('-created_date', )
    list_filter = ('status', 'disabled', )
    readonly_fields = ('id', 'created_date', 'modified_date')

admin.site.register(Workflow, WorkflowAdmin)


class CommandTemplateAdmin(admin.ModelAdmin):
    """
    Admin interface of CommandTemplate
    """
    list_display = ('id', 'name', 'keyword', 'pattern')
    list_display_links = ('id', )
    search_fields = ['id', 'name', 'keyword', 'pattern']
    ordering = ('-created_date', )
    readonly_fields = ('id', 'keyword', 'created_date', 'modified_date')

admin.site.register(CommandTemplate, CommandTemplateAdmin)


class VideoProfileAdmin(admin.ModelAdmin):
    """
    Admin interface of VideoProfile
    """
    list_display = ('id', 'name', 'vprofile', 'keyint', 'width', 'height', 'bitrate', 'bufsize', 'template')
    list_display_links = ('id', )
    search_fields = ['id', 'name', 'vprofile', 'width', 'height', 'bitrate', 'bufsize', 'template__id']
    ordering = ('-created_date', )
    readonly_fields = ('id', 'created_date', 'modified_date')

admin.site.register(VideoProfile, VideoProfileAdmin)


class AudioProfileAdmin(admin.ModelAdmin):
    """
    Admin interface of AudioProfile
    """
    list_display = ('id', 'name', 'bitrate', 'volume', 'template')
    list_display_links = ('id', )
    search_fields = ['id', 'name', 'bitrate', 'volume', 'template__id']
    ordering = ('-created_date', )
    readonly_fields = ('id', 'created_date', 'modified_date')

admin.site.register(AudioProfile, AudioProfileAdmin)


class ProfileMapperAdmin(admin.ModelAdmin):
    """
    Admin interface of ProfileMapper
    """
    list_display = ('id', 'name', 'video_profile', 'audio_profile', 'disabled')
    list_display_links = ('id', )
    search_fields = ['id', 'name', 'video_profile__id', 'audio_profile__id']
    ordering = ('-created_date', )
    list_filter = ('disabled', )
    readonly_fields = ('id', 'created_date', 'modified_date')

admin.site.register(ProfileMapper, ProfileMapperAdmin)


def make_live_task_to_inactive(modeladmin, request, queryset):
    queryset.update(status=LiveEncodeTask.S_INACTIVE)
make_live_task_to_inactive.short_description = "Mark selected live tasks as inactive"


class LiveEncodeTaskAdmin(admin.ModelAdmin):
    """
    Admin interface of LiveEncodeTask
    """
    list_display = ('id', 'file', 'status', 'retries', 'server_name', 'disabled')
    list_display_links = ('id', )
    search_fields = ['id', 'file__id', 'file__name', 'status', 'retries', 'server_name',
                     'hls_stream', 'dash_stream', 'smooth_stream']
    ordering = ('-created_date', )
    list_filter = ('status', 'disabled', )
    readonly_fields = ('id', 'file', 'status', 'retries', 'server_name', 'created_date', 'modified_date',
                       'hls_stream', 'dash_stream', 'smooth_stream')
    actions = [make_live_task_to_inactive, ]

admin.site.register(LiveEncodeTask, LiveEncodeTaskAdmin)


class CustomEncodingOptionAdmin(admin.ModelAdmin):
    """
    Admin interface of LiveEncodeTask
    """
    list_display = ('id', 'file', 'profile_data', 'audio_profile_data', 'video_profile_data')
    list_display_links = ('id', )
    search_fields = ['id', 'file__id']
    ordering = ('-created_date', )
    readonly_fields = ('id', 'file', 'profile_data', 'audio_profile_data', 'video_profile_data')

admin.site.register(CustomEncodingOption, CustomEncodingOptionAdmin)
