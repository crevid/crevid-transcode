from django.conf.urls import url
from django.views.decorators.cache import cache_page

from .views import FileInfoView, UpdateLiveTaskView

__author__ = 'duybq'


urlpatterns = [
    # Back-end API
    url(r'file/info$', FileInfoView.as_view(), name='generate_information_view'),
    url(r'live/update$', UpdateLiveTaskView.as_view(), name='update_live_task_view'),
]

