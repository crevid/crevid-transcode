try:
    import ujson as json
except ImportError:
    import json
import time
import requests
from django.db.models import Q
from django.db import transaction
from rest_framework import status
from rest_framework.response import Response

from api_core.contrib import utils, exceptions
from api_core.define import RESPONSE_SUCCESS_MSG
from api_core.contrib.views import APIViewBase, APIVersion

from ..tasks.driven import switch_task

from ..models import *
from ..serializers import *

__author__ = 'duybq'


class FileInfoView(APIViewBase):

    authentication_classes = ()
    permission_classes = ()

    def get(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.generate_file_info_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'file_id': None,
        }
        params.update({
            'ip': utils.get_client_ip(request),
            'ua': request.META['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in request.META else '',
        })
        params['file_id'] = request.GET.get('file_id', None)
        if not params['file_id']:
            raise exceptions.ParamDoesNotExist
        return params

    def generate_file_info_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        file_info = FileInfoSerializer(FileInfo.objects.get(pk=params['file_id'])).data
        workflow_queryset = Workflow.objects.filter(Q(file_id=params['file_id']) & ~Q(disabled=True))\
                                            .order_by('priority', 'created_date')
        profile_ids = list()
        video_profile_ids = list()
        audio_profile_ids = list()
        template_ids = list()
        workflow = list()
        live_tasks = list()
        for _w in workflow_queryset:
            _temp = WorkflowSerializer(_w).data
            workflow.append(_temp)
            if 'live_mode' in file_info['encode_options'] and file_info['encode_options']['live_mode'] and \
                    str(_temp['feature']['code']).startswith(Feature.PREFIX_FT_MUX):
                # Live Mode
                for e in LiveEncodeTask.objects.filter(file_id=file_info['id']):
                    live_tasks.append(LiveEncodeTaskSerializer(e).data)
                    profile = e.profile
                    if profile:
                        template_ids.append(profile.live_template_id)
                        profile_ids.append(str(profile.pk))
                        video_profile_ids.append(str(profile.video_profile_id))
                        audio_profile_ids.append(str(profile.audio_profile_id))

            elif str(_temp['feature']['code']).startswith(Feature.PREFIX_FT_ENCODE):
                # VOD Mode
                profile_ids += _temp['metadata']['profile_mappers']
                if 'video_profiles' in _temp['metadata'] and _temp['metadata']['video_profiles']:
                    video_profile_ids += _temp['metadata']['video_profiles']
                if 'audio_profiles' in _temp['metadata'] and _temp['metadata']['audio_profiles']:
                    audio_profile_ids += _temp['metadata']['audio_profiles']

        result = {
            'file_info': file_info,
            'workflow': workflow,
            'live_tasks': live_tasks,
            'profile_data': dict(),
            'video_profile_data': dict(),
            'audio_profile_data': dict(),
        }
        try:
            extra_option = CustomEncodingOption.objects.get(file_id=params['file_id'])
        except CustomEncodingOption.DoesNotExist:
            extra_option = None

        valid_uuid = [_temp for _temp in profile_ids if _temp and len(_temp) >= 32]
        if profile_ids:
            profiles = ProfileMapper.objects.filter(Q(id__in=valid_uuid) & ~Q(disabled=True))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = ProfileMapperShortSerializer(profile).data
            result.update({'profile_data': profile_data})
        # Add extra option
        if extra_option and extra_option.profile_data:
            result['profile_data'].update(extra_option.profile_data)

        valid_uuid = [_temp for _temp in video_profile_ids if _temp and len(_temp) >= 32]
        if video_profile_ids:
            profiles = VideoProfile.objects.filter(Q(id__in=valid_uuid))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = VideoProfileShortSerializer(profile).data
                template_ids.append(profile_data[str(profile.pk)]['template'])
            result.update({'video_profile_data': profile_data})
        # Add extra option
        if extra_option and extra_option.video_profile_data:
            _temp = extra_option.video_profile_data
            result['video_profile_data'].update(_temp)
            template_ids += [_temp[_key]['template'] for _key in _temp]

        valid_uuid = [_temp for _temp in audio_profile_ids if _temp and len(_temp) >= 32]
        if audio_profile_ids:
            profiles = AudioProfile.objects.filter(Q(id__in=valid_uuid))
            profile_data = dict()
            for profile in profiles:
                profile_data[str(profile.pk)] = AudioProfileShortSerializer(profile).data
                template_ids.append(profile_data[str(profile.pk)]['template'])
            result.update({'audio_profile_data': profile_data})
        # Add extra option
        if extra_option and extra_option.audio_profile_data:
            _temp = extra_option.audio_profile_data
            result['audio_profile_data'].update(_temp)
            template_ids += [_temp[_key]['template'] for _key in _temp]

        if template_ids:
            template_data = dict()
            for e in CommandTemplate.objects.filter(id__in=template_ids):
                template_data[str(e.pk)] = CommandTemplateSerializer(e).data
            result.update({'template_data': template_data})
        return result


class UpdateLiveTaskView(APIViewBase):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None, **kwargs):
        if request.version == APIVersion.VERSION_1_0:
            info = self.update_live_task_info_1_0(request, kwargs)
        else:
            raise exceptions.BadAPIVersion()

        # produce response
        return Response(data={
            'time': time.time(),
            'error': 0,
            'detail': str(RESPONSE_SUCCESS_MSG),
            'data': info
        }, status=status.HTTP_201_CREATED)

    def get_input_params(self, request, dispatch_params):
        params = {
            'error': None,
            'live_task': None,
            'description': None,
        }
        params.update(request.data)
        params.update({
            'ip': utils.get_client_ip(request),
            'ua': request.META['HTTP_USER_AGENT'] if 'HTTP_USER_AGENT' in request.META else '',
        })
        if not params['live_task']:
            raise exceptions.ParamDoesNotExist
        return params

    @transaction.atomic
    def update_live_task_info_1_0(self, request, dispatch_params):
        params = self.get_input_params(request, dispatch_params)
        # Update workflow
        if type(params['live_task']) is dict:
            live_obj = LiveEncodeTask.objects.get(pk=params['live_task']['id'])
            for _t_key, _t_data in params['live_task'].items():
                if _t_key in ('id', 'pk', 'feature', 'file', 'file_id', 'profile', 'profile_id', 'disabled'):
                    continue
                # Only update the dict, not overwrite
                if type(_t_data) is dict:
                    _temp = getattr(live_obj, _t_key)
                    if _temp and type(_temp) is dict:
                        _temp.update(_t_data)
                        _t_data = _temp
                elif type(_t_data) is list:
                    # Only extend the list, not overwrite
                    _temp = getattr(live_obj, _t_key)
                    if _temp and type(_temp) is list:
                        _t_data.extend(e for e in _temp if e not in _t_data)
                setattr(live_obj, _t_key, _t_data)
        else:
            live_obj = LiveEncodeTask.objects.get(pk=params['live_task']['id'])
            live_obj.metadata.update({'reason': params['description']})
            live_obj.status = LiveEncodeTask.S_ERROR
        live_obj.save()
        return True