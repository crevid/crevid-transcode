from __future__ import absolute_import

import os
import shutil
import psutil
import tempfile
import traceback
from celery import shared_task, states
from django.conf import settings
from urllib.parse import urlparse

from api_core.contrib import utils
from api_core.contrib.base_task import BaseTask, request_file_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

from ..contrib.external import download_external_source
from ..contrib.information import verify_input_information

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def pre_progress_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    enter_retry_mode = False

    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################

    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])

        # Scan for workflow information
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        if information['file_info']['encode_options'] and 'live_mode' in information['file_info']['encode_options'] \
            and information['file_info']['encode_options']['live_mode']:
            live_processing(input_params, information, workflow_data, junk_paths, self_task=self)
        else:
            if not os.path.isdir(settings.PERMANENTLY_INPUT_STORAGE_PREFIX):
                os.makedirs(settings.PERMANENTLY_INPUT_STORAGE_PREFIX)
            free_space = psutil.disk_usage(settings.PERMANENTLY_INPUT_STORAGE_PREFIX).free
            if free_space < 20*1024*1024*1024:
                enter_retry_mode = True
                pre_progress_stage.apply_async(
                    args=[{'file_id': input_params['file_id'], 'workflow': str(input_params['workflow'])}],
                    countdown=5*30, task_id=str(input_params['workflow']), priority=6)
                raise Exception('MANUAL_RETRY: Disk is less than 10 GB')
            offline_processing(input_params, information, workflow_data, junk_paths, self_task=self)
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': information['file_info'],
            'workflow': workflow_data,
        }
    except Exception as e:
        if not enter_retry_mode:
            print("Error: " + str(e.args))
            traceback.print_exc()
            utils.update_progress(self_task=self, error=1, percentage=None,
                                  message=str(e.args), traceback=traceback.format_exc(),
                                  state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        if not enter_retry_mode:
            utils.update_progress(self_task=self, error=1, percentage=None,
                                  message='Critical exception!', traceback=traceback.format_exc(),
                                  state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _path in junk_paths:
                if os.path.isfile(_path):
                    os.remove(_path)
                elif os.path.isdir(_path):
                    shutil.rmtree(_path)


def offline_processing(input_params, information, workflow_data, junk_paths, self_task=None):
    file_data = information['file_info']
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=2, message='Generate temporary metadata',
                          file_id=file_data['id'], workflow=workflow_data, file_info=file_data)
    # END OF: Update progress

    resolver = urlparse(file_data['uri'])
    _temp = os.path.basename(os.path.splitext(resolver.path)[0])
    _temp = ''.join((e if e.isalnum() else '_') for e in _temp)
    if not file_data['name']:
        file_data['name'] = _temp + os.path.splitext(resolver.path)[-1]
    rel_input_src_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
                                      utils.generate_random_string(8, str(workflow_data['id'])), file_data['name'])
    phys_input_src_path = os.path.join(settings.PERMANENTLY_INPUT_STORAGE_PREFIX, rel_input_src_path)
    uri_input_src_path = os.path.join(settings.URL_INPUT_STORAGE_PREFIX, rel_input_src_path)
    file_data['temporary_metadata'] = {
        'phys_input_src_path': phys_input_src_path,
        'uri_input_src_path': uri_input_src_path,
    }
    if not os.path.isdir(os.path.dirname(phys_input_src_path)):
        os.makedirs(os.path.dirname(phys_input_src_path))
    junk_paths.append(phys_input_src_path)

    workflow_data['status'] = Workflow.S_PROGRESSING
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=5, message='Update temporary metadata',
                          file_id=file_data['id'], workflow=workflow_data, file_info=file_data)
    # END OF: Update progress

    print('Download source to temporary storage')
    if 'load_input_from_external_service' in file_data['encode_options'] and \
            file_data['encode_options']['load_input_from_external_service']:
        if 'external_input_quality' in file_data['encode_options']:
            _quality = file_data['encode_options']['external_input_quality']
        else:
            _quality = 'best'
        phys_input_src_path += "_%s.mp4" % _quality
        uri_input_src_path += "_%s.mp4" % _quality
        # START OF: Update progress
        utils.update_progress(self_task=self_task, error=0, percentage=7, file_id=file_data['id'],
                              message='Begin to download external source')
        # END OF: Update progress
        #
        download_external_source(file_data['uri'], quality=_quality, output_file_path=phys_input_src_path)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self_task, error=0, percentage=15, file_id=file_data['id'],
                              message='Complete to download external source')
        # END OF: Update progress
    else:
        # START OF: Update progress
        utils.update_progress(self_task=self_task, error=0, percentage=7, file_id=file_data['id'],
                              message='Begin to download non-external source')
        # END OF: Update progress
        #
        StorageAdapter.copy_file(file_data['uri'], 'file://' + phys_input_src_path)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self_task, error=0, percentage=15, file_id=file_data['id'],
                              message='Complete to download non-external source')
        # END OF: Update progress

    file_data['size'] = os.path.getsize(phys_input_src_path)

    print('Check MediaInfo and FFProbe')

    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=19, file_id=file_data['id'],
                          message='Begin to check MEDIAINFO')
    # END OF: Update progress
    #
    mediainfo = utils.get_mediainfo(phys_input_src_path)
    #
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=25, file_id=file_data['id'],
                          message='Complete to check MEDIAINFO')
    # END OF: Update progress

    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=26, file_id=file_data['id'],
                          message='Begin to check FFPROBE')
    # END OF: Update progress
    #
    ffprobe = utils.probe_video_file(phys_input_src_path)
    #
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=34, file_id=file_data['id'],
                          message='Complete to check FFPROBE')
    # END OF: Update progress

    file_data['original_metadata'] = {'mediainfo': mediainfo, 'ffprobe': ffprobe, }
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=35, file_id=file_data['id'],
                          message='Begin to verify metadata')
    # END OF: Update progress
    #
    file_data['standard_metadata'] = verify_input_information(file_data['original_metadata']['mediainfo'],
                                                              file_data['original_metadata']['ffprobe'])
    if not file_data['standard_metadata']:
        raise Exception('Pre-progress stage was failed!')

    print('Extract video and audio')
    _phys_prefix, _phys_ext = os.path.splitext(phys_input_src_path)
    _uri_prefix, _uri_ext = os.path.splitext(uri_input_src_path)
    phys_video_src_path = _phys_prefix + '_video'
    uri_video_src_path = _uri_prefix + '_video'
    has_hardsub = not('subtitle' not in file_data['encode_options']
                       or 'input_uri' not in file_data['encode_options']['subtitle']
                       or not file_data['encode_options']['subtitle']['input_uri'])
    if has_hardsub or _phys_ext.lower() in ('.mpg', '.mpeg'):
        _phys_ext = _uri_ext = '.mkv'
    phys_video_src_path += _phys_ext
    uri_video_src_path += _phys_ext

    if len(file_data['standard_metadata']['videos']) == 1:
        _temp = 'v'
    else:
        _temp = str(file_data['standard_metadata']['videos'][0]['stream'])
    cmnd = ['ffmpeg', '-fflags', '+genpts', '-i', phys_input_src_path]

    #
    # Extract Video
    MAX_PIECE_SIZE = 10*1024*1024*1024
    if False and os.path.getsize(phys_input_src_path) > MAX_PIECE_SIZE:
        # Case 1: Big file size
        # calculate split size
        # n_splits = file_data['size'] /1
        # duration = file_obj.duration
        # splits = file_obj.num_concurrency
        # # calculate maximum number of splits base on size
        # while file_data['size'] / float(splits) > max_piece_file:
        #     splits += 1
        # # calculate segment time
        # segment_time = ceil(duration / float(splits))
        # # check if end time is even
        # if segment_time % 2 != 0:
        #     segment_time += 1
        # if segment_time == 0:
        #     segment_time = 2

        # Minor case: harsub
        if has_hardsub:
            # Hardsub
            temp_sub_path = os.path.join(tempfile.gettempdir(),
                                         os.path.basename(file_data['encode_options']['subtitle']['input_uri']))
            StorageAdapter.copy_file(file_data['encode_options']['subtitle']['input_uri'], 'file://' + temp_sub_path)
            junk_paths.append(temp_sub_path)
            cmnd += ['-fix_sub_duration', '-i', temp_sub_path]

        # Default for all cases
        cmnd += ['-vcodec', 'copy', '-map', "0:%s" % _temp, '-reset_timestamps', '1', '-map_metadata', '-1',
                 '-segment_time', '20', '-f', 'segment']
    else:
        # Case 2: otherwise
        cmnd += ['-vcodec', 'copy', '-map', "0:%s" % _temp]
    # Append output
    cmnd += ['-y', phys_video_src_path]

    #
    # Extract Audio
    phys_audio_src_paths = list()
    for _t_data in file_data['standard_metadata']['audios']:
        if str(_t_data['format']).lower().find('ac3') > -1:
            output_ext = 'ac3'
        elif str(_t_data['format']).lower().find('aac') > -1:
            output_ext = 'aac'
        elif str(_t_data['format']).lower().find('dca') > -1:
            output_ext = 'dts'
        elif str(_t_data['format']).lower().find('pcm') > -1:
            output_ext = 'wav'
        elif str(_t_data['format']).lower().find('mp2') > -1:
            output_ext = 'mp2'
        elif str(_t_data['format']).lower().find('mp3') > -1:
            output_ext = 'mp3'
        elif str(_t_data['format']).lower().find('flac') > -1:
            output_ext = 'flac'
        else:
            output_ext = 'mkv'
        _phys_path = _phys_prefix + '_audio-' + str(_t_data['stream']) + '.' + output_ext
        _uri_path = _uri_prefix + '_audio-' + str(_t_data['stream']) + '.' + output_ext
        cmnd += ['-acodec', 'copy', '-map', "0:%s" % str(_t_data['stream']), '-y', _phys_path]
        phys_audio_src_paths.append({
            'info': _t_data,
            'phys_path': _phys_path,
            'uri_path': _uri_path,
        })
    # Run command-line extracting
    print('######')
    print(' '.join(cmnd))

    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=40, file_id=file_data['id'],
                          message='Begin to extract audio & video', extras={'cmnd': ' '.join(cmnd)})
    # END OF: Update progress
    #
    utils.run_command_with_progress(cmnd=cmnd, percentage_init=40, percentage_factor=0.5, timeout=60*60,
                                    callback_func=utils.update_progress,
                                    callback_params={'self_task': self_task, 'error': 0, 'percentage': 40,
                                                     'message': '', 'file_id': file_data['id']})
    #
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=95, file_id=file_data['id'],
                          message='Complete to extract audio & video')
    # END OF: Update progress

    file_data['temporary_metadata'].update({
        'uri_video_src_path': uri_video_src_path,
        'phys_video_src_path': phys_video_src_path,
        'phys_audio_src_paths': phys_audio_src_paths,
    })

    workflow_data['status'] = Workflow.S_SUCCESS
    workflow_data['metadata'].update({
        'input': input_params['workflow'],
        'output': file_data['temporary_metadata']
    })
    return True


def live_processing(input_params, information, workflow_data, junk_paths, self_task=None):
    file_data = information['file_info']
    print('Check MediaInfo and FFProbe')

    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=20, file_id=file_data['id'],
                          message='Begin to check MEDIAINFO')
    # END OF: Update progress
    #
    mediainfo = utils.get_mediainfo(file_data['uri'])
    #
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=55, file_id=file_data['id'],
                          message='Complete to check MEDIAINFO')
    # END OF: Update progress

    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=60, file_id=file_data['id'],
                          message='Begin to check FFPROBE')
    # END OF: Update progress
    #
    ffprobe = utils.probe_video_file(file_data['uri'])
    #
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=80, file_id=file_data['id'],
                          message='Complete to check FFPROBE')
    # END OF: Update progress

    file_data['original_metadata'] = {'mediainfo': mediainfo, 'ffprobe': ffprobe, }
    # START OF: Update progress
    utils.update_progress(self_task=self_task, error=0, percentage=90, file_id=file_data['id'],
                          message='Begin to verify metadata')
    # END OF: Update progress
    #
    file_data['standard_metadata'] = verify_input_information(file_data['original_metadata']['mediainfo'],
                                                              file_data['original_metadata']['ffprobe'])
    workflow_data['status'] = Workflow.S_SUCCESS
    workflow_data['metadata'].update({
        'input': input_params['workflow'],
        'output': {}
    })
    return True
