try:
    import ujson as json
except ImportError:
    import json
import os

from api_core.contrib import utils

__author__ = 'duybq'


def get_external_metadata(uri):
    cmnd = ['livestreamer', '--json', '--http-timeout', '20', uri]
    info = utils.run_command(cmnd, timeout=60 * 5)
    info = json.loads(info)
    if 'error' in info:
        raise Exception(info['error'])
    return info


def download_external_source(uri, quality, output_file_path):
    metadata = get_external_metadata(uri)['streams']
    if quality not in metadata:
        quality = 'best'
    output_folder = os.path.dirname(output_file_path)
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    cmnd = ['livestreamer', '--http-stream-timeout', '60', '--http-timeout', '20', '--force',
            '--output', output_file_path, uri, quality]
    return utils.run_command(cmnd, timeout=60 * 60 * 2)
