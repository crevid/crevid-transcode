import re

__author__ = 'duybq'


def _get_code_framerate():
    return ['23.976', '24', '25', '29.970', '30', '50', '60']


def _get_code_name():
    return ['h264', 'vc1', 'mpeg2video', 'hevc', 'mpeg4', 'flv', 'flv1', 'mpeg1video', 'vp8', 'vp9', 'wmv1', 'wmv2',
            'wmv3', 'rawvideo', 'rv10', 'rv20', 'rv30', 'rv40', 'mts2', 'mvc1', 'mvc2']


def _merge_list(A, B, key):
    if len(A) == len(B) == 1:
        for temp_b in B:
            for temp_a in A:
                temp = list()
                temp_list = temp_b.copy()
                temp_list.update(temp_a)
                if (key not in temp_a or temp_a[key] is None) and key in temp_b:
                    temp_list[key] = temp_b[key]
                elif (key not in temp_b or temp_b[key] is None) and key in temp_a:
                    temp_list[key] = temp_a[key]
                temp.append(temp_list)
                return temp
    if len(A) >= len(B):
        temp_list = A
        considered = B
    else:
        temp_list = B
        considered = A
    flag = False
    for the_item in considered:
        for id_a in range(0,len(temp_list)):
            if temp_list[id_a][key] == the_item[key]:
                temp_list[id_a] = temp_list[id_a].copy()
                temp_list[id_a].update(the_item.items())
                flag = True
        if flag == False:
            temp_list.append(the_item)
        else:
            flag = False
    return temp_list


def _get_video_stream(streams_media):
    if '@streamid' in streams_media and streams_media['@streamid']:
        return str(int(streams_media['@streamid']) - 1)
    if 'streamid' in streams_media and streams_media['streamid']:
        return str(int(streams_media['streamid']) - 1)
    if 'ID' in streams_media and streams_media['ID']:
        return str(int(streams_media['ID']) - 1)
    return None


def _get_video_interlaced(streams_media):
    interlaced = False
    if 'Scan_order' in streams_media or\
            ('Scan_type' in streams_media and str(streams_media['Scan_type']).lower().find('interlaced') > -1):
        interlaced = True
    return interlaced


def _get_video_framerate(streams_media, info_framerate):

    def set_framerate(fr, info_framerate):
        for ori_fr in info_framerate:
            if fr[0:2] == ori_fr[0:2]:
                return ori_fr
        return None

    if 'Frame_rate' in streams_media:
        fr = str(streams_media['Frame_rate']).replace(" fps", "")
        if fr in info_framerate:
            return fr
        else:
            return set_framerate(fr, info_framerate)
    elif 'Original_frame_rate' in streams_media:
        fr = str(streams_media['Original_frame_rate']).replace(" fps", "")
        if fr in info_framerate:
            return fr
        else:
            return set_framerate(fr, info_framerate)

    return None


def _get_video_duration(streams_media):
    if 'Duration' in streams_media:
        return str(streams_media['Duration'])
    if 'duration' in streams_media:
        return str(streams_media['duration'])
    return None


def _get_video_bitrate(streams_media):
    if 'Bit_rate' in streams_media:
        return streams_media['Bit_rate']
    if 'Bitrate' in streams_media:
        return streams_media['Bitrate']
    if 'bitrate' in streams_media:
        return streams_media['bitrate']
    return None


def _get_video_width(streams_media):
    if 'Width' in streams_media:
        return streams_media['Width'].replace(" ", "").replace("pixels", "")
    if 'width' in streams_media:
        return streams_media['width'].replace(" ", "").replace("pixels", "")
    return None


def _get_video_height(streams_media):
    if 'Height' in streams_media:
        return streams_media['Height'].replace(" ", "").replace("pixels", "")
    if 'height' in streams_media:
        return streams_media['height'].replace(" ", "").replace("pixels", "")
    return None


def _check_video_from_mediainfo(video_mediainfo, bitrate_video, duration_video):
    videos = list()
    for streams_media in video_mediainfo['File']['track']:
        if streams_media['@type'] == 'Video':
            try:
                stream_ = _get_video_stream(streams_media)
                interlaced_ = _get_video_interlaced(streams_media)
                framerate_ = _get_video_framerate(streams_media, _get_code_framerate())
                duration_ = _get_video_duration(streams_media)
                bitrate_ = bitrate_video if bitrate_video else _get_video_bitrate(streams_media)
                width = _get_video_width(streams_media)
                height = _get_video_height(streams_media)
                videos.append({
                    'stream': stream_,
                    'interlaced': interlaced_,
                    'framerate': framerate_,
                    'duration': duration_video,
                    'bitrate': bitrate_,
                    'width': width,
                    'height': height
                })
            except:
                pass
    return videos


def _check_video_from_ffprobe(video_info):
    if not video_info:
        return None
    videos = list()
    for stream_video in video_info:
        try:
            stream_index = str(stream_video['index'])
            codec_name = str(stream_video['codec_name'])
            if codec_name not in _get_code_name():
                return None
            width_source = str(stream_video['width'])
            height_source = str(stream_video['height'])
            # r_frame_rate = video_stream['r_frame_rate']
            videos.append({
                'stream': stream_index,
                'width': width_source,
                'height': height_source
            })
        except:
            pass
    return videos


def _process_info_video(info_video_mediainfo, info_video_ffprobe, bitrate_video, duration_video):
    info_video_on_ffprobe = info_video_on_mediainfo = None
    if info_video_ffprobe:
        info_video_on_ffprobe = _check_video_from_ffprobe(info_video_ffprobe)
    if info_video_mediainfo and 'File' in info_video_mediainfo:
        info_video_on_mediainfo = _check_video_from_mediainfo(info_video_mediainfo, bitrate_video, duration_video)

    if not info_video_on_mediainfo and not info_video_on_ffprobe:
        raise Exception('Error: info_video is null')
    elif not info_video_on_ffprobe:
        return info_video_on_mediainfo
    elif not info_video_on_mediainfo:
        return info_video_on_ffprobe
    else:
        info_video = _merge_list(info_video_on_mediainfo, info_video_on_ffprobe, 'Stream')
    return info_video


def _check_audio(audio_info):
    audios = list()
    for audio_stream in audio_info:
        try:
            streams_audio_default = audio_stream['index']
            channels = audio_stream['channels']
            if str(audio_stream['codec_name']).lower().find('mp3') > -1:
                audio_type = 'mp3'
            elif 'channels' in audio_stream:
                if audio_stream['channels'] >= 3 and str(audio_stream['codec_name']).find('ac3') > -1:
                    audio_type = 'ac3'
                else:
                    audio_type = 'aac'
            elif 'Channel_s_' in audio_stream:
                audio_stream_temp = audio_stream['Channel_s_'].split(' ')
                if int(audio_stream_temp[0]) >= 3 and str(audio_stream['codec_name']).find('ac3') > -1:
                    audio_type = 'ac3'
                else:
                    audio_type = 'aac'
            else:
                audio_type = 'aac'
            bitrate_source_audio = '64'
            if 'bit_rate' in audio_stream:
                bitrate_source_audio = str(int(audio_stream['bit_rate']) / 1000)
            elif 'sample_rate' in audio_stream:
                bitrate_source_audio = str(int(audio_stream['sample_rate']) / 1000)
            audio_default = '0'
            if 'disposition' in audio_stream:
                if 'default' in audio_stream['disposition']:
                    audio_default = str(audio_stream['disposition']['default'])
            result = {
                'stream': streams_audio_default,
                'type': audio_type,
                'channel': channels,
                'bitrate': bitrate_source_audio,
                'default': audio_default,
                'format': audio_stream['codec_name'],
                'sample_fmt': audio_stream['sample_fmt'] if 'sample_fmt' in audio_stream else None,
                'channel_layout': None,
                'channel_position': None,
            }
            if 'channel_layout' in audio_stream:
                result.update({'channel_layout': audio_stream['channel_layout']})
                search = re.match(r'^.+\((.+)\).*$', audio_stream['channel_layout'])
                if search:
                    result.update({'channel_position': search.group(1)})
                elif str(audio_stream['channel_layout']).lower().find('mono') > -1:
                    result.update({'channel_position': 'FC'})
            audios.append(result)
        except:
            pass
    return audios


def verify_input_information(mediainfo, ffprobe):
    if not ffprobe and not mediainfo:
        return None

    audios_info = list()
    videos_info = list()
    for streams in ffprobe['streams']:
        if str(streams['codec_type']).find('audio') > -1:
            audios_info.append(streams)
        elif str(streams['codec_type']).find('video') > -1:
            videos_info.append(streams)

    # Get bitrate
    bitrate = 0
    try:
        key = 'bit_rate' if 'bit_rate' in ffprobe['format'] else 'Bit_rate'
        bitrate = int(ffprobe['format'][key])
    except:
        try:
            key = 'bit_rate' if 'bit_rate' in ffprobe['format'] else 'Bit_rate'
            bitrate = int(ffprobe['streams'][0][key])
        except:
            print('> Cannot find bitrate with ffprobe tool')
            try:
                settings = mediainfo['File']['track'][1]['Encoding_settings']
                bitrate = int(re.search('^.*bitrate=(.+?) .*$', settings).group(1))
            except:
                print('> Cannot find bitrate with mediainfo tool')

    bitrate_video = str(bitrate / 1024)
    duration_video = str(ffprobe['format']['duration']) if 'duration' in ffprobe['format'] else None
    info_video = _process_info_video(mediainfo, videos_info, bitrate_video, duration_video)
    # return failed if can't get video info
    if not info_video:
        return None
    info_audio = _check_audio(audios_info)
    Result_Audio = {'audios': info_audio}
    Result_Video = {'videos': info_video}

    Result = Result_Video.copy()
    Result.update(Result_Audio)
    return Result