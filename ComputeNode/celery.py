from __future__ import absolute_import

import os
from celery import Celery, current_app
from billiard.process import current_process
from celery.signals import celeryd_after_setup, worker_ready, worker_process_shutdown
from kombu import Exchange

from .contrib.device import DeviceInfo
from . import settings


app = Celery('ComputeNode', include=['ComputeNode.tasks'])
app.config_from_object(settings)


@celeryd_after_setup.connect
def prepare_on_setup(sender, instance, **kwargs):
    if not os.geteuid() == 0:
        print('Script must be run as root')
        # sys.exit(1)
    # Get hostname of this device
    device_info = DeviceInfo()
    # listen hostname with queue
    hostname_queue_name = "hostname-%s" % str(device_info.hostname)
    instance.app.amqp.queues.select_add(queue=hostname_queue_name, exchange=hostname_queue_name)


@worker_ready.connect
def worker_on_ready(sender, **kwargs):
    with sender.app.connection() as conn:
        # Get data
        device_info = DeviceInfo()
        request_data = {
            'action': 'initial_notify_device_info',
        }
        request_data.update(device_info.get_all_infos())
        # Notify data
        hostname_queue_name = "hostname-%s" % str(request_data['hostname'])
        request_data.update({'control_queue': hostname_queue_name})
        sender.app.send_task('Monitor.tasks.server.recv_request_action', connection=conn,
                             args=(request_data['action'], request_data),
                             queue='mon_routing_queue', exchange=Exchange('mon_routing_queue', 'direct'))


if __name__ == '__main__':
    app.start()
