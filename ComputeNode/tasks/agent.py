import traceback

from kombu import Exchange
from celery import shared_task, current_app
from ..contrib.device import DeviceInfo

__author__ = 'duybq'


@shared_task(bind=True)
def request_device_info(self):
    # This task will be executed on agent
    try:
        device_info = DeviceInfo()
        request_data = {
            'action': 'notify_device_info',
        }
        request_data.update(device_info.get_all_infos())
        # Notify data
        hostname_queue_name = "hostname-%s" % str(request_data['hostname'])
        request_data.update({'control_queue': hostname_queue_name})
        current_app.send_task('Monitor.tasks.server.recv_request_action',
                              args=(request_data['action'], request_data),
                              queue='mon_routing_queue', exchange=Exchange('mon_routing_queue', 'direct'))

    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        raise
