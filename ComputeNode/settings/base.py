import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Celery Broker

ENABLE_UTC = True

TIMEZONE = "UTC"

from kombu import Queue, Exchange
TASK_QUEUES = (
    Queue('default_queue', Exchange('default_queue', 'direct'), queue_arguments={'x-max-priority': 10}),
    Queue('mon_routing_queue', Exchange('mon_routing_queue', 'direct')),
)

TASK_ROUTES = {
    'Monitor.tasks.server.recv_request_action': {
        'queue': 'mon_routing_queue',
    },
}

CELERY_QUEUE_HA_POLICY = 'all'

CELERY_DEFAULT_QUEUE = 'default_queue'

CELERY_TASK_QUEUE_MAX_PRIORITY = 10

CELERY_REMOTE_TRACEBACKS = True

CELERY_TRACK_STARTED = True

CELERY_ACKS_LATE = True

CELERY_SEND_TASK_SENT_EVENT = True

CELERY_TASK_SERIALIZER = 'json'

CELERY_TASK_PUBLISH_RETRY = True

CELERY_IGNORE_RESULT = True

CELERY_STORE_ERRORS_EVEN_IF_IGNORED = True

CELERYD_TASK_TIME_LIMIT = 60 * 60 * 4

CELERY_TASK_CREATE_MISSING_QUEUES = True

CELERY_DEFAULT_DELIVERY_MODE = "persistent"

CELERY_RESULT_BACKEND = None

CELERY_RESULT_SERIALIZER = 'json'

CELERY_RESULT_PERSISTENT = True

# Remove this because it makes RPC mode hanging
# RESULT_EXPIRES = 60 * 60 * 24 * 30

CELERY_ACCEPT_CONTENT = ['json']

CELERYD_PREFETCH_MULTIPLIER = 1

CELERYD_MAX_TASKS_PER_CHILD = 1000

CELERYD_POOL_RESTARTS = True

CELERY_WORKER_DIRECT = False

CELERY_ENABLE_REMOTE_CONTROL = True

CELERYD_SEND_EVENTS = True

CELERY_REDIS_MAX_CONNECTIONS = 4

BROKER_HEARTBEAT = 60

BROKER_HEARTBEAT_CHECKRATE = 2

BROKER_POOL_LIMIT = 8

# Custom
