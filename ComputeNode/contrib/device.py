import re
import psutil
import socket
from distutils.spawn import find_executable

from .utils import run_command


class DeviceInfo(object):

    hostname = None
    gpu_nvidia_info = None
    gpu_intel_info = None
    cpu_info = None
    memory_info = None
    disk_info = None
    net_info = None

    def __init__(self, hostname=socket.gethostname()):
        self.hostname = hostname

    def get_vga_info(self):
        if find_executable('nvidia-smi'):
            info = run_command(['nvidia-smi', 'dmon', '-c', '1'], async=False, timeout=10)
            lines = info.split("\n")
            if len(lines) > 2:
                headers = re.split(r'\s+', lines[0])
                values = re.split(r'\s+', lines[2])
                self.gpu_nvidia_info = dict(zip(headers, values))
                self.gpu_nvidia_info.update({'#': 'gpu-nvidia'})
        return {'gpu_nvidia': self.gpu_nvidia_info, 'gpu_intel': self.gpu_intel_info}

    def get_cpu_info(self):
        self.cpu_info = {
            'cores': psutil.cpu_count(),
            'used': psutil.cpu_percent(interval=1),
        }
        return self.cpu_info

    def get_memory_info(self):
        phys_memory = psutil.virtual_memory()
        swap_mem = psutil.swap_memory()
        self.memory_info = {
            'total_phys_memory': round(float(phys_memory.total)/1024.0/1024.0, 2),
            'used_phys_memory': round(float(phys_memory.used)/1024.0/1024.0, 2),
            'total_swap_memory': round(float(swap_mem.total)/1024.0/1024.0, 2),
            'used_swap_memory': round(float(swap_mem.used)/1024.0/1024.0, 2),
        }
        return self.memory_info

    def get_disk_info(self, dirs=('/',)):
        self.disk_info = dict()
        for the_dir in dirs:
            dir_info = psutil.disk_usage(the_dir)
            self.disk_info[str(the_dir)] = {
                'total': round(float(dir_info.total)/1024.0/1024.0/1024.0, 2),
                'used': round(float(dir_info.used)/1024.0/1024.0/1024.0, 2),
            }
        return self.disk_info

    def get_network_info(self, interfaces=None):
        self.net_info = dict()
        infos = psutil.net_if_addrs()
        for interface_name in infos:
            # Skip interfaces: loopback, virtual interface of dockers
            if (str(interface_name).lower().find('lo') > -1) or str(interface_name).lower().startswith('veth') \
                    or str(interface_name).lower().startswith('docker') \
                    or ((interfaces is not None) and (interface_name not in interfaces)):
                continue
            # Scan for data
            for info in infos[interface_name]:
                # Only get IPv4 & IPv6
                if info.family not in (socket.AF_INET, socket.AF_INET6):
                    continue
                # Priority to get IPv4, next is IPv6
                if interface_name in self.net_info and info.family == socket.AF_INET6:
                    continue
                self.net_info[interface_name] = {
                    'family': info.family,
                    'address': info.address,
                    'netmask': info.netmask,
                }
        return self.net_info

    def get_all_infos(self, disks=('/',), interfaces=None):
        return {
            'hostname': self.hostname,
            'vga': self.get_vga_info(),
            'cpu': self.get_cpu_info(),
            'memory': self.get_memory_info(),
            'disk': self.get_disk_info(disks),
            'network': self.get_network_info(interfaces)
        }
