try:
    import ujson as json
except ImportError:
    import json
try:
    import zipfile
except ImportError:
    zipfile = None
import time
import subprocess

__author__ = 'duybq'


def run_command(cmnd, async=False, timeout=None):
    p = subprocess.Popen(cmnd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                         bufsize=1024*1024)
    if not async:
        if timeout:
            poll_seconds = .250
            deadline = time.time() + min(timeout, 60*60*2)
            while time.time() < deadline and p.poll() is None:
                time.sleep(poll_seconds)
            if not p.poll():
                try:
                    p.terminate()
                except Exception:
                    pass
        info, error = p.communicate()
        if error:
            if not timeout:
                raise Exception('Error: ' + error + ' ---- ' + str(cmnd))
            else:
                return 'Time-out'
        if not info:
            print('Command print EMPTY to console: ' + str(cmnd))
        return info
    return p
