import time
import logging
import traceback
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework.exceptions import APIException

__author__ = 'duybq'


class NotAuthenticated(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Authentication credentials were not provided.')


class PermissionDenied(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('You do not have permission to perform this action.')


class BadAPIVersion(APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_detail = _('This API version is not supported.')


class DoesNotExistAbstract(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    custom_type = 'DoesNotExist'


class ParamDoesNotExist(DoesNotExistAbstract):
    default_detail = _('Missing param(s).')


class InvalidParam(DoesNotExistAbstract):
    status_code = status.HTTP_406_NOT_ACCEPTABLE
    default_detail = _('Invalid param(s).')


def custom_exception_handler(exc, context):
    """
    :param exc: This is the exception instance
    :type exc: Exception
    :param context: This is a dict of context information
    :type context: dict
    """
    logger = logging.getLogger('Transcode')
    # Call REST framework's default exception handler first to get the standard error response.
    response = exception_handler(exc, context)

    # Get request data
    request = context['request']
    extra_info = {
        'params': {
            'get': request.GET.dict(),
            'post': request.POST.dict(),
            'raw_post_data': request.raw_post_data if hasattr(request, 'raw_post_data') else None,
            'data': request.data if hasattr(request, 'data') else None,
        },
    }
    # check if it is fatal error
    if response is None:
        status_code = status.HTTP_501_NOT_IMPLEMENTED
        logger.exception(exc, extra=extra_info)
        response = Response(exception=exc, status=status_code)
    else:
        # logging first
        if hasattr(exc, 'custom_type') and exc.custom_type == 'DoesNotExist':
            logger.warning(exc, extra=extra_info)
        else:
            # fatal error
            logger.exception(exc, extra=extra_info)

    # Now add the more info to the response.
    status_code = response.status_code
    if response is not None and not status.is_redirect(status_code):
        if not response.data:
            response.data = {'error': 0, 'data': None, 'detail': None}
        response.data['error'] = 0 if status.is_success(status_code) or status.is_informational(status_code) else 1
        response.data['data'] = None
        if settings.DEBUG:
            response.data['data'] = traceback.format_exc()
            traceback.print_exc()
        if 'debug' in request.GET or settings.DEBUG:
            response.data['detail'] = str(exc)
        response.data['time'] = time.time()
    return response
