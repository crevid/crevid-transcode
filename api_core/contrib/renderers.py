try:
    import ujson as json
except ImportError:
    import json
from rest_framework import renderers

__author__ = 'duybq'


class XMLRenderer(renderers.BaseRenderer):
    media_type = 'text/xml'
    format = 'xml'
    charset = 'utf-8'

    def render(self, data, media_type=None, renderer_context=None):
        return data
