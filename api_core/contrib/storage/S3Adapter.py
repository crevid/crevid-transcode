import re
import os
import glob
import boto
import boto.s3.multipart
import time
import threading
import traceback
import subprocess
from boto.exception import S3ResponseError

from .HTTPAdapter import HTTPConnector

__author__ = 'duybq'


def _advance_upload(conn, transfer_file, bucket_name, s3_key_name=None, use_rr=True, make_public=False):
    if s3_key_name is None:
        s3_key_name = os.path.basename(transfer_file)
    bucket = conn.lookup(bucket_name)
    mb_size = os.path.getsize(transfer_file) / (1024 * 1024)
    if mb_size <= 1024:
        _standard_transfer(bucket, s3_key_name, transfer_file, use_rr)
    else:
        _multipart_upload(conn, bucket, s3_key_name, transfer_file, mb_size, use_rr)
    s3_key = bucket.get_key(s3_key_name)
    if make_public:
        s3_key.set_acl("public-read")


def _standard_transfer(bucket, s3_key_name, transfer_file, use_rr):
    new_s3_item = bucket.new_key(s3_key_name)
    new_s3_item.set_contents_from_filename(transfer_file, reduced_redundancy=use_rr)


def _mp_from_ids(conn, mp_id, mp_keyname, mp_bucketname):
    """Get the multipart upload from the bucket and multipart IDs.

    This allows us to reconstitute a connection to the upload
    from within multiprocessing functions.
    """
    bucket = conn.lookup(mp_bucketname)
    mp = boto.s3.multipart.MultiPartUpload(bucket)
    mp.key_name = mp_keyname
    mp.id = mp_id
    return mp


def _transfer_part(conn, mp_id, mp_keyname, mp_bucketname, i, part):
    """Transfer a part of a multipart upload. Designed to be run in parallel.
    """
    try:
        mp = _mp_from_ids(conn, mp_id, mp_keyname, mp_bucketname)
        print(" Transferring", i, part)
        with open(part, 'rb') as t_handle:
            try:
                mp.upload_part_from_file(t_handle, i + 1)
            except Exception:
                traceback.print_exc()
                time.sleep(60)
                mp.upload_part_from_file(t_handle, i + 1)
    finally:
        if os.path.isfile(part):
            os.remove(part)


def _multipart_upload(conn, bucket, s3_key_name, tarball, mb_size, use_rr=True):
    """Upload large files using Amazon's multipart upload functionality.
    """
    parts = None
    try:
        def split_file(in_file, mb_size, split_num=5):
            prefix = os.path.join(os.path.dirname(in_file),
                                  "%sS3PART" % (os.path.basename(s3_key_name)))
            split_size = int(min(mb_size / (split_num * 2.0), 250))
            if not os.path.exists("%saa" % prefix):
                cl = ["split", "-b%sm" % split_size, in_file, prefix]
                subprocess.check_call(cl)
            return sorted(glob.glob("%s*" % prefix))

        mp = bucket.initiate_multipart_upload(s3_key_name, reduced_redundancy=use_rr)
        file_size = int(os.path.getsize(tarball))
        min_size = 1024 * 1024 * 5
        standard_size = 1024 * 1024 * 512
        num_splits = 2
        for divided in range(2, 100):
            part_size = int(file_size / divided)
            if part_size > standard_size or file_size % part_size <= min_size:
                continue
            num_splits = divided
            break
        parts = split_file(tarball, mb_size, num_splits)
        for (i, part) in enumerate(parts):
            _transfer_part(conn, mp.id, mp.key_name, mp.bucket_name, i, part)
        mp.complete_upload()
    finally:
        if parts and isinstance(parts, list):
            for path in parts:
                try:
                    if os.path.isfile(path):
                        os.remove(path)
                except Exception:
                    pass


class S3Connector:
    connector = None

    def __init__(self, host, user, password, port=None, proxy=None, is_secure=False):
        if proxy:
            self.connector = boto.connect_s3(user, password, host=host, proxy=proxy,
                                             proxy_port=port, is_secure=is_secure)
        else:
            self.connector = boto.connect_s3(user, password, host=host, is_secure=is_secure)

    def get_or_create_bucket(self, bucketname):
        try:
            return self.connector.get_bucket(bucketname)
        except S3ResponseError:
            return self.connector.create_bucket(bucketname)

    def upload_file(self, source_path, output_path, **kwargs):
        # print "STARTING UPLOAD %s -> %s" % (str(source_path), str(output_path))
        output_path = re.sub(r'\\+', '/', output_path)
        output_path = re.sub(r'\/{2,}', '/', output_path)
        output_path = re.sub(r'^\/', '', output_path)
        output_base_dir = os.path.dirname(output_path)
        output_filename = os.path.basename(output_path)
        # get bucket
        parts = output_base_dir.split('/')
        bucket = self.get_or_create_bucket(parts[0])
        # get key
        key_name = os.path.join(output_base_dir, output_filename).replace(parts[0], '')
        key_name = re.sub(r'^\/', '', key_name)
        key = bucket.get_key(key_name)
        if key:
            bucket.delete_key(key_name)

        try:
            _advance_upload(self.connector, source_path, bucket.name, key_name,
                            make_public=kwargs.get('make_public', True))
        except Exception:
            import traceback
            traceback.print_exc()
            time.sleep(60)
            _advance_upload(self.connector, source_path, bucket.name, key_name)
        # print "UPLOAD DONE!"
        return True

    def upload_folder(self, source_path, output_path, **kwargs):
        try:
            self.upload_folder_multithreads(source_path, output_path, nthreads=64, item_per_threads=256, **kwargs)
        except Exception:
            traceback.print_exc()
            upload_file_names = os.listdir(source_path)
            # upload file
            for filename in upload_file_names:
                source_file = os.path.join(source_path, filename)
                dest_file = os.path.join(output_path, filename)
                if os.path.isfile(source_file):
                    self.upload_file(source_file, dest_file)
                elif os.path.isdir(source_file):
                    self.upload_folder(source_file, dest_file)
        return True

    def _upload_a_set_of_files(self, info, bucket, **kwargs):
        for e in info:
            _advance_upload(self.connector, e['phys_path'], bucket.name, e['key_name'],
                            make_public=kwargs.get('make_public', True))
        return True

    def upload_folder_multithreads(self, source_path, output_path, nthreads=32, item_per_threads=1024, **kwargs):
        print("Start to scan dir")
        nthreads = max(1, min(2048, int(nthreads))) + 1
        item_per_threads = max(1, min(4096, int(item_per_threads))) + 1
        local_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(source_path) for f in filenames]
        output_path = re.sub(r'\\+', '/', output_path)
        output_path = re.sub(r'\/{2,}', '/', output_path)
        output_path = re.sub(r'^\/', '', output_path)
        output_path = re.sub(r'\/$', '', output_path)
        # get bucket
        parts = output_path.split('/')
        output_bucket = self.get_or_create_bucket(parts[0])
        output_key_name = '/'.join(parts[1:]) if len(parts) > 1 else ''

        print("Upload: %s files with %s threads and %s items per thread" \
              % (str(len(local_files)), str(nthreads), str(item_per_threads)))
        threads = list()
        while len(local_files) > 0:
            print("Remain: %s" % str(len(local_files)))
            for _temp in range(0, nthreads - len(threads) + 1):
                _info = list()
                for _count in range(0, item_per_threads):
                    if len(local_files) == 0:
                        break
                    _path = local_files.pop()
                    # get key
                    key_name = _path.replace(source_path, output_key_name)
                    key_name = re.sub(r'^\/', '', key_name)
                    key_name = re.sub(r'\/$', '', key_name)
                    _info.append({
                        'phys_path': _path,
                        'key_name': key_name,
                    })
                # Lunch threads
                kwargs.update({
                    'info': _info,
                    'bucket': output_bucket,
                })
                t = threading.Thread(target=self._upload_a_set_of_files, kwargs=kwargs)
                t.start()
                threads.append(t)
            print("\t* Wait for processing...")
            _count = len(threads)
            _temp = max(1, int(nthreads * 0.9))
            _unprocessed_files = len(local_files)
            while (_count > _temp and _unprocessed_files > 0) or (_count > 0 and _unprocessed_files == 0):
                for _idx, _value in enumerate(threads):
                    if not _value.isAlive():
                        print("\t -> Finished thread: %s / %s" % (str(_value.name), str(len(threads))))
                        threads.pop(_idx)
                _count = len(threads)
                time.sleep(1)
        # Join all remaining threads
        for t in threads:
            t.join(60 * 30)
        return True

    def download_file(self, source_path, output_path):
        source_url = self.get_link(source_path)
        http_connector = HTTPConnector()
        return http_connector.download_file(source_url, output_path)

    def get_link(self, input_path, auth=True):
        input_path = re.sub(r'\\+', '/', input_path)
        input_path = re.sub(r'\/{2,}', '/', input_path)
        input_path = re.sub(r'^\/', '', input_path)
        input_path = re.sub(r'$\/', '', input_path)
        # get bucket
        parts = input_path.split('/')
        bucket = self.get_or_create_bucket(parts[0])
        # get key
        key_name = '/'.join(parts[1:])
        key = bucket.get_key(key_name)
        if key:
            return key.generate_url(60*60*24, query_auth=auth)
        return None

    def list_files(self, input_path, extensions=None):
        output_path = re.sub(r'\\+', '/', input_path)
        output_path = re.sub(r'\/{2,}', '/', output_path)
        output_path = re.sub(r'^\/', '', output_path)
        output_path = re.sub(r'$\/', '', output_path)
        output_base_dir = os.path.dirname(output_path)
        output_filename = os.path.basename(output_path)
        # get bucket
        parts = output_base_dir.split('/')
        bucket_name = parts[0] if parts[0] else output_filename         # fix for folder path does not end with '/'
        bucket = self.get_or_create_bucket(bucket_name)
        # get key
        prefix = os.path.join(output_base_dir, output_filename).replace(bucket_name, '')
        prefix = re.sub(r'^\/', '', prefix)
        result = list()
        for key in bucket.list(prefix):
            file_name, file_ext = os.path.splitext(key.name)
            fullname = os.path.join('/', bucket_name, key.name)
            if extensions:
                if file_ext in extensions:
                    result.append({"url": fullname, "size": key.size})
            else:
                result.append({"url": fullname, "size": key.size})
        return result

    def delete_by_prefix(self, prefix_path, nthreads=256):
        prefix_path = re.sub(r'\\+', '/', prefix_path)
        prefix_path = re.sub(r'\/{2,}', '/', prefix_path)
        prefix_path = re.sub(r'^\/', '', prefix_path)
        prefix_path = re.sub(r'\/$', '', prefix_path)
        # get bucket
        parts = prefix_path.split('/')
        prefix_key_name = '/'.join(parts[1:]) if len(parts) > 1 else ''
        threads = list()
        bucket = self.connector.get_bucket(parts[0])
        for key in bucket.list(prefix=prefix_key_name):
            t = threading.Thread(target=key.delete)
            t.start()
            threads.append(t)
            # Limit threads
            while len(threads) >= nthreads:
                for _idx, _value in enumerate(threads):
                    if not _value.isAlive():
                        threads.pop(_idx)
                time.sleep(0.001)
        # Join all remaining threads
        for t in threads:
            t.join(60)
        return True

    def is_folder(self, folder_path):
        # Dummy API
        return True

    def remove_file(self, remote_path):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: bool
        :return: The result
        """
        self.delete_by_prefix(remote_path, 2)
        return False

    def remove_folder(self, remote_path):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: bool
        :return: The result
        """
        self.delete_by_prefix(remote_path, 32)
        return False
