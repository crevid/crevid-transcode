_author__ = 'duybq'

import re
import os
import shutil
import urllib.parse
import tempfile
from urllib.parse import urlparse

from api_core.contrib import utils

from .HTTPAdapter import HTTPConnector
from .FTPAdapter import FTPConnector
from .FileAdapter import FileSystem
from .S3Adapter import S3Connector


class StorageAdapter:
    resolver = None
    connector = None

    def __init__(self, **kwargs):
        pass

    @staticmethod
    def get_connector_by_uri(uri, **kwargs):
        resolver = urlparse(uri)
        connector = None
        if str(resolver.scheme).lower().startswith('http'):
            connector = HTTPConnector()
        elif str(resolver.scheme).lower() == 'ftp':
            username = urllib.parse.unquote(resolver.username) if resolver.username else resolver.username
            password = urllib.parse.unquote(resolver.password) if resolver.password else resolver.password
            connector = FTPConnector(host=resolver.hostname, user=username, password=password, **kwargs)
        elif str(resolver.scheme).lower() == 'file':
            connector = FileSystem
        elif str(resolver.scheme).lower() == 's3':
            username = urllib.parse.unquote(resolver.username) if resolver.username else resolver.username
            password = urllib.parse.unquote(resolver.password) if resolver.password else resolver.password
            connector = S3Connector(host=resolver.hostname, user=username, password=password, **kwargs)
        return connector

    @staticmethod
    def get_mediainfo(uri):
        # Only support HTTP(S), FTP
        return utils.get_mediainfo(uri)

    @staticmethod
    def get_ffprobe(uri):
        # Only support HTTP, HTTPS. FILE
        return utils.probe_video_file(uri)

    @staticmethod
    def copy_file(src_path, dst_path):
        src_resolver = urlparse(src_path)
        dst_resolver = urlparse(dst_path)
        if str(dst_resolver.scheme).lower().startswith('file'):
            _temp = str(src_resolver.scheme).lower()
            if _temp.startswith('file'):
                FileSystem.copy_file(src_resolver.path, dst_resolver.path)
            elif _temp.startswith('ftp') or _temp.startswith('http') or _temp.startswith('s3'):
                connector = StorageAdapter.get_connector_by_uri(src_path)
                connector.download_file(src_path if _temp.startswith('http') else src_resolver.path,
                                        dst_resolver.path)
            else:
                raise Exception("Not support! %s -> %s" % (src_path, dst_path))

        elif not str(dst_resolver.scheme).lower().startswith('http'):
            temp_dir = os.path.join(tempfile.gettempdir(), 'transcode', 'temp-download-' + utils.generate_random_string())
            try:
                src_connector = StorageAdapter.get_connector_by_uri(src_path)
                if str(src_resolver.scheme).lower().startswith('file'):
                    dst_connector = StorageAdapter.get_connector_by_uri(dst_path)
                    dst_connector.upload_file(src_resolver.path, dst_resolver.path)
                elif str(src_resolver.scheme).lower().startswith('ftp') \
                        or str(src_resolver.scheme).lower().startswith('s3'):
                    # Download to temp dir
                    if not os.path.isdir(temp_dir):
                        os.makedirs(temp_dir)

                    _temp = os.path.join(temp_dir, os.path.basename(src_path))
                    dst_connector = StorageAdapter.get_connector_by_uri(dst_path)
                    src_connector.download_file(src_resolver.path, _temp)
                    dst_connector.upload_file(_temp, dst_resolver.path)
                else:
                    raise Exception("Not support! %s -> %s" % (src_path, dst_path))
            finally:
                if os.path.isdir(temp_dir):
                    shutil.rmtree(temp_dir)
        else:
            raise Exception("Not support! %s -> %s" % (src_path, dst_path))

    @staticmethod
    def remove_it(f_path):
        connector = StorageAdapter.get_connector_by_uri(f_path)
        resolver = urlparse(f_path)
        if connector.is_folder(resolver.path):
            connector.remove_folder(resolver.path)
            return True
        elif connector.is_file(resolver.path):
            connector.remove_file(resolver.path)
            return True
        return False