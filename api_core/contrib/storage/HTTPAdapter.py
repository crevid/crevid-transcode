import requests
from requests.auth import HTTPBasicAuth
from requests.adapters import HTTPAdapter

__author__ = 'duybq'


class HTTPConnector:
    instance = None
    user = None
    password = None

    def download_file(self, remote_location, output_location=None, timeout=30, max_retries=5):
        session = requests.Session()
        session.mount('http', HTTPAdapter(max_retries=max_retries))
        session.mount('https', HTTPAdapter(max_retries=max_retries))
        content = None
        if output_location:
            if self.user:
                r = session.get(remote_location, auth=HTTPBasicAuth(self.user, self.password), timeout=timeout)
            else:
                r = session.get(remote_location, stream=True, timeout=timeout)
            with open(output_location, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024 * 1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                        f.flush()
            content = True
        else:
            if self.user:
                r = session.get(remote_location, auth=HTTPBasicAuth(self.user, self.password), timeout=timeout)
            else:
                r = session.get(remote_location, stream=True, timeout=timeout)
            content = r.content
        session.close()
        return content
