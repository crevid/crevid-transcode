import os
import time
import ftplib
import ftputil
import ftputil.error
import ftputil.session
import traceback

__author__ = 'duybq'


class FTPConnector:
    ftp_instance = None
    host = None
    user = None
    password = None

    def __init__(self, host, user, password, ftp_port=21, debug_level=0):
        session_factory = ftputil.session.session_factory(base_class=ftplib.FTP, port=ftp_port,
                                                          encrypt_data_channel=False, debug_level=debug_level)
        self.host = host
        self.user = user
        self.password = password
        try:
            self.ftp_instance = ftputil.FTPHost(host, user, password, session_factory=session_factory)
        except Exception:
            self.ftp_instance = ftputil.FTPHost(host, user, password, session_factory=session_factory)

    def __del__(self):
        self.close()

    def close(self):
        try:
            if self.ftp_instance:
                self.ftp_instance.close()
            self.ftp_instance = None
        except Exception:
            traceback.print_exc()

    def get_instance(self):
        """
        :rtype: :class:`ftputil.FTPHost`
        :return: FTPHost instance
        """
        try:
            self.ftp_instance.keep_alive()
        except Exception:
            self.__init__(self.host, self.user, self.password)
        return self.ftp_instance

    def create_folder(self, folder_path):
        """
        :type folder_path: str`
        :param folder_path: The folder path
        :rtype: bool
        :return: The result
        """
        if self.get_instance().path.isfile(folder_path):
            self.get_instance().remove(folder_path)
        if not self.get_instance().path.isdir(folder_path):
            try:
                self.get_instance().makedirs(folder_path, 0o777)
            except:
                self.get_instance().makedirs(folder_path)
        return self.get_instance().path.isdir(folder_path)

    def upload_file(self, local_path, remote_path, return_file_path=True):
        """
        :type local_path: str
        :param local_path: The local path
        :type remote_path: str
        :param remote_path: The remote path
        :type return_file_path: bool
        :param return_file_path: return file path or not
        :rtype: bool
        :return: The result
        """
        local_path = str(local_path)
        remote_path = str(remote_path)
        # check if file does not exist
        if not os.path.isfile(local_path):
            raise Exception('File does not exist: ' + local_path)
        # create dest folder
        retries = 3
        while retries > 0:
            try:
                retries -= 1
                self.create_folder(self.get_instance().path.dirname(remote_path))
                break
            except ftputil.error.TemporaryError:
                time.sleep(120)
                if retries <= 0:
                    traceback.print_exc()
        # copy file to remote storage
        retries = 3
        while retries > 0:
            try:
                retries -= 1
                self.get_instance().upload(local_path, remote_path)
                break
            except ftputil.error.TemporaryError:
                time.sleep(120)
                if retries <= 0:
                    raise

        # try to change mode
        try:
            self.get_instance().chmod(remote_path, 0o777)
        except Exception:
            traceback.print_exc()
        return remote_path if return_file_path else self.get_instance().path.isfile(remote_path)

    def upload_folder(self, source_path, output_path, return_file_path=False):
        """
        :type source_path: str
        :param source_path: The local path
        :type output_path: str
        :param output_path: The remote path
        :type return_file_path: bool
        :param return_file_path: return file path or not
        :rtype: bool
        :return: The result
        """
        local_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(source_path) for f in filenames]
        # upload file
        for file_path in local_files:
            source_file = file_path
            dest_file = os.path.join(output_path, file_path.replace(source_path, output_path))
            if os.path.isfile(source_file):
                self.upload_file(source_file, dest_file)
        return output_path if return_file_path else self.get_instance().path.isdir(output_path)

    def download_file(self, remote_path, local_path, return_file_path=True):
        """
        :type local_path: str
        :param local_path: The local path
        :type remote_path: str
        :param remote_path: The remote path
        :type return_file_path: bool
        :param return_file_path: return file path or not
        :rtype: bool
        :return: The result
        """
        remote_path = str(remote_path)
        local_path = str(local_path)
        # check if file does not exist
        if not self.get_instance().path.isfile(remote_path):
            raise Exception('File does not exist: ' + remote_path)
        # copy file to local storage
        retries = 3
        local_file_size = os.path.getsize(local_path) if os.path.exists(local_path) else 0
        remote_file_size = self.get_instance().path.getsize(remote_path)
        while retries > 0 and local_file_size != remote_file_size:
            retries -= 1
            self.get_instance().download(remote_path, local_path)
            local_file_size = os.path.getsize(local_path) if os.path.exists(local_path) else 0
        return local_path if return_file_path else os.path.isfile(local_path)

    def is_file(self, file_path):
        return self.get_instance().path.isfile(file_path)

    def is_folder(self, folder_path):
        return self.get_instance().path.isdir(folder_path)

    def remove_file(self, remote_path):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: bool
        :return: The result
        """
        if self.get_instance().path.isfile(remote_path):
            self.get_instance().remove(remote_path)
            return not self.get_instance().path.isfile(remote_path)
        return False

    def remove_folder(self, remote_path):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: bool
        :return: The result
        """
        if self.get_instance().path.isdir(remote_path):
            self.get_instance().rmtree(remote_path)
            return not self.get_instance().path.isdir(remote_path)
        return False

    def list_files(self, remote_path, extensions=None):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: list
        :return: The result
        """
        # single file
        if self.get_instance().path.isfile(remote_path):
            if extensions and self.get_instance().path.splitext(remote_path)[-1] in extensions:
                return [{"url": remote_path, "size": self.get_instance().path.getsize(remote_path)}]
        # folder
        result = list()
        count = 0
        recursive = self.get_instance().walk(remote_path, topdown=True, onerror=None)
        for root, dirs, files in recursive:
            for name in files:
                if extensions:
                    if str(self.get_instance().path.splitext(name)[-1]).lower() in extensions:
                        full_path = self.get_instance().path.join(root, name)
                        file_size = self.get_instance().path.getsize(full_path)
                        result.append({"url": full_path, "size": file_size})
                else:
                    full_path = self.get_instance().path.join(root, name)
                    file_size = self.get_instance().path.getsize(full_path)
                    result.append({"url": full_path, "size": file_size})
                count += 1
                # only support scan <= 1000000 files
                if count > 1000000:
                    return result
        return result

    def list_files_1(self, remote_path, exts):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: list
        :return: The result
        """
        result = list()
        recursive = self.get_instance().walk(remote_path, topdown=True, onerror=None, followlinks=True)
        file_names = []
        for (sourceDir, dirname, filename) in recursive:
            file_names.extend(filename)
            break

        for filename in file_names:
            source_file = self.get_instance().path.join(remote_path, filename)
            if str(self.get_instance().path.splitext(source_file)[-1]).lower() in exts or True:
                file_size = self.get_instance().path.getsize(source_file)
                result.append({"url": source_file, "size": file_size})
        return result

    def is_file_exists(self, remote_path):
        """
        :type remote_path: str
        :param remote_path: The remote path
        :rtype: bool
        :return: The result
        """
        return self.get_instance().path.isfile(remote_path)
