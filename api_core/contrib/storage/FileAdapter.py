import os
import errno
import shutil

__author__ = 'duybq'


class FileSystem:

    @classmethod
    def copy_file(cls, src_file_path, dst_file_path):
        if not os.path.isfile(src_file_path):
            return False
        dst_dir = os.path.dirname(dst_file_path)
        if not os.path.isdir(dst_file_path):
            os.makedirs(dst_dir)
        shutil.copyfile(src_file_path, dst_file_path)
        return True

    @classmethod
    def move_file(cls, src_file_path, dst_file_path):
        if not os.path.isfile(src_file_path):
            return False
        dst_dir = os.path.dirname(dst_file_path)
        if not os.path.isdir(dst_file_path):
            os.makedirs(dst_dir)
        shutil.move(src_file_path, dst_file_path)
        return True

    @classmethod
    def is_file(cls, file_path):
        return os.path.isfile(file_path)

    @classmethod
    def is_folder(cls, folder_path):
        return os.path.isdir(folder_path)

    @classmethod
    def remove_file(cls, file_path):
        if os.path.isfile(file_path):
            os.remove(file_path)
        return True

    @classmethod
    def copy_folder(cls, src_folder_path, dst_folder_path):
        if not os.path.isdir(src_folder_path):
            return False
        if not os.path.isdir(dst_folder_path):
            os.makedirs(dst_folder_path)
        try:
            shutil.copytree(src_folder_path, dst_folder_path)
        except OSError as exc:  # python >2.5
            if exc.errno == errno.ENOTDIR:
                shutil.copy(src_folder_path, dst_folder_path)
            else:
                raise
        return True

    @classmethod
    def move_folder(cls, src_folder_path, dst_folder_path):
        if not os.path.isdir(dst_folder_path):
            return False
        if os.path.isdir(dst_folder_path):
            shutil.rmtree(dst_folder_path)
        shutil.move(src_folder_path, dst_folder_path)
        return True

    @classmethod
    def remove_folder(cls, folder_path):
        if os.path.isdir(folder_path):
            shutil.rmtree(folder_path)
        return True
