try:
    import ujson as json
except ImportError:
    import json
try:
    import zipfile
except ImportError:
    zipfile = None
import re
import os
import sys
import time
import shutil
import random
import string
import xmltodict
import subprocess
import traceback
from io import StringIO
from urllib.parse import urlparse
from ipware.ip import get_real_ip, get_ip
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.cache import caches

__author__ = 'duybq'


def get_client_ip(request):
    """
    This function will return the real ip address of client
    :param request: This is the request context
    :type request: django.core.handlers.wsgi.WSGIRequest|rest_framework.request.Request
    """
    ip = get_real_ip(request)
    return ip if ip else get_ip(request)


def validate_sha1_string(text):
    """
    This function will verify SHA1 string
    :param text: A sample string
    :type text: str
    """
    return re.match(r'^[0-9a-f]{40}$', str(text).lower()) and True


def generate_random_string(length=4, seed=None):
    if seed:
        random.seed(seed)
    else:
        time.sleep(0.000001)
        random.seed(None)
    return ''.join(random.choice(string.digits + string.ascii_letters) for i in range(length))


def validate_email_string(text):
    """
    This function will verify SHA1 string
    :param text: A sample string
    :type text: str
    """
    text = str(text)
    if len(text) > 254:
        return False
    try:
        validate_email(text)
        return True
    except ValidationError:
        return False


def list_files_with_ext(path, exts=('.mp4',)):
    result = list()
    if os.path.exists(path):
        if os.path.isdir(path):
            # Case: path points dir
            for root, dirs, files in os.walk(path):
                for item in files:
                    # Check extensions
                    filename, file_ext = os.path.splitext(item)
                    if file_ext.lower() in exts:
                        # Check folders or files
                        file_location = os.path.join(root, item)
                        if os.path.isfile(file_location):
                            result.append(file_location)
        else:
            # Case: path points to file
            result = [path]
    return result


def run_command(cmnd, async=False, timeout=None):
    p = subprocess.Popen(cmnd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1024*1024)
    if not async:
        if timeout:
            poll_seconds = .250
            deadline = time.time() + min(timeout, 60*60*2)
            while time.time() < deadline and p.poll() is None:
                time.sleep(poll_seconds)
            if not p.poll():
                try:
                    p.terminate()
                except Exception:
                    pass
        info, error = p.communicate()
        if error:
            if not timeout:
                raise Exception('Error: ' + error + ' ---- ' + str(cmnd))
            else:
                return 'Time-out'
        if not info:
            print('Command print EMPTY to console: ' + str(cmnd))
        return info
    return p


def run_command_with_progress(cmnd, percentage_init, percentage_factor, callback_func, callback_params, timeout=None):

    def get_duration_in_seconds(buffer_line):
        # str_time ~ hh:mm:ss.xx
        search = re.search(r'Duration: (\d{2}):(\d{2}):(\d{2})', buffer_line)
        if search:
            return int(search.group(1)) * 60 * 60 + int(search.group(2)) * 60 + int(search.group(3))
        return None

    def get_ffmpeg_progress(buffer_line):
        # Case: Video
        search = re.search(r'frame=\s*([\.\d]+).*size=\s*(\w+)\s+time=(\d{2}):(\d{2}):(\d{2})\.(\d{2})'
                           r'\s+bitrate=\s*(.+)\s+speed=\s*([\.\de\+\-]+)x', buffer_line, re.IGNORECASE)
        if search:
            return {
                'encoded_frame': search.group(1),
                'output_size': search.group(2),
                'encoded_seconds': int(search.group(3)) * 60 * 60 + int(search.group(4)) * 60 + int(search.group(5)),
                'encoding_bitrate': search.group(7),
                'xpeed': search.group(8),
            }
        # Case: Audio
        search = re.match(r'size=\s*(\w+)\s+time=(\d{2}):(\d{2}):(\d{2})\.(\d{2})\s+bitrate=\s*(.+)'
                          r'\s+speed=\s*([\.\de\+\-]+)x', buffer_line, re.IGNORECASE)
        if search:
            return {
                'encoded_frame': None,
                'output_size': search.group(1),
                'encoded_seconds': int(search.group(2)) * 60 * 60 + int(search.group(3)) * 60 + int(search.group(4)),
                'encoding_bitrate': search.group(6),
                'xpeed': search.group(7),
            }
        # Case: Splitting
        search = re.match(r'frame=\s*(\d+)\s+.*Lsize=\s*(\w+)\s+time=(\d{2}):(\d{2}):(\d{2})\.(\d{2})\s+bitrate=\s*(.+)'
                          r'\s+speed=\s*([\.\de\+\-]+)x', buffer_line, re.IGNORECASE)
        if search:
            return {
                'encoded_frame': search.group(1),
                'output_size': search.group(2),
                'encoded_seconds': int(search.group(3)) * 60 * 60 + int(search.group(4)) * 60 + int(search.group(5)),
                'encoding_bitrate': search.group(7),
                'xpeed': search.group(8),
            }
        return None

    deadline = time.time() + min(timeout if timeout else 60 * 5, 60 * 60 * 2)
    p = subprocess.Popen(cmnd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                         bufsize=1024*1024)
    duration = timestamp = 0
    while True:
        current_line = str(p.stdout.readline(1024*1024)).strip()
        # Case: process ends
        if current_line == '' and p.poll() is not None:
            break
        # Case: timeout
        if time.time() > deadline:
            break
        # Case: get Duration
        if not duration:
            print(current_line)
            duration = get_duration_in_seconds(buffer_line=current_line)
        else:
            # Case: FFMPEG progress line
            metadata = get_ffmpeg_progress(current_line)
            if metadata:
                current_time = int(time.time())
                _temp = float(metadata['encoded_seconds'])/float(duration)
                if ((_temp >= 0.95) and (current_time - timestamp > 10)) or (current_time - timestamp > 30):
                    timestamp = current_time
                    percentage = percentage_init + percentage_factor * _temp * 100
                    callback_params['extras'] = metadata
                    callback_params.update({'percentage': round(percentage, 2), 'message': current_line})
                    callback_func(**callback_params)
            else:
                # Only print not progress info
                print(current_line)
    return p.returncode


def get_mediainfo(filename):
    parser = urlparse(filename)
    if str(parser.scheme).lower().find('udp') > -1:
        return None
    cmnd = ['mediainfo', '--Output=XML', filename]
    try:
        info = run_command(cmnd=cmnd, timeout=60*2)
        return xmltodict.parse(info)['Mediainfo']
    except Exception as e:
        print("Error: " + str(e.args), str(cmnd))
        return None


def probe_video_file(filename):
    cmnd = ['ffprobe', '-show_streams', '-show_format', '-loglevel', 'quiet',
            '-print_format', 'json', filename]
    try:
        info = run_command(cmnd=cmnd, timeout=60*2)
        return json.loads(info)
    except Exception as e:
        print("Error: " + str(e.args), str(cmnd))
        return None


def disable_key_prefix(key, key_prefix, version):
    return key


def disable_key_version(key, key_prefix, version):
    return key_prefix + key


def dictfetchall(cursor):
    """
    Return all rows from a cursor as a dict
    """
    columns = [col[0] for col in cursor.description]
    data = cursor.fetchall()
    return [dict(zip(columns, row)) for row in data] if data else None


def dictfetchone(cursor):
    """
    Return 1 row from a cursor as a dict
    """
    columns = [col[0] for col in cursor.description]
    data = cursor.fetchone()
    return dict(zip(columns, data)) if data else None


def unzip_file(input_path, output_path):
    count = 0
    with open(input_path, 'rb') as file_handler:
        z = zipfile.ZipFile(file_handler, allowZip64=True)
        for name in z.namelist():
            z.extract(name, output_path)
            count += 1
    return count


def make_archive(base_name, format, root_dir=None, base_dir=None, verbose=0,
                 dry_run=0, owner=None, group=None, logger=None):

    save_cwd = os.getcwd()
    if root_dir is not None:
        if logger is not None:
            logger.debug("changing into '%s'", root_dir)
        base_name = os.path.abspath(base_name)
        if not dry_run:
            os.chdir(root_dir)

    if base_dir is None:
        base_dir = os.curdir

    kwargs = {'dry_run': dry_run, 'logger': logger}

    try:
        format_info = shutil._ARCHIVE_FORMATS[format]
    except KeyError:
        raise ValueError("unknown archive format '%s'" % format)

    func = format_info[0]
    for arg, val in format_info[1]:
        kwargs[arg] = val

    if format != 'zip':
        kwargs['owner'] = owner
        kwargs['group'] = group

    filename = None
    try:
        # For file ZIP
        if format == 'zip':
            zip_filename = base_name + ".zip"
            archive_dir = os.path.dirname(base_name)

            if not os.path.exists(archive_dir):
                if logger is not None:
                    logger.info("creating %s", archive_dir)
                if not dry_run:
                    os.makedirs(archive_dir)

            # If zipfile module is not available, try spawning an external 'zip'
            # command.
            if zipfile is None:
                shutil._call_external_zip(base_dir, zip_filename, verbose, dry_run)
            else:
                if logger is not None:
                    logger.info("creating '%s' and adding '%s' to it",
                                zip_filename, base_dir)
                if not dry_run:
                    with zipfile.ZipFile(zip_filename, "w", allowZip64=True, compression=zipfile.ZIP_STORED) as zf:
                        for dirpath, dirnames, filenames in os.walk(base_dir):
                            for name in filenames:
                                path = os.path.normpath(os.path.join(dirpath, name))
                                if os.path.isfile(path):
                                    zf.write(path, path)
                                    if logger is not None:
                                        logger.info("adding '%s'", path)
        else:
            # For tar, bztar, gztar
            filename = func(base_name, base_dir, **kwargs)
    finally:
        if root_dir is not None:
            if logger is not None:
                logger.debug("changing back to '%s'", save_cwd)
            os.chdir(save_cwd)
    return filename


def update_progress(**kwargs):
    from Controller.tasks.driven import drive_events

    self_task = kwargs.pop('self_task', None)
    if not self_task:
        print('self_task is null!')
    else:
        status = kwargs.get('state', None)
        trace_info = kwargs.get('traceback', None)
        if not trace_info and hasattr(sys, 'last_type'):
            result = StringIO()
            traceback.print_last(file=result)
            trace_info = result.getvalue()
            result.close()
        if not status:
            status = kwargs.get('status', 'PROGRESSING'),
        data = {
            'task_id': self_task.request.id,
            'result': {
                'error': 0,
                'task_info': {
                    'group': self_task.request.group,
                    'eta': self_task.request.eta,
                    'expires': self_task.request.expires,
                    'hostname': self_task.request.hostname,
                    'callbacks': self_task.request.callbacks,
                    'root_id': self_task.request.root_id,
                    'parent_id': self_task.request.parent_id,
                    'chain': self_task.request.chain,
                },
                'percentage': 1,
                'message': 'Unknown',
                'workflow': None,
                'file_info': None,
                'file_id': None,
            },
            'state': status,
            'status': status,
            'traceback': trace_info,
        }
        data['result'].update(kwargs)
        drive_events.apply_async(args=[data, ])
        # self_task.update_state(state=kwargs.get('state', 'PROGRESSING'), meta=data)
    return None


class AdvancedRedisCache(object):

    """
    :type connection: redis.client.StrictRedis
    """
    connection = None
    prefix = None
    django_cache = None

    def __init__(self, alias='default'):
        """
        Initial method
        :param alias: the alias of redis cache
        """
        self.django_cache = caches[alias]
        if not hasattr(self.django_cache.client, 'get_client'):
            raise NotImplementedError("This backend does not supports this feature")

        self.connection = self.django_cache.client.get_client(True)
        self.prefix = self.django_cache.key_prefix

    def get_key(self, key):
        return self.prefix + key

    def set(self, key, value, expire):
        """
        Set simple key to redis
        :param key: The key of cache
        :type key: str
        :param value: The value of cache
        :type value: mixed
        :param expire: Expired time
        :type expire: int
        """
        return self.django_cache.set(key, value, expire)

    def get(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.django_cache.get(key)

    def delete_pattern(self, key):
        """
        Get simple key from redis and return the value
        :param key: Key of object
        :type key: str
        """
        return self.django_cache.delete_pattern(key)

    def hset(self, key, field, value, expire):
        """
        Multiple set hash object via hmset by pipeline transaction
        :param key: Key of object
        :type key: str
        :param field: String field
        :type field: str
        :param value: String field
        :type value: str
        :param expire: Expired time
        :type expire: int
        :return: list
        """
        key = self.get_key(key)
        pipe = self.connection.pipeline()
        pipe.hset(key, field, value)
        if expire == 0:
            pipe.persist(key)
        else:
            pipe.expire(key, expire)
        return pipe.execute()

    def hget(self, key, field):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hget(key, field)

    def hmset(self, key, mapping, expire):
        """
        Multiple set hash object via hmset by pipeline transaction
        :param key: Key of object
        :type key: str
        :param mapping: Hash object
        :type mapping: dict
        :param expire: Expired time
        :type expire: int
        :return: list
        """
        key = self.get_key(key)
        pipe = self.connection.pipeline()
        pipe.hmset(key, mapping)
        if expire == 0:
            pipe.persist(key)
        else:
            pipe.expire(key, expire)
        return pipe.execute()

    def hmget(self, key, field):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hmget(key, field)

    def hsetnx(self, key, field, value):
        """
        Get value from key with hash field name
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        """
        key = self.get_key(key)
        return self.connection.hsetnx(key, field, value)

    def hgetall(self, key):
        """
        Multiple set hash object via hmset by pipeline transaction
        :param key: Key of object
        :type key: str
        """
        key = self.get_key(key)
        return self.connection.hgetall(key)

    def exists(self, key):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :return: boolean
        """
        return self.django_cache.exists(key)

    def hexists(self, key, field):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :param field: Field name in hash object
        :type field: str
        :return: boolean
        """
        key = self.get_key(key)
        return self.connection.hexists(key, field)

    def expire(self, key, seconds):
        """
        Check if key exists
        :param key: Key of object
        :type key: str
        :param seconds: Number of seconds this cache is expired
        :type seconds: int
        :return: boolean
        """
        key = self.get_key(key)
        return self.connection.expire(key, seconds)

    def ttl(self, key):
        """
        :type key: str
        :return: int
        """
        key = self.get_key(key)
        return self.connection.ttl(key)

    def flushdb(self):
        """
        Flush all key in database
        """
        return self.connection.flushdb()
