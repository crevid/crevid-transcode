from Crypto.Hash import MD5, SHA, SHA256, SHA512, HMAC

__author__ = 'duybq'


class Hasher(object):

    @classmethod
    def md5(cls, text):
        """
        MD5 Hash
        :param text: Text content
        :type text: str|unicode
        :return: str
        """
        if hasattr(text, 'encode'):
            text = text.encode('utf-8')
        return MD5.new(text).hexdigest()

    @classmethod
    def sha1(cls, text):
        """
        SHA1 Hash
        :param text: Text content
        :type text: str|unicode
        :return: str
        """
        if hasattr(text, 'encode'):
            text = text.encode('utf-8')
        return SHA.new(text).hexdigest()

    @classmethod
    def hmac(cls, text):
        """
        HMAC Hash
        :param text: Text content
        :type text: str|unicode
        :return: str
        """
        if hasattr(text, 'encode'):
            text = text.encode('utf-8')
        return HMAC.new(text).hexdigest()

    @classmethod
    def sha256(cls, text):
        """
        SHA256 Hash
        :param text: Text content
        :type text: str|unicode
        :return: str
        """
        if hasattr(text, 'encode'):
            text = text.encode('utf-8')
        return SHA256.new(text).hexdigest()

    @classmethod
    def sha512(cls, text):
        """
        SHA512 Hash
        :param text: Text content
        :type text: str|unicode
        :return: str
        """
        if hasattr(text, 'encode'):
            text = text.encode('utf-8')
        return SHA512.new(text).hexdigest()
