import hashlib
from django import http
from django.conf import settings
from django.middleware.common import CommonMiddleware

__author__ = 'duybq'


class CustomCommonMiddleware(CommonMiddleware):

    def process_response(self, request, response):
        """
        Calculate the ETag, if needed.
        """
        if settings.USE_ETAGS:
            if response.has_header('ETag'):
                etag = response['ETag']
            elif response.streaming:
                etag = None
            else:
                etag = '"%s"' % hashlib.md5(response.content).hexdigest()
            if etag is not None:
                if (200 <= response.status_code < 300) and (request.META.get('HTTP_IF_NONE_MATCH') == etag):
                    cookies = response.cookies
                    response = http.HttpResponseNotModified()
                    response.cookies = cookies
                else:
                    response['ETag'] = etag

        if response.status_code == 200 and not response.has_header('Content-Length'):
            response['Content-Length'] = str(len(response.content))
        return response
