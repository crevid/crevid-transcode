from rest_framework import generics
from rest_framework.views import APIView

from . import exceptions
from .parsers import UJSONParser

__author__ = 'duybq'


class APIViewBase(APIView):
    parser_classes = (UJSONParser,)

    def permission_denied(self, request, message=None):
        """
        If request is not permitted, determine what kind of exception to raise.
        """
        if not request.successful_authenticator:
            raise exceptions.NotAuthenticated()
        raise exceptions.PermissionDenied()


class ListAPIViewBase(generics.ListAPIView):
    parser_classes = (UJSONParser,)

    def permission_denied(self, request, message=None):
        """
        If request is not permitted, determine what kind of exception to raise.
        """
        if not request.successful_authenticator:
            raise exceptions.NotAuthenticated()
        raise exceptions.PermissionDenied()


class APIVersion(object):

    VERSION_1_0 = "1.0"
