from rest_framework.pagination import CursorPagination

from ..define import CURSOR_PAGE_SIZE, CURSOR_ORDERING

__author__ = 'duybq'


class CursorResultsSetPagination(CursorPagination):
    page_size = CURSOR_PAGE_SIZE
    ordering = CURSOR_ORDERING
