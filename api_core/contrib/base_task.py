from __future__ import absolute_import

try:
    import ujson as json
except ImportError:
    import json
import time
import requests
import traceback
from celery import Task, states
from django.conf import settings

from api_core.contrib import utils
from Monitor.tasks import recv_request_action
from Monitor.models import Task as MonitorTask

__author__ = 'duybq'


def request_file_info(file_id):
    num_retries = 0
    headers = {
        'Accept': 'application/json; version=1.0',
        'Content-type': 'application/json; charset=utf-8'
    }
    request_data = {'file_id': file_id}
    while num_retries <= 10:
        try:
            response = requests.get(settings.API_FILE_INFO, params=request_data, headers=headers, timeout=30)
            resp_data = response.json()
            if str(resp_data['error']) == '0':
                resp_data = resp_data['data']
                file_data = resp_data['file_info']
                if not file_data['size'] or (file_data['size'] and isinstance(file_data['standard_metadata'], dict)):
                    return resp_data
                print('Waring: the standard_metadata value is not a dict: ' + str(file_data['standard_metadata']))
        except Exception as e:
            traceback.print_exc()
        num_retries += 1
        time.sleep(min(1 * num_retries, 60 * 30))
    return None


def update_data_info(api_url, request_data):
    num_retries = 0
    headers = {
        'Accept': 'application/json; version=1.0',
        'Content-type': 'application/json; charset=utf-8'
    }
    while num_retries <= 10:
        try:
            response = requests.post(api_url, data=json.dumps(request_data), headers=headers, timeout=30)
            resp_data = response.json()
            if str(resp_data['error']) == '0':
                return resp_data['data']
        except Exception as e:
            traceback.print_exc()
        num_retries += 1
        time.sleep(min(1 * num_retries, 60 * 30))
    return None


class BaseTask(Task):
    abstract = True

    logger = None

    def on_success(self, retval, task_id, args, kwargs):
        print('Finish task with SUCCESS!')
        # START OF: Update progress
        utils.update_progress(self_task=self, error=0, percentage=100,  status=states.SUCCESS, message='Done',
                              workflow=retval.get('workflow', None),
                              file_id=retval.get('file_id', None), file_info=retval.get('file_info', None))
        # END OF: Update progress

        #####################################################
        # BEGIN OF NOTIFYING TASK
        request_data = {
            'action': 'mapping_task_with_worker',
            'hostname': self.request.hostname,
            'task_id': self.request.id,
            'task_name': self.name,
            'status': MonitorTask.S_STOPPED,
        }
        recv_request_action.apply_async(args=[request_data['action'], request_data])
        # END OF NOTIFYING TASK
        #####################################################

        # call parent method
        super(BaseTask, self).on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print('Finish task with FAILURE!')

        #####################################################
        # BEGIN OF NOTIFYING TASK
        request_data = {
            'action': 'mapping_task_with_worker',
            'hostname': self.request.hostname,
            'task_id': self.request.id,
            'task_name': self.name,
            'status': MonitorTask.S_ERROR,
        }
        recv_request_action.apply_async(args=[request_data['action'], request_data])
        # END OF NOTIFYING TASK
        #####################################################

        # call parent method
        super(BaseTask, self).on_failure(exc, task_id, args, kwargs, einfo)


def get_value(data_set, key_name):
    if not isinstance(data_set, dict) or not isinstance(key_name, str):
        return None
    try:
        return data_set[key_name]
    except KeyError:
        pass
    return None
