try:
    import ujson as json
except ImportError:
    import json

__author__ = 'duybq'


def build_parent_playlist(medias, parent_output_m3u8):
    with open(parent_output_m3u8, 'w') as parent_handler:
        parent_handler.write('#EXTM3U\n')
        parent_handler.write('#EXT-X-VERSION:3\n')
        for item in sorted(medias, key=lambda temp: (int(temp['video_bandwidth']),)):
            video_width = item['video_width']
            video_height = item['video_width']
            bandwidth = item['video_bandwidth']
            if int(video_width) <= 1024:
                codecs = 'avc1.77.31,mp4a.40.2'
            else:
                codecs = 'avc1.100.41,mp4a.40.2'
            if 'video_resolution' in item and item['video_resolution']:
                resolution = item['video_resolution']
            else:
                resolution = str(video_width) + "x" + str(video_height)

            parent_handler.write('#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=%s,CODECS="%s",RESOLUTION=%s\n'
                                 % (bandwidth, codecs, resolution))
            parent_handler.write(str(item['playlist_path']) + "\n")
    return True