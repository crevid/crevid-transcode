from __future__ import absolute_import

import os
import shutil
import tempfile
import traceback
from celery import shared_task, states
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.custom_crypto import Hasher
from api_core.contrib.base_task import BaseTask, request_file_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def mux_to_mp4_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])
        file_data = information['file_info']
        profile_data = information['profile_data']

        # Scan for workflow information
        workflow_data = None
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Prepare output encode
        rel_output_dst_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
                                           utils.generate_random_string(8, str(workflow_data['id'])))
        phys_output_dst_path = os.path.join(settings.PERMANENTLY_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        uri_output_dst_path = os.path.join(settings.URL_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        temporary_dst_path = os.path.join(tempfile.gettempdir(), 'transcode',
                                          utils.generate_random_string(32, str(workflow_data['id'])))
        junk_paths.append(temporary_dst_path)
        if not os.path.isdir(temporary_dst_path):
            os.makedirs(temporary_dst_path)

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=5, message='Update temporary metadata',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Process for per profile
        temporary_mp4_paths = list()
        current_percentage = 5
        delta_percentage = round((70 - current_percentage) / (len(profile_data) * len(profile_data) * 2), 2)
        for mapper_key, mapper_data in profile_data.items():
            input_list = list()
            temporary_mp4_path = os.path.join(temporary_dst_path, "%s_%s.mp4" % (input_params['file_id'], mapper_key))
            # Firstly, scan for 1 stream video
            for _temp in workflow_data['metadata']['input']:
                if str(_temp['data_type']).lower() == 'video' and mapper_data['video_profile'] == _temp['profile_id']:
                    if not os.path.isfile(_temp['phys_output_dst_path']):
                        _temp_name = Hasher.md5(_temp['phys_output_dst_path'])
                        _temp_name += os.path.split(_temp['phys_output_dst_path'])[-1]
                        _temp['phys_output_dst_path'] = os.path.join(temporary_dst_path, _temp_name)
                        if not os.path.isfile(_temp['phys_output_dst_path']):
                            StorageAdapter.copy_file(_temp['uri_output_dst_path'],
                                                     'file://' + _temp['phys_output_dst_path'])
                            junk_paths.append(_temp['phys_output_dst_path'])
                    input_list += ['-add', _temp['phys_output_dst_path']]
                    break

            # Secondly, scan for audio
            for _temp in workflow_data['metadata']['input']:
                if str(_temp['data_type']).lower() == 'audio' and mapper_data['audio_profile'] == _temp['profile_id']:
                    if not os.path.isfile(_temp['phys_output_dst_path']):
                        _temp_name = Hasher.md5(_temp['phys_output_dst_path'])
                        _temp_name += os.path.split(_temp['phys_output_dst_path'])[-1]
                        _temp['phys_output_dst_path'] = os.path.join(temporary_dst_path, _temp_name)
                        if not os.path.isfile(_temp['phys_output_dst_path']):
                            StorageAdapter.copy_file(_temp['uri_output_dst_path'],
                                                     'file://' + _temp['phys_output_dst_path'])
                            junk_paths.append(_temp['phys_output_dst_path'])
                    input_list += ['-add', _temp['phys_output_dst_path']]

            # Build command-line
            cmnd = ['MP4Box', '-flat', '-force-cat']
            cmnd += input_list
            cmnd += ['-new', temporary_mp4_path]
            print('######')
            print(' '.join(cmnd))

            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Mux file with mapper_key=' + str(mapper_key),
                                  extras={'cmnd': ' '.join(cmnd)}, workflow=workflow_data,
                                  file_id=input_params['file_id'], file_info=None)
            # END OF: Update progress
            #
            utils.run_command(cmnd)
            #
            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Mux file done with mapper_key=' + str(mapper_key),
                                  extras={'cmnd': ' '.join(cmnd)}, workflow=workflow_data,
                                  file_id=input_params['file_id'], file_info=None)
            # END OF: Update progress
            current_percentage += delta_percentage
            temporary_mp4_paths.append(temporary_mp4_path)

        # Upload MP4 files
        delta_percentage = round((90 - current_percentage) / (len(temporary_mp4_paths) * len(temporary_mp4_paths) * 2), 2)
        workflow_data['metadata']['output'] = list()
        for _path in temporary_mp4_paths:
            _temp = os.path.basename(_path)
            _temp_phys_output_dst_path = os.path.join(phys_output_dst_path, _temp)
            _temp_uri_output_dst_path = os.path.join(uri_output_dst_path, _temp)

            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Begin to upload file ' + str(_temp_phys_output_dst_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress
            #
            StorageAdapter.copy_file('file://' + _path, _temp_uri_output_dst_path)
            #
            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Complete to upload file ' + str(_temp_phys_output_dst_path),
                                  file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
            # END OF: Update progress

            workflow_data['metadata']['output'].append({
                'phys_output_dst_path': _temp_phys_output_dst_path,
                'uri_output_dst_path': _temp_uri_output_dst_path,
            })

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95,
                              message='Complete to upload output files',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=None)
        # END OF: Update progress
        # Update metadata
        workflow_data['status'] = Workflow.S_SUCCESS
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if os.path.isfile(_temp):
                    os.remove(_temp)
                elif os.path.isdir(_temp):
                    shutil.rmtree(_temp)
