from __future__ import absolute_import

import os
import shutil
import tempfile
import traceback
from celery import shared_task, states
from django.conf import settings

from api_core.contrib import utils
from api_core.contrib.custom_crypto import Hasher
from api_core.contrib.base_task import BaseTask, request_file_info, update_data_info
from api_core.contrib.storage import StorageAdapter

from Controller.models import Workflow
from Monitor.tasks import recv_request_action

from ..contrib import hls_playlist

__author__ = 'duybq'


@shared_task(bind=True, base=BaseTask)
def mux_to_hls_stage(self, input_params):
    error = 0
    workflow_data = file_data = None
    junk_paths = list()
    #####################################################
    # BEGIN OF NOTIFYING TASK
    request_data = {
        'action': 'mapping_task_with_worker',
        'hostname': self.request.hostname,
        'task_id': self.request.id,
        'task_name': self.name,
    }
    recv_request_action.apply_async(args=[request_data['action'], request_data])
    # END OF NOTIFYING TASK
    #####################################################
    try:
        # Request File information
        print('Query file / workflow information from server')
        information = request_file_info(input_params['file_id'])
        file_data = information['file_info']
        profile_data = information['profile_data']
        video_profile_data = information['video_profile_data']

        # Scan for workflow information
        workflow_data = None
        for _t_data in information['workflow']:
            if str(_t_data['id']) == str(input_params['workflow']):
                workflow_data = _t_data
                break

        # Prepare output encode
        rel_output_dst_path = os.path.join(utils.generate_random_string(1, str(file_data['id'])),
                                           utils.generate_random_string(8, str(workflow_data['id'])))
        phys_output_dst_path = os.path.join(settings.PERMANENTLY_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        uri_output_dst_path = os.path.join(settings.URL_OUTPUT_STORAGE_PREFIX, rel_output_dst_path)
        temporary_dst_path = os.path.join(tempfile.gettempdir(), 'transcode',
                                          utils.generate_random_string(32, str(workflow_data['id'])))
        junk_paths.append(temporary_dst_path)
        if not os.path.isdir(temporary_dst_path):
            os.makedirs(temporary_dst_path)

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=1, message='Get task information',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Process for per profile
        profile_info = list()
        current_percentage = 5
        delta_percentage = round((75 - current_percentage) / (len(profile_data) * len(profile_data) * 3), 2)
        for mapper_key, mapper_data in profile_data.items():
            input_list = list()
            mapping_count = 0
            temporary_playlist_path = os.path.join(temporary_dst_path, mapper_key, 'playlist.m3u8')
            temporary_segment_path = os.path.join(temporary_dst_path, mapper_key)
            if not os.path.isdir(temporary_segment_path):
                os.makedirs(temporary_segment_path)
            # Firstly, scan for 1 stream video
            for _temp in workflow_data['metadata']['input']:
                if str(_temp['data_type']).lower() == 'video' and mapper_data['video_profile'] == _temp['profile_id']:
                    if not os.path.isfile(_temp['phys_output_dst_path']):
                        _temp_name = Hasher.md5(_temp['phys_output_dst_path'])
                        _temp_name += os.path.split(_temp['phys_output_dst_path'])[-1]
                        _temp['phys_output_dst_path'] = os.path.join(temporary_dst_path, _temp_name)
                        if not os.path.isfile(_temp['phys_output_dst_path']):
                            StorageAdapter.copy_file(_temp['uri_output_dst_path'],
                                                     'file://' + _temp['phys_output_dst_path'])
                            junk_paths.append(_temp['phys_output_dst_path'])
                    input_list += ['-i', _temp['phys_output_dst_path']]
                    profile_info.append({
                        'playlist_path': os.path.join(mapper_key, os.path.basename(temporary_playlist_path)),
                        'video_resolution': _get_default_resolution(_temp['phys_output_dst_path']),
                        'video_width': str(video_profile_data[_temp['profile_id']]['width']),
                        'video_height': str(video_profile_data[_temp['profile_id']]['height']),
                        'video_bandwidth': str(video_profile_data[_temp['profile_id']]['bitrate']) + '000',
                    })
                    mapping_count += 1
                    break

            # Secondly, scan for audio
            for _temp in workflow_data['metadata']['input']:
                if str(_temp['data_type']).lower() == 'audio' and mapper_data['audio_profile'] == _temp['profile_id']:
                    if not os.path.isfile(_temp['phys_output_dst_path']):
                        _temp_name = Hasher.md5(_temp['phys_output_dst_path'])
                        _temp_name += os.path.split(_temp['phys_output_dst_path'])[-1]
                        _temp['phys_output_dst_path'] = os.path.join(temporary_dst_path, _temp_name)
                        if not os.path.isfile(_temp['phys_output_dst_path']):
                            StorageAdapter.copy_file(_temp['uri_output_dst_path'],
                                                     'file://' + _temp['phys_output_dst_path'])
                            junk_paths.append(_temp['phys_output_dst_path'])
                    input_list += ['-i', _temp['phys_output_dst_path']]
                    mapping_count += 1

            # Build command-line
            cmnd = list()
            for _t_data in str(workflow_data['feature']['metadata']['command_template']).split(' '):
                if _t_data == '<input_list>':
                    cmnd += input_list
                elif _t_data == '<map_list>':
                    for _temp in range(0, mapping_count):
                        cmnd += ['-map', str(_temp)]
                elif _t_data == '<output_playlist>':
                    cmnd.append(temporary_playlist_path)
                elif _t_data == '<output_segments>':
                    cmnd.append(os.path.join(temporary_segment_path, 'segment_%d.ts'))
                else:
                    cmnd.append(_t_data)
            print('######')
            print(' '.join(cmnd))

            # START OF: Update progress
            current_percentage += delta_percentage
            utils.update_progress(self_task=self, error=error, percentage=current_percentage,
                                  message='Mux file with mapper_key=' + str(mapper_key),
                                  extras={'cmnd': ' '.join(cmnd)}, workflow=workflow_data,
                                  file_id=input_params['file_id'], file_info=None)
            # END OF: Update progress
            #
            utils.run_command_with_progress(cmnd=cmnd, timeout=60*60, percentage_init=current_percentage,
                                            percentage_factor=delta_percentage/100, callback_func=utils.update_progress,
                                            callback_params={'self_task': self, 'error': 0, 'percentage': 40,
                                                             'message': '', 'file_id': file_data['id']})
            current_percentage += delta_percentage

        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=76, message='Begin to build HLS parent playlist',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress
        #
        hls_playlist.build_parent_playlist(profile_info, os.path.join(temporary_dst_path, 'playlist.m3u8'))
        #
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=77,
                              message='Complete to build HLS parent playlist', file_id=input_params['file_id'],
                              workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Zip this folder
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=78, message='Begin to build zip file',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress
        #
        temporary_zip_path = os.path.join(tempfile.gettempdir(), 'transcode',
                                          os.path.splitext(file_data['name'])[0] + '_hls')
        utils.make_archive(temporary_zip_path, 'zip', temporary_dst_path)
        #
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=88, message='Complete to build zip file',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        temporary_zip_path += '.zip'
        junk_paths.append(temporary_zip_path)
        # Upload ZIP file
        phys_output_dst_path = os.path.join(phys_output_dst_path, os.path.basename(temporary_zip_path))
        uri_output_dst_path = os.path.join(uri_output_dst_path, os.path.basename(temporary_zip_path))
        # START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=89, message='Start to upload zip file',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress
        #
        StorageAdapter.copy_file('file://' + temporary_zip_path, uri_output_dst_path)
        #
        #  START OF: Update progress
        utils.update_progress(self_task=self, error=error, percentage=95, message='Complete to upload zip file',
                              file_id=input_params['file_id'], workflow=workflow_data, file_info=information['file_info'])
        # END OF: Update progress

        # Update metadata
        workflow_data['status'] = Workflow.S_SUCCESS
        workflow_data['metadata']['output'] = {
            'temporary_zip_path': temporary_zip_path,
            'phys_output_dst_path': phys_output_dst_path,
            'uri_output_dst_path': uri_output_dst_path,
        }
        return {
            'error': error,
            'file_id': input_params['file_id'],
            'file_info': None,
            'workflow': workflow_data,
        }
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message=str(e.args), traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    except:
        utils.update_progress(self_task=self, error=1, percentage=None,
                              message='Critical exception!', traceback=traceback.format_exc(),
                              state=states.FAILURE, file_id=input_params['file_id'], workflow=workflow_data)
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if os.path.isfile(_temp):
                    os.remove(_temp)
                elif os.path.isdir(_temp):
                    shutil.rmtree(_temp)


def _get_default_resolution(filename):
    try:
        info = utils.probe_video_file(filename)
        if not info:
            return None
        # info = json.loads(info)
        return "%sx%s" % (info['streams'][0]['width'], info['streams'][0]['height'])
    except Exception as e:
        traceback.print_exc()
        return None
