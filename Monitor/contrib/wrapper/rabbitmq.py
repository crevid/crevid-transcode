import re
import requests
import traceback
from celery import current_app
from celery.app.control import Inspect
from simplejson.scanner import JSONDecodeError
from urllib.parse import urlparse, quote
from django.conf import settings


class ControlRabbitMQ(object):

    """
    :type app_control: celery.app.control.Control
    """
    app_control = current_app.control
    mgn_rbmq_api_url = None
    mgn_credential = None
    mgn_env = None

    def __init__(self, mgn_rbmq_api_url=getattr(settings, 'RABBITMQ_ADMIN_API'), **kwargs):
        self.mgn_rbmq_api_url = mgn_rbmq_api_url
        self.mgn_credential = (settings.RABBITMQ_ADMIN_USERNAME, settings.RABBITMQ_ADMIN_PASSWORD)
        resolver = urlparse(settings.CELERY_BROKER_URL)
        self.mgn_env = quote(resolver.path.replace('//', '/'), safe='')

    def request_get(self, api_url, query_params=None, timeout=10):
        data = None
        r = None
        try:
            print('Request GET URL: %s with query_data %s' % (str(api_url), str(query_params)))
            r = requests.get(api_url, params=query_params, timeout=timeout, auth=self.mgn_credential)
            data = r.json()
        except JSONDecodeError:
            traceback.print_exc()
            if r is not None:
                print(r.text)
                data = r.text
        except:
            traceback.print_exc()
        finally:
            return data

    def request_delete(self, api_url, timeout=10):
        try:
            print('Request DELETE URL: ' + str(api_url))
            requests.delete(api_url, timeout=timeout, auth=self.mgn_credential)
            return True
        except:
            traceback.print_exc()
            return False

    def list_active_workers(self):
        workers = list()
        results = self.app_control.ping(timeout=2.0)
        for name in results:
            if 'ok' in results[name]:
                workers.append(name)
        return results

    def inspect_worker_info(self, worker_names):
        if not isinstance(worker_names, list):
            worker_names = [worker_names, ]
        results = dict()
        inspector = self.app_control.inspect(worker_names)
        if isinstance(inspector, Inspect):
            sample_data = {
                'active_queues': list(),
                'used_memory': 0,
                'pid': None,
                'total_accepted_tasks': {},
                'max-concurrency': 1,
                'processes': [],
            }
            # get active queues
            data = inspector.active_queues()
            if isinstance(data, dict):
                for worker_name in data:
                    if worker_names not in results:
                        results[worker_name] = sample_data.copy()
                    for queue_info in data[worker_names]:
                        results[worker_name]['active_queues'].append(queue_info['name'])

            # get memory info
            data = inspector.memdump()
            if isinstance(data, dict):
                for worker_name in data:
                    if worker_names not in results:
                        results[worker_name] = sample_data.copy()
                    search = re.match(r'.*\s([0-9\.]+)\s*([A-Za-z]+).*', data[worker_name], re.IGNORECASE)
                    if search:
                        results[worker_name]['used_memory'] = "%s %s" % (str(search.group(1)), str(search.group(2)))

            # get other info
            data = inspector.stats()
            if isinstance(data, dict):
                for worker_name in data:
                    if worker_names not in results:
                        results[worker_name] = sample_data.copy()
                    results[worker_name]['pid'] = data[worker_name]['pid']
                    results[worker_name]['total_accepted_tasks'] = data[worker_name]['total']
                    results[worker_name]['max-concurrency'] = data[worker_name]['pool']['max-concurrency']
                    results[worker_name]['processes'] = data[worker_name]['pool']['processes']
        return results

    def get_queues_info(self, queue_name=None):
        results = dict()
        api_url = "/api/queues/" + self.mgn_env
        if queue_name:
            api_url += '/' + queue_name
        api_url = str(self.mgn_rbmq_api_url).strip('/') + api_url
        query_params = {'consumer_details': 1}
        data = self.request_get(api_url=api_url, query_params=query_params)
        if queue_name:
            data = [data]
        for info in data:
            if 'name' in info:
                temp = {
                    'queue_name': info['name'],
                    'num_consumers': len(info['consumer_details']) if 'consumer_details' in info else None,
                    'msg_ready': info['messages_ready'],
                    'msg_uack': info['messages_unacknowledged'],
                    'msg_total': info['messages']
                }
                results[info['name']] = temp
        return results

    def delete_queue_by_name(self, queue_name):
        api_url = "/api/queues/%s/%s" % (self.mgn_env, queue_name)
        api_url = str(self.mgn_rbmq_api_url).strip('/') + api_url
        return self.request_delete(api_url=api_url)

    def revoke_task(self, task_id):
        self.app_control.revoke(task_id=task_id, terminate=True)

