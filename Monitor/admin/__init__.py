from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from ..models import ComputeNode, Instance, Task

__author__ = 'duybq'


class ComputeNodeAdmin(admin.ModelAdmin):
    """
    Admin interface of Instance
    """
    list_display = ('id', 'hostname', 'control_queue', 'ssh_account', 'cpu_info', 'gpu_info',
                    'memory_info', 'status', 'created_date')
    list_display_links = ('id', 'hostname')
    search_fields = ['id', 'hostname', 'control_queue', 'status', 'ssh_hostname', 'ssh_username', 'ssh_private_key']
    ordering = ('-created_date', )
    list_filter = ('status', )
    readonly_fields = ('id', 'control_queue', 'created_date', 'modified_date', 'cpu', 'gpu', 'memory_mb')

    def ssh_account(self, obj):
        return str(obj.ssh_hostname) + '@' + str(obj.ssh_username)
    ssh_account.short_description = _("SSH")

    def cpu_info(self, obj):
        if isinstance(obj.cpu, dict):
            print(obj.cpu)
            return obj.cpu.get('__num__', 0)
        return None
    cpu_info.short_description = _("CPU")

    def gpu_info(self, obj):
        if isinstance(obj.gpu, dict) and obj.gpu.get('gpu_nvidia', None):
            return obj.gpu['gpu_nvidia'].get('gpu', None)
        return None
    gpu_info.short_description = _("GPU")

    def memory_info(self, obj):
        if isinstance(obj.memory_mb, dict):
            return obj.memory_mb.get('total_phys_memory', None)
        return None
    memory_info.short_description = _("Memory")

    def ssh_account(self, obj):
        return str(obj.ssh_username) + '@' + str(obj.ssh_hostname)
    ssh_account.short_description = _("SSH")

admin.site.register(ComputeNode, ComputeNodeAdmin)


class InstanceAdmin(admin.ModelAdmin):
    """
    Admin interface of Instance
    """
    list_display = ('id', 'name', 'hostname', 'run_mode', 'status', 'gpu_mode',
                    'cpu_mode', 'cpu_reversed_num', 'memory_mb', 'launched_at')
    list_display_links = ('id', 'hostname', 'name')
    search_fields = ['id', 'hostname', 'name', 'status']
    ordering = ('-created_date', )
    list_filter = ('run_mode', 'cpu_mode', 'gpu_mode')
    readonly_fields = ('id', 'hostname', 'status', 'created_date', 'modified_date', 'launched_at',
                       'terminated_at', 'metadata')

admin.site.register(Instance, InstanceAdmin)


class TaskAdmin(admin.ModelAdmin):
    """
    Admin interface of Task
    """
    list_display = ('id', 'published_feature', 'published_id', 'priority', 'status',
                    'launched_at', 'terminated_at')
    list_display_links = ('id', )
    search_fields = ['id', 'published_feature', 'published_id', 'metadata']
    ordering = ('-created_date', )
    list_filter = ('status', )
    readonly_fields = ('id', 'published_feature', 'published_id',
                       'created_date', 'modified_date', 'launched_at', 'terminated_at', 'metadata')

admin.site.register(Task, TaskAdmin)
