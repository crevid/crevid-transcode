from __future__ import absolute_import

from celery import Celery
from . import settings


app = Celery('Monitor', include=['Monitor.tasks'])
app.config_from_object(settings)


if __name__ == '__main__':
    app.start()
