import re
import time
import traceback
from datetime import datetime, timedelta
from django.conf import settings
from celery import shared_task, current_app

import Controller.tasks
from Monitor.contrib.wrapper.rabbitmq import ControlRabbitMQ

from ..models import ComputeNode, Task

__author__ = 'duybq'


def process_notify_device_info(params):
    try:
        print('Query existing compute node info')
        compute_obj = ComputeNode.objects.get(hostname=params['hostname'])
    except ComputeNode.DoesNotExist:
        print('Create new compute node info')
        compute_obj = ComputeNode()
    compute_obj.hostname = params['hostname']
    compute_obj.control_queue = params['control_queue']
    compute_obj.network = params['network']
    compute_obj.cpu['__num__'] = params['cpu']['cores']
    compute_obj.gpu = params['vga']
    compute_obj.memory_mb = params['memory']
    compute_obj.disk_gb = params['disk']
    if compute_obj not in (ComputeNode.S_INITIAL, ComputeNode.S_RUN_DOCKER,
                           ComputeNode.S_RUN_PROCESS, ComputeNode.S_RUN_ALL):
        compute_obj.status = ComputeNode.S_INITIAL
    compute_obj.save()
    return compute_obj.status


def notify_agent_process_shutdown(params):
    try:
        print('Query existing compute node info')
        compute_obj = ComputeNode.objects.get(hostname=params['hostname'])
    except ComputeNode.DoesNotExist:
        print('Create new compute node info')
        compute_obj = ComputeNode()
    compute_obj.hostname = params['hostname']
    compute_obj.control_queue = params['control_queue']
    compute_obj.network = params['network']
    compute_obj.cpu['__num__'] = params['cpu']['cores']
    compute_obj.gpu = params['vga']
    compute_obj.memory_mb = params['memory']
    compute_obj.disk_gb = params['disk']
    compute_obj.status = ComputeNode.S_STOPPED
    compute_obj.save()
    return compute_obj.status


def scan_check_compute_queue():
    start_time = int(time.time())
    print('# Task start at: ' + str(datetime.now()))
    control_queues = list()
    broker_control = ControlRabbitMQ()
    # Check from DB
    query = ComputeNode.objects.filter(status__in=(ComputeNode.S_INITIAL, ComputeNode.S_RUN_DOCKER,
                                                   ComputeNode.S_RUN_PROCESS, ComputeNode.S_RUN_ALL))
    for compute_obj in query:
        queue_info = broker_control.get_queues_info(compute_obj.control_queue)
        control_queues.append(compute_obj.control_queue)
        # Case: can not get info of this queue in RabbitMQ
        if (not queue_info) or (not isinstance(queue_info, dict)) or (compute_obj.control_queue not in queue_info):
            if not isinstance(compute_obj.metadata, dict):
                compute_obj.metadata = dict()
            compute_obj.metadata.update({
                'error': 'Mismatch Info: DB marks ok but there is no information about this control_queue in RabbitMQ'
            })
            print(compute_obj.metadata['error'] + ' | ' + compute_obj.control_queue)
            if ('history' not in compute_obj.metadata) or (not compute_obj.metadata['history']) \
                    or (not isinstance(compute_obj.metadata['history'], list)):
                compute_obj.metadata['history'] = list()
            compute_obj.metadata['history'].append("%s: DB marks ok but there is no information about this "
                                                   "control_queue in RabbitMQ" % str(datetime.now()))
            compute_obj.status = ComputeNode.S_ERROR
            compute_obj.save()
            continue

        queue_info = queue_info[compute_obj.control_queue]
        # Case: there are no consumer on this queue
        if int(queue_info['num_consumers']) < 1:
            if not isinstance(compute_obj.metadata, dict):
                compute_obj.metadata = dict()
            compute_obj.metadata.update({
                'error': 'Mismatch Info: DB marks ok but there is no consumer listen this control_queue in RabbitMQ'
            })
            print(compute_obj.metadata['error'] + ' | ' + compute_obj.control_queue)
            if ('history' not in compute_obj.metadata) or (not compute_obj.metadata['history']) \
                    or (not isinstance(compute_obj.metadata['history'], list)):
                compute_obj.metadata['history'] = list()
            compute_obj.metadata['history'].append("%s: DB marks ok but there is no consumer listen this control_queue "
                                                   "in RabbitMQ" % str(datetime.now()))
            compute_obj.status = ComputeNode.S_ERROR
            compute_obj.save()
            broker_control.delete_queue_by_name(compute_obj.control_queue)
            continue

        # Case: there are more than 1 consumer
        if int(queue_info['num_consumers']) > 1:
            if not isinstance(compute_obj.metadata, dict):
                compute_obj.metadata = dict()
            compute_obj.metadata.update({
                'error': 'Warning: There are more than 1 consumer in this control queue'
            })
            print(compute_obj.metadata['error'] + ' | ' + compute_obj.control_queue)
            compute_obj.save()
            continue

        # Case: consumer == 1, do nothing
        pass
    print('Finish check on DB: ' + str(int(time.time()-start_time)))

    # Check from RabbitMQ and exclude control_queue
    data = broker_control.get_queues_info()
    if data and isinstance(data, dict):
        for hostname, info in data.items():
            # Skip current queue
            if info['queue_name'] in control_queues:
                continue
            # check if this is control queue
            search = re.match(r'^hostname-[0-9A-Za-z\-@]+$', info['queue_name'])
            if not search:
                continue
            # Keep on
            print('Miss compute info with queue: ' + info['queue_name'])
            current_app.send_task('ComputeNode.tasks.agent.request_device_info', expires=60,
                                  queue=info['queue_name'], exchange=info['queue_name'])
    print('Finish check on RabbitMQ: ' + str(int(time.time()-start_time)))
    print('# Task end at ' + str(datetime.now()))


def check_queue_resource(feature_module):
    module = getattr(Controller.tasks, feature_module)
    class_name = module.__module__ + '.' + module.__class__.__name__
    route_info = settings.CELERY_TASK_ROUTES.get(class_name, None)
    if (not route_info) or (not isinstance(route_info, dict)) or ('queue' not in route_info):
        #
        # in case this module is not declare in queue, skip it
        #
        print('Bypass case: This module is not declare in queue, skip: ' + str(class_name))
        return True
    queue_name = route_info['queue']
    # query RabbitMQ to check
    broker_control = ControlRabbitMQ()
    queue_info = broker_control.get_queues_info(queue_name)
    if (not queue_info) or (not isinstance(queue_info, dict)) or (queue_name not in queue_info):
        #
        # in case this queue has not declare yet
        #
        print('Skip case: This queue has not declare yet: ' + str(queue_name))
        return False
    queue_info = queue_info[queue_name]
    if int(queue_info['msg_total']) - int(queue_info['num_consumers']) > 2:
        #
        # in case this queue reaches "own-limit", do not publish task anymore
        #
        print('Skip case: This queue reaches "own-limit", do not publish task anymore')
        return False
    return True


def auto_schedule_initial_task():
    current_date = datetime.now()
    query = Task.objects.filter(status=Task.S_INITIAL).order_by('-priority', 'created_date')
    count = 0
    for task_obj in query:
        # Check queue resource
        if not check_queue_resource(task_obj.published_feature):
            print("Temporary skip file_id=%s task_id=%s, module=%s"
                  % (str(task_obj.metadata['file_id']), str(task_obj.id), str(task_obj.published_feature)))
            return False

        # Produce basic info
        task_obj.status = Task.S_ENQUEUED
        task_obj.metadata['history'].append("%s: task has been enqueued by auto scheduler." % str(current_date))
        # publish task
        getattr(Controller.tasks, task_obj.published_feature).apply_async(
            args=[{'file_id': task_obj.metadata['file_id'], 'workflow': str(task_obj.id)}],
            countdown=1, task_id=str(task_obj.id), priority=task_obj.priority)
        task_obj.save()
        count += 1
    return count


def auto_schedule_pending_task():
    current_date = datetime.now()
    considered_date = current_date - timedelta(hours=12)
    query = Task.objects.filter(status__in=(Task.S_ENQUEUED, Task.S_RUNNING), modified_date__lt=considered_date)\
                        .order_by('-priority', 'modified_date')
    count = 0
    broker_control = ControlRabbitMQ()
    for task_obj in query:
        # Revoke task
        broker_control.revoke_task(task_id=str(task_obj.id))
        # Check queue resource
        manual_schedule_task(file_id=task_obj.metadata['file_id'], task_id=str(task_obj.id),
                             feature_module=task_obj.published_feature, published_id=task_obj.published_id,
                             priority=2)
    return count


def manual_schedule_task(file_id, task_id, feature_module, published_id, priority=3, **kwargs):
    # Reset to initial info
    print("Check the file_id=%s: %s" % (str(file_id), str(feature_module)))
    current_date = datetime.now()
    try:
        task_obj = Task.objects.get(pk=task_id)
    except Task.DoesNotExist:
        task_obj = Task()
        task_obj.id = task_id
    task_obj.status = Task.S_INITIAL
    task_obj.published_feature = feature_module
    task_obj.published_id = published_id
    task_obj.priority = priority
    if not task_obj.metadata or not isinstance(task_obj.metadata, dict):
        task_obj.metadata = Task.DEFAULT_METADATA_VALUE
    if 'history' not in task_obj.metadata or not isinstance(task_obj.metadata, list):
        task_obj.metadata['history'] = list()
    task_obj.metadata['history'].append("%s: Initially setup." % str(current_date))
    task_obj.metadata['file_id'] = str(file_id)
    task_obj.save()

    # Check queue resource
    if not check_queue_resource(feature_module):
        print("Temporary skip file_id=%s task_id=%s, module=%s"
              % (str(task_obj.metadata['file_id']), str(task_obj.id), str(feature_module)))
        return False

    # Produce basic info
    task_obj.status = Task.S_ENQUEUED
    task_obj.metadata['history'].append("%s: task has been enqueued by manual scheduler." % str(current_date))
    # publish task
    getattr(Controller.tasks, feature_module).apply_async(
        args=[{'file_id': task_obj.metadata['file_id'], 'workflow': str(task_obj.id)}],
        countdown=10, task_id=str(task_obj.id), priority=priority)
    task_obj.save()
    return True


def mapping_task_with_worker(task_id, hostname, **kwargs):
    current_date = datetime.now()
    task_obj = Task.objects.get(pk=task_id)
    task_obj.status = kwargs.get('status', Task.S_RUNNING)
    task_obj.hostname = hostname
    if task_obj.status == Task.S_RUNNING:
        task_obj.launched_at = current_date
    else:
        task_obj.terminated_at = current_date
    if not task_obj.metadata or not isinstance(task_obj.metadata, dict):
        task_obj.metadata = Task.DEFAULT_METADATA_VALUE
    if 'history' not in task_obj.metadata or not isinstance(task_obj.metadata, list):
        task_obj.metadata['history'] = list()
    task_obj.metadata['history'].append("%s: task has been launched." % str(current_date))
    task_obj.save()


def auto_clean_task():
    current_date = datetime.now()
    # Query for stopped task
    considered_date = current_date - timedelta(days=7)
    query = Task.objects.filter(status=Task.S_STOPPED, modified_date__lt=considered_date)
    count_stopped = 0
    for task_obj in query:
        print("Auto-remove stopped task: %s %s" % (str(task_obj.published_feature), str(task_obj.id)))
        task_obj.delete()
        count_stopped += 1

    # Query for error task
    considered_date = current_date - timedelta(days=14)
    query = Task.objects.filter(status=Task.S_ERROR, modified_date__lt=considered_date)
    count_error = 0
    for task_obj in query:
        print("Auto-remove error task: %s %s" % (str(task_obj.published_feature), str(task_obj.id)))
        task_obj.delete()
        count_error += 1
    return count_stopped, count_error


@shared_task(bind=True)
def send_request_action(self, action, params):
    # This task will be executed from Monitor
    action = str(action).lower()
    try:
        print('############################')
        print('Enter action: ' + str(action))
        if action.find('query_device_info') > -1:
            for compute_obj in ComputeNode.objects.filter(
                    status__in=(ComputeNode.S_INITIAL, ComputeNode.S_RUN_DOCKER,
                                ComputeNode.S_RUN_PROCESS, ComputeNode.S_RUN_ALL)):
                current_app.send_task('ComputeNode.tasks.agent.request_device_info', expires=60,
                                      queue=compute_obj.control_queue, exchange=compute_obj.control_queue)

        elif action.find('check_compute_queue') > -1:
            scan_check_compute_queue()

        else:
            print('Does not support this action: ' + str(action))
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        raise


@shared_task(bind=True)
def recv_request_action(self, action, params):
    # This task will be executed from Monitor
    action = str(action).lower()
    try:
        print('Enter action: ' + str(action))
        if action.find('notify_device_info') > -1:
            compute_status = process_notify_device_info(params=params)
            if compute_status == ComputeNode.S_INITIAL and action.find('initial') > -1:
                print('> state initial')

        elif action.find('notify_agent_process_shutdown') > -1:
            compute_status = notify_agent_process_shutdown(params=params)

        elif action.find('manual_schedule_task') > -1:
            manual_schedule_task(**params)

        elif action.find('auto_schedule_task') > -1:
            auto_schedule_pending_task()
            auto_schedule_initial_task()

        elif action.find('mapping_task_with_worker') > -1:
            mapping_task_with_worker(**params)

        elif action.find('auto_clean_task') > -1:
            auto_clean_task()

        else:
            print('> Does not support this action: ' + str(action))
            print(action.find('check_compute_queue'))
    except Exception as e:
        print("Error: " + str(e.args))
        print(params)
        traceback.print_exc()
        raise
