import os
import time
import shutil
import random
import tempfile
import traceback
from datetime import datetime
from django.db.models import Q, Count

from celery import shared_task
from fabric import operations as fabric_operations

from Monitor.contrib.wrapper.rabbitmq import ControlRabbitMQ

from ..models import ComputeNode, Instance

__author__ = 'duybq'


def get_least_instances_compute(run_mode=(ComputeNode.S_RUN_DOCKER,), force_gpu=False):
    min_compute = None
    min_instances = 0
    compute_ids = list()
    # compute condition
    condition = Q(status__in=run_mode)
    if force_gpu:
        condition &= Q(can_use_gpu=True)
    # query instance table
    results = Instance.objects.filter(Q(status=Instance.S_RUNNING) & Q(compute_node_id__isnull=False)) \
                              .values('compute_node_id').annotate(instances_count=Count('compute_node_id'))
    compute_obj = None
    for info in results:
        # only mark the compute has lower instances
        if (not min_compute) or (min_instances > info['instances_count']):
            try:
                compute_obj = ComputeNode.objects.get(condition & Q(id=info['compute_node_id']))
                min_compute = info['compute_node_id']
                min_instances = int(info['instances_count'])
            except ComputeNode.DoesNotExist:
                print('This compute does not exist anymore: ' + str(info['compute_node_id']))
                continue
        # record all compute id available
        if info['compute_node_id'] not in compute_ids:
            compute_ids.append(info['compute_node_id'])

    # check if some compute have never been used
    query = ComputeNode.objects.filter(condition & ~Q(id__in=compute_ids))
    if query.count() > 0:
        # return randomly un-used compute
        compute_obj = random.choice(query)
    # otherwise, return least instances compute
    return compute_obj


def create_instance_info(instance_id, **kwargs):
    try:
        instance_obj = Instance.objects.get(pk=instance_id)
    except:
        instance_obj = Instance()
        instance_obj.id = instance_id
    for key in kwargs:
        if hasattr(instance_obj, key):
            setattr(instance_obj, key, kwargs.get(key))
    instance_obj.status = Instance.S_INITIAL
    instance_obj.save()
    return instance_obj


@shared_task(bind=True)
def add_instance_backjob(self, params):
    # This task will be executed from Monitor
    instance_id = params['instance_id']
    junk_paths = list()
    temp_folder = os.path.join(tempfile.gettempdir(), 'transcode-' + str(int(time.time())))
    if not os.path.isdir(temp_folder):
        os.makedirs(temp_folder)
    junk_paths.append(temp_folder)
    instance_obj = None

    try:
        #
        # Query compute information
        compute_obj = get_least_instances_compute()
        if not compute_obj:
            raise Exception('Do not have any compute!')
        run_with_gpu = True if params['gpu_mode'] in (Instance.G_AUTO_SETUP, Instance.G_MANUAL_SETUP) else False
        run_with_gpu = run_with_gpu and isinstance(compute_obj.gpu, dict) and ('gpu_nvidia' in compute_obj.gpu) \
                                    and isinstance(compute_obj.gpu['gpu_nvidia'], dict) \
                                    and (compute_obj.gpu['gpu_nvidia']['gpu'] is not None)
        if run_with_gpu:
            params = params['gpu']
        else:
            params = params['cpu']
        # Retrieve instance information
        instance_obj = create_instance_info(instance_id, **params)
        instance_obj.launched_at = datetime.now()
        instance_obj.status = Instance.S_ENQUEUED
        instance_obj.save()

        # Write compute key
        ssh_key_file = os.path.join(temp_folder, 'id_rsa')
        with open(ssh_key_file, 'w') as file_handler:
            file_handler.write(compute_obj.ssh_private_key)
        os.chmod(ssh_key_file, 0o400)
        # ssh to remote compute
        ssh_info = {
            "key_filename": ssh_key_file,
            "host_string": compute_obj.ssh_hostname,
            "user": compute_obj.ssh_username,
            "clean_revert": True,
            "disable_known_hosts": True,
            "warn_only": True,
        }

        #
        # check run_mode
        if instance_obj.run_mode != Instance.M_DOCKER:
            print("This mode have not supported yet %s: %s" % (str(instance_id), instance_obj.run_mode))
            return 'Skip run-mode: does not support!'
        # build command-line

        cmnd = 'nvidia-docker' if run_with_gpu else 'docker'
        cmnd += " run -d --name %s --restart unless-stopped" % instance_obj.name
        cmnd += " -h " + str(instance_obj.hostname)
        cmnd += " -m " + str(instance_obj.memory_mb) + 'm'

        if instance_obj.cpu_mode:
            if instance_obj.cpu_mode == Instance.C_MANUAL_SETUP and instance_obj.cpu_reversed_pos:
                cmnd += " --cpuset-cpus='%s'" % str(','.join(instance_obj.cpu_reversed_pos))

        if instance_obj.volume and isinstance(instance_obj.volume, dict):
            for path_local, path_docker in instance_obj.volume.items():
                cmnd += " -v %s:%s" % (str(path_local), str(path_docker))

        if instance_obj.static_host and isinstance(instance_obj.static_host, dict):
            for host_domain, host_ip in instance_obj.static_host.items():
                cmnd += " --add-host %s:%s" % (str(host_domain), str(host_ip))

        if instance_obj.env_var and isinstance(instance_obj.env_var, dict):
            for var_name, var_val in instance_obj.env_var.items():
                cmnd += " -e %s=\"%s\"" % (str(var_name), str(var_val))

        if instance_obj.network and isinstance(instance_obj.network, dict):
            network_type = instance_obj.network.pop('__type__', 'nat')
            if str(network_type).lower().find('host') > -1:
                cmnd += ' --net=host'
            else:
                for port_local, port_docker in instance_obj.network.items():
                    _temp = str(port_local).split('/')
                    _protocol = 'tcp' if len(_temp) < 2 else _temp[1]
                    cmnd += " -p %s:%s/%s" % (str(_temp[0]), str(port_docker), str(_protocol))

        # Get execution info
        # image
        if run_with_gpu and 'docker_image_gpu' in instance_obj.execute_info:
            docker_image = str(instance_obj.execute_info['docker_image_gpu'])
        else:
            docker_image = str(instance_obj.execute_info['docker_image'])
        # cmnd
        if instance_obj.execute_info.get('docker_cmnd', None):
            docker_cmnd = instance_obj.execute_info['docker_cmnd']
        else:
            docker_cmnd = ''
        cmnd += " %s %s" % (docker_image, docker_cmnd)

        # Filter cmnd AGAIN
        if run_with_gpu:
            cmnd = cmnd.replace('libx264', 'nvenc_h264 -gpu 1')

        #
        # Executing command-line
        instance_obj.metadata['history'].append("%s: Launching instance..." % str(datetime.now()))
        with fabric_operations.settings(**ssh_info):
            print("Clean old docker")
            last_result = fabric_operations.run("docker stop " + str(instance_obj.name))
            last_result = fabric_operations.run("docker rm " + instance_obj.name)
            print("Start docker with: " + cmnd)
            last_result = fabric_operations.run(cmnd)
            if last_result != 0:
                raise Exception('Bad status code!')
        # Update instance info
        instance_obj.status = Instance.S_RUNNING
        instance_obj.metadata['success'].append("%s: Instance has launched success" % str(datetime.now()))
    except Exception as e:
        print("Error: " + str(e.args))
        traceback.print_exc()
        if instance_obj:
            if not instance_obj.metadata or not isinstance(instance_obj.metadata, dict):
                instance_obj.metadata = dict()
            if 'history' not in instance_obj.metadata:
                instance_obj.metadata['history'] = list()
            instance_obj.metadata['history'].append("%s: Instance has launched failed" % str(datetime.now()))
            if 'error' not in instance_obj.metadata:
                instance_obj.metadata['error'] = list()
            instance_obj.metadata['error'].append("%s: Instance has launched failed" % str(datetime.now()))
            instance_obj.terminated_at = datetime.now()
            instance_obj.status = Instance.S_ERROR
        raise
    finally:
        if junk_paths:
            for _temp in junk_paths:
                if os.path.isfile(_temp):
                    os.remove(_temp)
                elif os.path.isdir(_temp):
                    shutil.rmtree(_temp)
        if instance_obj:
            instance_obj.save()
    return True

