from jsonfield import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from api_core.models import CommonInfo

__author__ = 'duybq'


class ComputeNode(CommonInfo):
    S_INITIAL = 'initial'
    S_RUN_DOCKER = 'run-docker'
    S_RUN_PROCESS = 'run-process'
    S_RUN_ALL = 'run-all'
    S_STOPPED = 'stopped'
    S_ERROR = 'error'
    S_CLEANED = 'cleaned'
    STATUSES = (
        (S_INITIAL, _('Initially')),
        (S_RUN_DOCKER, _('Running with docker mode')),
        (S_RUN_PROCESS, _('Running with process mode')),
        (S_RUN_ALL, _('Running with all modes')),
        (S_STOPPED, _('Stopped')),
        (S_ERROR, _('Error')),
        (S_CLEANED, _('Cleaned')),
    )

    dirs_in_use = JSONField(default=['/tmp'], null=False)
    disk_available_least_gb = models.IntegerField(null=False, default=20)
    supported_features = JSONField(default={'feature-code': ['instance-id']}, null=False)
    is_locked = models.BooleanField(null=False, default=True, db_index=True,
                                    help_text=_('If the instance is locked, it can not be delete from our system'))
    ssh_hostname = models.CharField(max_length=40, null=True, default=None)
    ssh_username = models.CharField(max_length=40, null=True, default=None)
    ssh_private_key = models.TextField(null=True, default=None)
    can_use_gpu = models.BooleanField(null=False, default=False, blank=True, db_index=True)

    hostname = models.CharField(max_length=40, null=False, unique=True)
    control_queue = models.CharField(max_length=40, null=True)
    network = JSONField(null=True, default=None, editable=False, blank=True)
    cpu = JSONField(default={'cpu-id': ['instance-id'], '__num__': 0}, null=True, editable=False, blank=True)
    gpu = JSONField(null=True, editable=False, blank=True, default={
        'gpu_nvidia': {'gpu': None, 'sm': None, 'mem': None, 'enc': None, 'dec': None}, 'gpu_intel': dict()})
    memory_mb = JSONField(null=True, editable=False, blank=True, default={
        'total_phys_memory': 0, 'used_phys_memory': 0, 'total_swap_memory': 0, 'used_swap_memory': 0
    })
    disk_gb = JSONField(default={'<dir>': {'used': 0, 'total': 0}}, null=True, editable=False, blank=True)
    status = models.CharField(max_length=32, null=False, default=S_INITIAL, choices=STATUSES, blank=True)

    def __str__(self):
        return str(self.ssh_username) + '@' + str(self.hostname) + ': ' + str(self.status)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(CommonInfo, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class Instance(CommonInfo):
    S_INITIAL = 'initial'
    S_ENQUEUED = 'en-queued'
    S_RUNNING = 'running'
    S_STOPPED = 'stopped'
    S_ERROR = 'error'
    S_CLEANED = 'cleaned'   # Records whether an instance has been deleted from disk
    STATUSES = (
        (S_INITIAL, _('Initially')),
        (S_ENQUEUED, _('En-queued')),
        (S_RUNNING, _('Running')),
        (S_STOPPED, _('Stopped')),
        (S_ERROR, _('Error')),
        (S_CLEANED, _('Cleaned')),
    )

    G_NOUSE = 'gpu-nouse'
    G_MANUAL_SETUP = 'gpu-manualsetup'
    G_AUTO_SETUP = 'gpu-autosetup'
    GPU_MODES = (
        (G_NOUSE, _('Not use GPU')),
        (G_MANUAL_SETUP, _('Manual setup GPU card ID')),
        (G_AUTO_SETUP, _('Automatically setup')),
    )

    C_LIMITLESS = 'cpu-limitless'
    C_MANUAL_SETUP = 'cpu-manualsetup'
    C_AUTO_SETUP = 'cpu-autosetup'
    CPU_MODES = (
        (C_LIMITLESS, _('Not set any cpu limit')),
        (C_MANUAL_SETUP, _('Manual setup CPU core ID')),
        (C_AUTO_SETUP, _('Automatically setup')),
    )

    M_DOCKER = 'docker-mode'
    M_PROCESS = 'process-mode'
    RUN_MODES = (
        (M_DOCKER, _('Docker mode')),
        (M_PROCESS, _('Process mode')),
    )

    run_mode = models.CharField(max_length=32, null=False, default=M_DOCKER, choices=RUN_MODES)
    feature_codes = JSONField(default=[], null=False,
                              help_text=_('A list of feature codes is supported by this instance'))
    name = models.CharField(max_length=255, null=False, default=None)
    gpu_mode = models.CharField(max_length=32, null=False, default=G_NOUSE, choices=GPU_MODES)
    gpu_reversed_num = models.IntegerField(default=0, null=True,
                                           help_text=_('Number of reserved GPU card, currently only support 0 or 1.'))
    gpu_reversed_pos = JSONField(default=[], null=True,
                                 help_text=_('The list of GPU IDs that this instance will be reserved'))
    cpu_mode = models.CharField(max_length=32, null=False, default=C_LIMITLESS, choices=CPU_MODES)
    cpu_reversed_num = models.IntegerField(default=0, null=True,
                                           help_text=_('Number of reserved CPU cores'))
    cpu_reversed_pos = JSONField(default=[], null=True,
                                 help_text=_('The list of CPU IDs that this instance will be reserved'))
    memory_mb = models.IntegerField(default=1024, null=False,
                                    help_text=_('Set to 0 unlimited, or the value must >= 128 and <= max memory'))
    memory_swap_mb = models.IntegerField(default=-1, null=False,
                                         help_text=_('Default is unlimited swap page if OS supported'))
    volume = JSONField(default={}, null=True, help_text=_('Add host with key pair is <path>:<path-in-docker>'))
    network = JSONField(default={'__type__': None, 'mapping': dict()}, null=True,
                        help_text=_('Type can be \'host\' or \'nat\''))
    static_host = JSONField(default={}, null=True, help_text=_('Add host with key pair is <domain>:<ip>'))
    env_var = JSONField(default={}, null=True,
                        help_text=_('Add environment variable with key pair is <var_name>:<value>'))
    is_locked = models.BooleanField(null=False, default=True, db_index=True,
                                    help_text=_('If the instance is locked, it can not be delete from our system'))
    execute_info = JSONField(default={'docker_image': None, 'docker_cmnd': None}, null=False,
                             help_text='Include: docker_image, docker_cmnd')

    compute_node_id = models.CharField(max_length=40, null=True, default=None, blank=True)
    hostname = models.CharField(max_length=40, null=True, default=None, blank=True)
    status = models.CharField(max_length=32, null=False, default=S_INITIAL, choices=STATUSES, blank=True)
    launched_at = models.DateTimeField(null=True, default=None, editable=False, blank=True)
    terminated_at = models.DateTimeField(null=True, default=None, editable=False, blank=True)
    metadata = JSONField(null=True, default={'history': [], 'success': [], 'error': [], }, blank=True)

    class Meta:
        index_together = [
            ["cpu_mode", "gpu_mode"],
        ]

    def __str__(self):
        return str(self.id) + ': ' + str(self.hostname)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(CommonInfo, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class Task(CommonInfo):
    S_INITIAL = 'initial'
    S_ENQUEUED = 'enqueued'
    S_RUNNING = 'running'
    S_STOPPED = 'stopped'
    S_ERROR = 'error'
    S_CLEANED = 'cleaned'
    STATUSES = (
        (S_ENQUEUED, _('En-queued')),
        (S_RUNNING, _('Running')),
        (S_STOPPED, _('Stopped')),
        (S_ERROR, _('Error')),
        (S_CLEANED, _('Cleaned')),
    )
    DEFAULT_METADATA_VALUE = {'history': list(), 'success': None, 'error': None, 'file_id': None}

    published_feature = models.CharField(max_length=32, null=False, help_text=_('The feature module'))
    published_id = models.CharField(max_length=40, null=False, help_text=_('The workflow id'))
    hostname = models.CharField(max_length=40, null=False, help_text=_('The workflow id'))
    module_name = models.CharField(max_length=256, null=False, help_text=_('The module name'))
    priority = models.IntegerField(null=False, default=5)
    status = models.CharField(max_length=32, null=False, blank=True, db_index=True,
                              default=S_INITIAL, choices=STATUSES)
    launched_at = models.DateTimeField(null=True, default=None, editable=False, blank=True)
    terminated_at = models.DateTimeField(null=True, default=None, editable=False, blank=True)
    metadata = JSONField(null=True, default=DEFAULT_METADATA_VALUE, blank=True)

    def __str__(self):
        return str(self.id) + ' - ' + str(self.published_feature)
